@Library("perso-shared-lib@develop") _

pipeline {
    agent any
	tools {
		maven 'Maven339'
		jdk 'JDK8'
		nodejs 'NodeJS8.5.0'
	}
	
	options { 
		timestamps()
	}
	  
    parameters {
        booleanParam(defaultValue: true, description: 'Should build?', name: 'shouldBuild')
	}


	environment {
		VERSION = readMavenPom().getVersion()
		MVN_BUILD=true
        PATH = "/usr/local/bin:$PATH"
	}
     
    stages {
        
		stage('Looking for a bump') {
            when {
                branch 'release/*'
            }
            steps {
                echo 'Checking commit message'
                script{
                    result = sh (script: "git log -1 --pretty=format:%B | grep '.*Bump ---->.*'", returnStatus: true) 
                    if (result != 0) {
                        echo ("Bump not spotted, skipping build")
                        env.shouldBuild = "false"
                    }
                }           
            }
        }
        
		stage('Checking dependencies') {
			when {
                expression {
                    return env.shouldBuild != "false"
                }
            }
			steps {
				 retry(30){
					sleep 15 
					sh 'mvn wagon:exist -Dwagon.url=http://mvn.personetics.com/nexus/'
					}
				}
			}
        
		stage('Build') {
            when {
                expression {
                    return env.shouldBuild != "false"
                }
            }
            steps {
                sh 'mvn -Pprod clean install -Dmodules=all -DskipTests=true --batch-mode'
            }
        }
        stage('Publish To Nexus') {
            when {
                expression {
                    return env.shouldBuild != "false"
                }
            }
            steps {
                sh 'mvn -Pprod clean deploy -DdeployAtEnd=true -Dmodules=all -DskipTests=true --batch-mode'
            }
        }

    }

	post {
		always {
			script {
				notifySlack currentBuild.result, '#solutions-client', currentBuild
			}
		}
	}
}