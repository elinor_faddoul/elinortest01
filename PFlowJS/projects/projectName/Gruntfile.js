module.exports = function (grunt) {
	var productsFileList = {
		api: [
			"<%=clientCorePath%>/resources/configuration/perso-configuration.js",
			"<%=projectPath%>/js/resources/configuration/perso-configuration.js",
			"<%=clientCorePath%>/resources/images/images-mapping.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-dictionary.js",
			"<%=clientCorePath%>/utils/perso-helper.js",
			"<%=clientCorePath%>/utils/*.js",
			"<%=clientCorePath%>/utils/d3-extensions.js",
			"<%=clientCorePath%>/utils/web-analytics/*.js",
			"<%=clientCorePath%>/utils/events/*.js",
			"<%=projectPath%>/pstory/utils/events/*.js",
			"<%=clientCorePath%>/resources/texts/perso-dictionary.js",
			"<%=projectPath%>/js/resources/texts/perso-dictionary.js",
			"<%=clientCorePath%>/API/pserver-proxy.js",
			"<%=clientCorePath%>/API/pserver-proxy-native.js",
			"<%=clientCorePath%>/API/widget.js",
			"<%=clientCorePath%>/API/widgets-factory.js",
			"<%=clientCorePath%>/API/personetics.js",
			"<%=parentProjectPath%>/js/api-extensions/personetics.js"
		],
		vendor: [
			"<%=clientCorePath%>/lib/jsep.min.js",
			"<%=clientCorePath%>/lib/selfish.js",
			"<%=clientCorePath%>/lib/handlebars.runtime-v4.0.5.min.js",
			"<%=clientCorePath%>/lib/d3.min.js",
			"<%=clientCorePath%>/lib/jquery.touch.js",
			"<%=clientCorePath%>/lib/jquery.swipe.js",
			"<%=clientCorePath%>/lib/bluebird.min.js",
			"<%=clientCorePath%>/lib/html2canvas.min.js",
			"<%=clientCorePath%>/lib/jspdf.min.js"
		],
		vendorEditor: [
			"<%=clientCorePath%>/lib/velocity.min.js"
		],
		blockProcessor: [
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-exp.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-action.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-dictionary.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-utils.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-config.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-datasource.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-future.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-optionmodel.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-table.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-fact.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-query.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-model.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-text-template.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-text.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-image.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-options.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-buttons.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-account-selector.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-text-boxes.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-tabs.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-hbox.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-layout.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-date-box.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-table.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-bar.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-pie.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-pin.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-line.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-compare.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-manager.js",
			"<%=clientCorePath%>/pstory/blocks/block-processor/pstory-block-processor-entity-selector.js"
		],
		pstory: [
			"<%=clientCorePath%>/pstory/p-ctrl.js",
			"<%=clientCorePath%>/pstory/pstory-blocks-factory.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-base.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-layout.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-hbox.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-text.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-date-box.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-image.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-blocks-table.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-options.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-textBoxes.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-compareBlock.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-buttons.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-buttons-new.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-account-selector.js",
			"<%=clientCorePath%>/pstory/<%=skin%>/blocks/pstory-block-tabs.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-selectOne.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-bar-D3chart.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-bar-chart-new.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-pin-graph-chart.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-pinGraph-D3chart.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-dots-line-chart.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-pie-chart.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-pie-chart-new.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-pie-chart-wide.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-map.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-session.js",
			"<%=clientCorePath%>/pstory/blocks/pstory-block-entity-selector.js",
			"<%=clientCorePath%>/pstory/pstory-skin-ctrl.js",
			"<%=clientCorePath%>/pstory/pstory-dialog.js",
			"<%=clientCorePath%>/pstory/pstory-context.js",
			"<%=clientCorePath%>/pstory/pstory.js",
			"<%=clientCorePath%>/pstory/perso-skin-config.js",
			"<%=clientCorePath%>/utils/**/*.js",
			"<%=projectPath%>/pstory/utils/events/*.js",
			"<%=clientCorePath%>/utils/d3-extensions.js",
			"<%=clientCorePath%>/pstory/pstory-preview.js",
			"<%=parentProjectPath%>/js/blocks/*.js",
			"<%=projectPath%>/js/templates/js/templates.js",
			//block helpers
			"<%=clientCorePath%>/pstory/blocks/block-chart-helpers/block-chart-factory.js",
			"<%=clientCorePath%>/pstory/blocks/block-chart-helpers/block-helper-base.js",
			"<%=clientCorePath%>/pstory/blocks/block-chart-helpers/block-multi-vertical-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-chart-helpers/block-vertical-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-chart-helpers/block-pinGraph-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-chart-helpers/block-line-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-chart-helpers/block-line-chart-helper-new.js",
			"<%=clientCorePath%>/pstory/blocks/block-chart-helpers/block-horizontal-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-chart-helpers/block-multi-horizontal-chart-helper.js",
			"<%=parentProjectPath%>/js/blocks/block-chart-helpers/block-vertical-chart-helper.js",
			"<%=parentProjectPath%>/js/blocks/block-chart-helpers/block-pinGraph-chart-helper.js",
			"<%=projectPath%>/js/blocks/block-chart-helpers/*.js",

			//Block utils
			"<%=clientCorePath%>/pstory/blocks/block-utils/block-chart-data-builder.js",
			"<%=parentProjectPath%>/js/blocks/block-utils/block-collapse-helper.js",
			"<%=clientCorePath%>/utils/animations/button-animation.js",
			"<%=clientCorePath%>/utils/animations/dot-animation/dot-animation.js",
			"<%=clientCorePath%>/utils/animations/carousel-animation/carousel-animation.js",
			"<%=clientCorePath%>/pstory/blocks/renderer/PBlockRenderer.js",
			"<%=projectPath%>/js/blocks/*.js"
		],
		pstoryResponsive: [
			"<%=parentProjectPath%>/js/blocks/block-utils/PUIEvents/PResizeEvent.js",
			"<%=parentProjectPath%>/js/blocks/block-utils/PUIEvents/UIBreakpoints.js"
		],
		skin: [
			"<%=parentProjectPath%>/js/product-skin-ctrl<%=skin%>.js",
			"<%=projectPath%>/js/<%=projectName%>-skin-ctrl.js",
		],
		css: [
			"<%=clientCorePath%>/skin/stylesheets/pstory.css",
			"<%=clientCorePath%>/utils/animations/button-animation.css",
			"<%=clientCorePath%>/utils/animations/carousel-animation/carousel-animation.css",

			"<%=parentProjectPath%>/css/stylesheets/pstory-fonts.css",
			"<%=parentProjectPath%>/css/stylesheets/block-account-selector.css",
			"<%=parentProjectPath%>/css/stylesheets/block-account-selector-card.css",
			"<%=parentProjectPath%>/css/stylesheets/block-account-selector-all.css",
			"<%=parentProjectPath%>/css/stylesheets/block-buttons.css",
			"<%=parentProjectPath%>/css/stylesheets/block-buttons-navigateTo.css",
			"<%=parentProjectPath%>/css/stylesheets/block-buttons-radio.css",
			"<%=parentProjectPath%>/css/stylesheets/block-table.css",
			"<%=parentProjectPath%>/css/stylesheets/block-text.css",
			"<%=parentProjectPath%>/css/stylesheets/block-image.css",
			"<%=parentProjectPath%>/css/stylesheets/block-text-box.css",
			"<%=parentProjectPath%>/css/stylesheets/block-pie-chart.css",
			"<%=parentProjectPath%>/css/stylesheets/block-d3.css",
			"<%=parentProjectPath%>/css/stylesheets/block-line-chart.css",
			"<%=parentProjectPath%>/css/stylesheets/block-d3-collapse.css",
			"<%=parentProjectPath%>/css/stylesheets/rating-widget.css",
			"<%=parentProjectPath%>/css/stylesheets/product-skin.css",
			"<%=parentProjectPath%>/css/stylesheets/direction-rtl.css",
			"<%=parentProjectPath%>/css/stylesheets/block-bar-chart.css",
            "<%=parentProjectPath%>/css/stylesheets/block-pin-chart.css",
			"<%=parentProjectPath%>/css/stylesheets/direction-ltr.css",
			"<%=parentProjectPath%>/css/stylesheets/block-buttons-animation.css",
			"<%=parentProjectPath%>/css/stylesheets/error-msg.css",
			'<%=parentProjectPath%>/teasers/skin/css/stylesheets/*.css',
			"<%=projectPath%>/teasers/skin/css/stylesheets/*.css",
			"<%=projectPath%>/css/stylesheets/*.css"
		],
		cssVariables: [
			"<%=clientCorePath%>/skin/<%=skin%>/sass/_variables.scss",
			"<%=parentProjectPath%>/css/<%=skin%>/sass/_variables.scss",
			"<%=projectPath%>/css/sass/_variables.scss"
		],
		widgets: [
			"<%=clientCorePath%>/widgets/p-widgets-texts-factory.js",
			"<%=clientCorePath%>/widgets/app-ctrl/js/*.js",
			"<%=parentProjectPath%>/widgets/app-ctrl/js/*.js",
			"<%=clientCorePath%>/widgets/widget-ratings/js/*.js",
			"<%=clientCorePath%>/widgets/widget-loading-animation/js/*.js",
			"<%=clientCorePath%>/widgets/widget-story/js/*.js",

			"<%=clientCorePath%>/widgets/widget-teaser/js/handlebars-helpers.js",
			"<%=clientCorePath%>/widgets/widget-teaser/js/widget-api.js",
			"<%=clientCorePath%>/widgets/widget-teaser/js/widget-delegate.js",
			"<%=clientCorePath%>/widgets/widget-teaser/js/p-teasers-ctrl.js",
			"<%=clientCorePath%>/widgets/widget-teaser-multi/js/*.js",
			"<%=clientCorePath%>/widgets/widget-teaser-carousel/js/*.js",
			"<%=clientCorePath%>/widgets/widget-inbox-story/js/*.js",
			"<%=parentProjectPath%>/widgets/widget-story/js/*.js",
		],
		templates: [
			"<%=clientCorePath%>/pstory/<%=skin%>/templates/*.handlebars",
			"<%=clientCorePath%>/pstory/<%=skin%>/templates/teasers-templates/*.handlebars",
			"<%=clientCorePath%>/widgets/widget-loading-animation/templates/*.handlebars",
			"<%=clientCorePath%>/widgets/widget-story/templates/*.handlebars",
			"<%=clientCorePath%>/widgets/widget-teaser-multi/templates/*.handlebars",
			"<%=clientCorePath%>/widgets/widget-teaser-carousel/templates/*.handlebars",
			"<%=clientCorePath%>/widgets/widget-ratings/templates/*.handlebars",

			"<%=clientCorePath%>/widgets/widget-insight-teaser/templates/*.handlebars",
			"<%=clientCorePath%>/widgets/widget-inbox-story/templates/*.handlebars",
			"<%=clientCorePath%>/teasers/skin/templates/*.handlebars",
			"<%=clientCorePath%>/widgets/widget-insight-teaser/templates/*.handlebars",
			"<%=clientCorePath%>/js/templates/*.handlebars",
			"<%=parentProjectPath%>/js/<%=skin%>/templates/*.handlebars",
			"<%=clientCorePath%>/teasers/skin/templates/*.handlebars",
			"<%=clientCorePath%>/widgets/widget-insight-teaser/templates/*.handlebars",
			"<%=clientCorePath%>/js/templates/*.handlebars",
			"<%=parentProjectPath%>/js/<%=skin%>/templates/*.handlebars",
			"<%=parentProjectPath%>/teasers/skin/templates/*.handlebars",
			"<%=projectPath%>/teasers/skin/templates/*.handlebars",
			"<%=parentProjectPath%>/teasers/skin/templates/*.handlebars",
			"<%=projectPath%>/teasers/skin/templates/*.handlebars",
			"<%=projectPath%>/teasers/skin/templates/*.handlebars",
			"<%=projectPath%>/js/templates/*.handlebars"

		],
		teasersBlock: [
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-helpers/block-helper-base.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-helpers/block-vertical-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-helpers/block-multi-horizontal-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-helpers/block-single-horizontal-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-helpers/block-multi-vertical-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-helpers/block-pinGraph-chart-helper.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-helpers/block-chart-factory.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-base.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-text.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-image.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-date-box.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-bar-D3chart.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-pure-bar-chart.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-pure-pinGraph-chart.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-pie-chart.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-pie-chart-new.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-pinGraph-D3chart.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-line-chart.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/block-line-chart-new.js",
			"<%=clientCorePath%>/pstory/blocks/block-teasers/blocks-factory.js"
		],
		teasersSkin: [          
			"<%=parentProjectPath%>/teasers/skin/skin-setup<%=skin%>.js",
			"<%=parentProjectPath%>/teasers/skin/blocks/pstory-block-pie-chart-new.js",
			"<%=parentProjectPath%>/teasers/skin/blocks/pstory-block-pie-chart.js",
			"<%=parentProjectPath%>/teasers/skin/blocks/pstory-block-entity-selector.js",
			"<%=projectPath%>/teasers/skin/skin-setup.js",
		],
		cssInsightSimulator: [
			"<%=insightSimulatorPath%>/css/stylesheets/pstory-fonts.css",
			"<%=insightSimulatorPath%>/css/stylesheets/insightSimulator-base.css",
			"<%=insightSimulatorPath%>/css/stylesheets/insightSimulator-<%=channel%>.css",
			"<%=insightSimulatorPath%>/css/stylesheets/insightSimulator-story.css",
			"<%=insightSimulatorPath%>/css/stylesheets/insightSimulator-teaser.css",
			"<%=projectPath%>/insightSimulator/css/stylesheets/insightSimulator-story.css",
			"<%=projectPath%>/insightSimulator/css/stylesheets/insightSimulator-teaser.css"
		],
	}
	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),
		jsClientPath: '../../',
		skin : "",//"2018_1",
		channel: grunt.config('channel'),
		platform: grunt.config('platform'),
		clientBuildPath: "build/<%=platform%><%=channel%>",
		clientCorePath: process.env.PERSONETICS_CLIENT_CORE,
		parentProjectPath: "<%=clientCorePath%>/projects/product",
		projectName: "<%=pkg.projectName%>",
		projectPath: "<%=jsClientPath%>/projects/<%=projectName%>",
		releaseName: grunt.config('releaseName'),
		widgetVersion: "<%=pkg.widgetVersion%>",
		apiVersion: "<%=pkg.apiVersion%>",
		skinCtrlClass: "<%=pkg.skinCtrlClass%>",
		teasersProjectPath: "<%=jsClientPath%>/projects/teasers",

		insightSimulatorPath: "<%=parentProjectPath%>/insightSimulator",
		insightSimulatorBuildPath: "<%=clientBuildPath %>/insightSimulator-<%=channel%>-<%=widgetVersion%>",

		nodeServer: process.env.PERSONETICS_CLIENT_NODE + '/projects/<%=projectName%>/<%=channel%>',

        compress: {
            insight_simulator: {
                options: {
                    archive: './zip/<%=projectName%>-insight-simulator-<%=widgetVersion%>.zip'
                },
                files: [
                    {expand: true, cwd: 'build/release/default/insightSimulator-default-<%=widgetVersion%>/', src: ['**'], dest: ''}, // makes all src relative to cwd
                ]
            },
            default: {
                options: {
                    archive: './zip/<%=projectName%>-default-<%=widgetVersion%>.zip'
                },
                files: [
                    {expand: true, cwd: 'build/release/default/', src: ['home/**','personetics-<%=projectName%>/**'], dest: ''}, // makes all src relative to cwd
                ]
            }
        },

		// clean the build folder
		clean: {
			build: [
				'<%=clientBuildPath %>',
				'<%=nodeServer %>'
			],
			templatesJs:[
				"<%=projectPath%>/js/templates/js",
			],
			cssMobile: [
				'<%=parentProjectPath%>/css/stylesheets',
				'<%=projectPath%>/teasers/skin/css/stylesheets',
				'<%=clientCorePath%>/skin/stylesheets',
				'<%=projectPath%>/css/stylesheets',
				'<%=clientCorePath%>/utils/animations/carousel-animation/*.css',
				'<%=parentProjectPath%>/teasers/skin/css/stylesheets',
				// '<%=jsClientPath%>/projects/<%=projectName%>/product-tablet/css/stylesheets',
				'<%=insightSimulatorPath%>/css/sytylesheets',
			],
			variables:[
				'<%=projectPath%>/css/sass/_variables-build.scss',
				'<%=parentProjectPath%>/css/<%=skin%>/sass/_variables-build.scss',
				'<%=parentProjectPath%>/css/sass/_variables-build.scss',
				'<%=clientCorePath%>/skin/<%=skin%>/sass/_variables-build.scss',
				'<%=clientCorePath%>/skin/sass/_variables-build.scss'
			],	
			options: {
				force: true
			}
		},

		// process app html templates
		replace: {
			default: {
				options: {
					patterns: [{
						json: {
							'PROD_FOLDER': '<%=releaseName%>',
							'WIDGET_VERSION': '<%=widgetVersion %>',
							'FILE_EXTENTION': '<%=projectName %>',
							"MINIFIED_SUFFIX": "<%=grunt.config('minifiedSuffix') %>",
							'LANG': 'en',
							'HOME_INCLUDE': 'home',
							'INDEX_PAGE_NAME': 'home',
							'CHANNEL': '-mobile'
						}
					}]
				},
				files: [
					{
						expand: true,
						flatten: true,
						src: ['<%=parentProjectPath%>/home/template/home.html'],
						dest: '<%=clientBuildPath%>/home/'
					},
					{
						expand: true,
						flatten: true,
						src: ['<%=parentProjectPath%>/home/template/login.html'],
						dest: '<%=clientBuildPath%>/home/'
					},
					{
						expand: true,
						flatten: true,
						src: ['<%=insightSimulatorPath%>/insight-simulator.html'],
						dest: '<%=insightSimulatorBuildPath%>/'
					},
					{
						expand: true,
						flatten: true,
						src: ['<%=insightSimulatorPath%>/personetics-page.html'],
						dest: '<%=insightSimulatorBuildPath%>/insight-simulator/'
					}
				]
			},
			editor: {
				options: {
					patterns: [{
						json: {
							'PROD_FOLDER': '<%=releaseName %>',
							'WIDGET_VERSION': '<%=pkg.widgetVersion %>',
							'FILE_EXTENTION': '<%=projectName %>',
							"MINIFIED_SUFFIX": "<%=grunt.config('minifiedSuffix') %>",
							'LANG': 'en',
							'SKIN_CTRL_CLASS': '<%=skinCtrlClass %>'
						}
					}]
				},
				files: [
					{
						expand: true,
						flatten: true,
						src: ['<%=clientCorePath%>/projects/editor/samplePage/template/preview.htm',
							'<%=clientCorePath%>/projects/editor/samplePage/template/preview_teasers.htm'],
						dest: '<%=clientBuildPath%>/samplePage/'
					},
					{
						expand: true,
						flatten: true,
						src: ['<%=clientCorePath%>/projects/editor/samplePage/template/preview.js',
							'<%=clientCorePath%>/projects/editor/samplePage/template/preview_teasers.js'],
						dest: '<%=clientBuildPath%>/samplePage/js/'
					}

				]
			}
		},

		// process story and widgets handlebars templates
		handlebars: {
			templates: {
				src: [productsFileList.templates],
				dest: "<%=projectPath%>/js/templates/js/templatesTemp.js",
				options: {
					namespace: "Personetics.UI.Handlebars.templates",
					processName: function (filepath) {
						var regex = new RegExp(".*\/");
						var name = filepath.replace(regex, "").replace(".handlebars", "");
						return name;
					}
				}
			}
		},

		// process sass files
		compass: {
			//sass files
			cssParentProject: {
				options: {
					sassDir: '<%=parentProjectPath%>/css/<%=skin%>/sass',
					cssDir: '<%=parentProjectPath%>/css/stylesheets'
				}
			},
			cssTeasers: {
				options: {
					sassDir: '<%=projectPath%>/teasers/skin/css/sass',
					cssDir: '<%=projectPath%>/teasers/skin/css/stylesheets'
				}
			},
			cssCoreProject: {
				options: {
					sassDir: '<%=clientCorePath%>/skin/<%=skin%>/sass',
					cssDir: '<%=clientCorePath%>/skin/stylesheets'
				}
			},

			css: {
				options: {
					sassDir: '<%=projectPath%>/css/sass',
					cssDir: '<%=projectPath%>/css/stylesheets'
				}
			},
			cssUtils: {
				options: {
					sassDir: '<%=clientCorePath%>/utils/animations/carousel-animation',
					cssDir: '<%=clientCorePath%>/utils/animations/carousel-animation'
				}
			},
			//teasers sass files
			cssParentTeasers: {
				options: {
					sassDir: '<%=parentProjectPath%>/teasers/skin/css/<%=skin%>/sass',
					cssDir: '<%=parentProjectPath%>/teasers/skin/css/stylesheets'
				}
			},
			// insightSimulator sass files
			cssInsightSimulator: {
				options: {
					sassDir: '<%=insightSimulatorPath%>/css/<%=skin%>/sass',
					cssDir: '<%=insightSimulatorPath%>/css/stylesheets'
				}
			},
		},

		// create package files
		concat: {
			jsTemplates: {
				options: {
					stripBanners: true,
					banner: '(function(window, Personetics, $, Handlebars, undefined){' +
						grunt.util.linefeed +
						grunt.util.linefeed,
					footer: '})(window, window.Personetics = window.Personetics || {}, jQuery, window.Personetics.UI.Handlebars);' +
						grunt.util.linefeed +
						grunt.util.linefeed
				},
				files: [{
					src: "<%=projectPath%>/js/templates/js/templatesTemp.js",
					dest: "<%=projectPath%>/js/templates/js/templates.js"
				}]
			},
			jsStory: {
				options: {
					stripBanners: true,
					banner: '/**' +
						grunt.util.linefeed +
						' * @preserve Personetics story widget ' +
						grunt.util.linefeed +
						' * API version: v<%= apiVersion %>' +
						grunt.util.linefeed +
						' * Client version: <%= widgetVersion %>' +
						grunt.util.linefeed +
						' * Generated on: <%= grunt.template.today("mm-dd-yy") %>' +
						grunt.util.linefeed +
						' */' +
						grunt.util.linefeed +
						grunt.util.linefeed
				},
				files: [{
					src: [productsFileList.blockProcessor,
					productsFileList.pstory,
					productsFileList.pstoryResponsive,
					productsFileList.widgets,
					productsFileList.skin,
					productsFileList.teasersBlock,
					productsFileList.teasersSkin
					],
					dest: '<%=clientBuildPath%>/<%=releaseName %>/js/personetics-widget.js'
				}]
			},
			// Personetics API
			jsApi: {
				options: {
					stripBanners: true,
					banner: '/**' +
						grunt.util.linefeed +
						' * @preserve Personetics story API ' +
						grunt.util.linefeed +
						' * API version: v<%= apiVersion %>' +
						grunt.util.linefeed +
						' * Client version: <%= widgetVersion %>' +
						grunt.util.linefeed +
						' * Generated on: <%= grunt.template.today("mm-dd-yy") %>' +
						grunt.util.linefeed +
						' */' +
						grunt.util.linefeed +
						grunt.util.linefeed
				},
				files: [{
					src: [productsFileList.api],
					dest: '<%=clientBuildPath%>/<%=releaseName %>/js/personetics-api.js'
				}]
			},
			// third party
			jsVendors: {
				options: {
					stripBanners: true,
					banner: '/**' +
						grunt.util.linefeed +
						' * @preserve Personetics story Vendors ' +
						grunt.util.linefeed +
						' * API version: v<%= apiVersion %>' +
						grunt.util.linefeed +
						' * Client version: <%= widgetVersion %>' +
						grunt.util.linefeed +
						' * Generated on: <%= grunt.template.today("mm-dd-yy") %>' +
						grunt.util.linefeed +
						' */' +
						grunt.util.linefeed +
						grunt.util.linefeed
				},
				files: [{
					src: [productsFileList.vendor],
					dest: '<%=clientBuildPath%>/<%=releaseName %>/js/personetics-vendors.js'
				}]
			},
			// editor third party
			jsVendorsEditor: {
				options: {
					stripBanners: true,
					banner: '/**' +
						grunt.util.linefeed +
						' * @preserve Personetics story Vendors ' +
						grunt.util.linefeed +
						' * API version: v<%= apiVersion %>' +
						grunt.util.linefeed +
						' * Client version: <%= widgetVersion %>' +
						grunt.util.linefeed +
						' * Generated on: <%= grunt.template.today("mm-dd-yy") %>' +
						grunt.util.linefeed +
						' */' +
						grunt.util.linefeed +
						grunt.util.linefeed
				},
				files: [{
					src: [productsFileList.vendor, productsFileList.vendorEditor],
					dest: '<%=clientBuildPath%>/<%=releaseName %>/js/personetics-vendors.js'
				}]
			},
			css: {
				files: [{
					src: [productsFileList.css],
					dest: '<%=clientBuildPath%>/<%=releaseName %>/css/personetics.css'
				}]
			},
			cssVariables: {
				files: [{
					src: [productsFileList.cssVariables],
					dest: '<%=projectPath%>/css/sass/_variables-build.scss'
				}]
			},
			cssInsightSimulator: {
				files: [{
					src: [productsFileList.cssInsightSimulator],
					dest: '<%=insightSimulatorBuildPath%>/insight-simulator/css/insightSimulator.css'
				}]
			}
		},

		// minify css package files
		cssmin: {
			minifyCss: {
				options: {
					'lineBreak': 7000
				},
				src: ['<%=clientBuildPath%>/<%=releaseName %>/css/personetics.css'],
				dest: '<%=clientBuildPath%>/<%=releaseName %>/css/personetics.min.css'
			}
		},
		// minify js package files
		run: {
            commands: {
                exec: 'node jsMinifier.js <%=clientBuildPath%>/<%=releaseName %>/js',
            }
        },
		http: {
			minifyJs: {
				options: {
					url: 'https://closure-compiler.appspot.com/compile',
					method: 'POST',
					form: {
						output_info: 'compiled_code',
						output_format: 'text',
						compilation_level: 'SIMPLE_OPTIMIZATIONS',
						warning_level: 'default'
					},
					sourceField: 'form.js_code'
				},
				files: {
					'<%=clientBuildPath%>/<%=releaseName %>/js/personetics-api.min.js': '<%=clientBuildPath%>/<%=releaseName %>/js/personetics-api.js',
					'<%=clientBuildPath%>/<%=releaseName %>/js/personetics-widget.min.js': '<%=clientBuildPath%>/<%=releaseName %>/js/personetics-widget.js',
					'<%=clientBuildPath%>/<%=releaseName %>/js/personetics-vendors.min.js': '<%=clientBuildPath%>/<%=releaseName %>/js/personetics-vendors.js'
				}
			}
		},

		// misc copy tasks
		copy: {
			sassVariables: {
				files: [
					{
						cwd: '<%=projectPath%>/css/sass/',
						src: "_variables-build.scss",
						dest: '<%=clientCorePath%>/skin/sass',
						expand: true
					},
					{
						cwd: '<%=projectPath%>/css/sass/',
						src: "_variables-build.scss",
						dest: '<%=clientCorePath%>/skin/<%=skin%>/sass',
						expand: true
					},
					{
						cwd: '<%=projectPath%>/css/sass/',
						src: "_variables-build.scss",
						dest: '<%=parentProjectPath%>/css/sass/',
						expand: true
					},
					{
						cwd: '<%=projectPath%>/css/sass/',
						src: "_variables-build.scss",
						dest: '<%=parentProjectPath%>/css/<%=skin%>/sass/',
						expand: true
					},
					{
						cwd: '<%=projectPath%>/css/sass/',
						src: "_variables-build.scss",
						dest: '<%=projectPath%>/css/sass/',
						expand: true
					}
				]
			},
			packageFiles: {
				files: [
					{
						cwd: '<%=clientCorePath%>/resources/fonts/Roboto/',
						src: "*.ttf",
						dest: '<%=clientBuildPath%>/<%=releaseName %>/resources/fonts/',
						expand: true
					},
					{
						cwd: '<%=clientCorePath%>/resources/fonts/OpenSans/',
						src: "*.ttf",
						dest: '<%=clientBuildPath%>/<%=releaseName %>/resources/fonts/',
						expand: true
					},
					{
						cwd: '<%=projectPath%>/css/fonts/',
						src: "*.ttf",
						dest: '<%=clientBuildPath%>/<%=releaseName %>/resources/fonts/',
						expand: true
					},
					{
						cwd: "<%=clientCorePath%>/resources/images/",
						src: "**",
						dest: '<%=clientBuildPath%>/<%=releaseName %>/resources/images/',
						expand: true
					},
					{
						cwd: "<%=jsClientPath%>/resources/images/",
						src: "**",
						dest: '<%=clientBuildPath%>/<%=releaseName %>/resources/images/',
						expand: true
					},
					{
						cwd: "<%=projectPath%>/teasers/skin/images/",
						src: "**",
						dest: '<%=clientBuildPath%>/<%=releaseName %>/resources/images/',
						expand: true
					},
					{
						cwd: "<%=clientCorePath%>/teasers/skin/images/",
						src: "**",
						dest: '<%=clientBuildPath%>/<%=releaseName %>/resources/images/',
						expand: true
					}
				]
			},
			editorSamplePageFiles: {
				files: [
					{
						cwd: "<%=clientCorePath%>/projects/editor/samplePage/files/css/",
						src: "*.css",
						dest: '<%=clientBuildPath%>/samplePage/css/',
						expand: true
					},
					{
						cwd: "<%=clientCorePath%>/projects/editor/samplePage/files/js/",
						src: ["preview.js", "preview_teasers.js", "jquery-2.2.4.min.js"],//"preview_singal_teaser.js"],
						dest: '<%=clientBuildPath%>/samplePage/js/',
						expand: true
					},
					{
						cwd: '<%=clientCorePath%>/data/',
						src: ["predefinedInsights.js", "predefinedInsights-payload.js", "predefinedInsightsTeasers.js", "newPredefinedInsights.js"],
						dest: '<%=clientBuildPath%>/samplePage/js/',
						expand: true
					}]
			},
			homeFiles: {
				files: [
					{
						cwd: '<%=parentProjectPath%>/home/files/css/',
						src: "*.css",
						dest: '<%=clientBuildPath%>/home/css/',
						expand: true
					},
					{
						cwd: '<%=parentProjectPath%>/home/files/js/',
						src: "*.js",
						dest: '<%=clientBuildPath%>/home/js/',
						expand: true
					},
					{
						cwd: '<%=parentProjectPath%>/teasers/skin/images/',
						src: "**",
						dest: '<%=clientBuildPath%>/<%=releaseName %>/resources/images/',
						expand: true
					}
				]
			},
			// build package to solution git
			solution: {
				cwd: '<%=jsClientPath%>/projects/<%=projectName%>/build',
				src: "**",
				dest: '<%=solutionWebBankPath%>',
				expand: true
			},
			// build package to node server project
			node: {
				cwd: '<%=clientBuildPath%>',
				src: "**",
				dest: '<%=nodeServer%>',
				expand: true
			},
			// editor build package to node server project
			nodeEditor: {
				cwd: '<%=clientBuildPath%>',
				src: "**",
				dest: '<%=nodeServer%>',
				expand: true
			},
			insightSimulator: {
				files: [
					{
						cwd: '<%=insightSimulatorPath%>/js/',
						src: "**",
						dest: '<%=insightSimulatorBuildPath%>/insight-simulator/js',
						expand: true
					},
					{
						cwd: '<%=insightSimulatorPath%>/resources/',
						src: "**",
						dest: '<%=insightSimulatorBuildPath%>/insight-simulator/resources',
						expand: true
					},
					{
						cwd: '<%=clientBuildPath%>/<%=releaseName %>/js/',
						src: "*",
						dest: '<%=insightSimulatorBuildPath%>/<%=releaseName%>/js/',
						expand: true
					},
					{
						cwd: '<%=clientBuildPath%>/<%=releaseName %>/css/',
						src: 'personetics.css',
						dest: '<%=insightSimulatorBuildPath%>/<%=releaseName%>/css/',
						expand: true
					},
					{
						cwd: '<%=jsClientPath%>/resources/fonts/Roboto/',
						src: "*.ttf",
						dest: '<%=insightSimulatorBuildPath%>/<%=releaseName%>/resources/fonts/',
						expand: true
					},
					{
						cwd: '<%=clientCorePath%>/resources/fonts/OpenSans/',
						src: "*.ttf",
						dest: '<%=insightSimulatorBuildPath%>/<%=releaseName%>/resources/fonts/',
						expand: true
					},{
						cwd: '<%=clientBuildPath%>/<%=releaseName %>/resources/fonts/',
						src: ["*.TTF","*.ttf"],
						dest: '<%=insightSimulatorBuildPath%>/<%=releaseName%>/resources/fonts/',
						expand: true
					},
					{
						cwd: '<%=clientBuildPath%>/<%=releaseName %>/resources/images/',
						src: "**",
						dest: '<%=insightSimulatorBuildPath%>/<%=releaseName%>/resources/images/',
						expand: true
					},
				]
			},
			copySolutionProcessor: {
				files: [
					{
						cwd: '<%=projectPath%>/processor-server/',
						src: "**",
						dest: '<%=clientBuildPath%>/',
						expand: true
					},
				]
			}
		},

		datauri: {
			teasers: {
				options: {
					//classPrefix: 'perso-teaser-template .'
				},
				src: [
					'<%=parentProjectPath%>/teasers/skin/images/teaser_images/*',
					'<%=parentProjectPath%>/teasers/skin/images/*.*'
				],
				dest: [
					"<%=insightSimulatorBuildPath%>/insight-simulator/personetics.css",
				]
			},
			story: {
				options: {
					//classPrefix: 'perso-teaser-template .'
				},
				src: [
					//'<%=jsClientPath%>/projects/<%=projectName%>/teasers/skin/images/teaser_images/*',
					//'<%=jsClientPath%>/projects/<%=projectName%>/teasers/skin/images/*.*'
					"<%=insightSimulatorBuildPath%>/insight-simulator/resources/images/*.*",
					"<%=insightSimulatorBuildPath%>/personetics-<%=projectName%>/resources/images/*.*",
					"<%=insightSimulatorBuildPath%>/personetics-<%=projectName%>/resources/images/account_selector/*.*",
					"<%=insightSimulatorBuildPath%>/personetics-<%=projectName%>/resources/images/charts/*.*",
					"<%=insightSimulatorBuildPath%>/personetics-<%=projectName%>/resources/images/compare-block/*.*",
					"<%=insightSimulatorBuildPath%>/personetics-<%=projectName%>/resources/images/icons/*.*",
					"<%=insightSimulatorBuildPath%>/personetics-<%=projectName%>/resources/images/rating/*.*",
					'<%=parentProjectPath%>/teasers/skin/images/teaser_images/*.*'
				],
				dest: [
					"<%=insightSimulatorBuildPath%>/insight-simulator/personeticsTemp.css",
				]
			}
		},
		css_important: {
			dist: {
				options: {
					minified: true
				},
				src: ["<%=insightSimulatorBuildPath%>/insight-simulator/personeticsTemp.css"],
				dest: "<%=insightSimulatorBuildPath%>/insight-simulator/personetics.css"
			},
			dev: {
				src: ["<%=insightSimulatorBuildPath%>/insight-simulator/personeticsTemp.css"],
				dest: "<%=insightSimulatorBuildPath%>/insight-simulator/personetics.css"
			}
		}
	});

	grunt.loadNpmTasks('grunt-datauri');
    grunt.loadNpmTasks('grunt-css-important');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-http');
	grunt.loadNpmTasks('grunt-yui-compressor');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-handlebars');
	grunt.loadNpmTasks('grunt-replace');
	grunt.loadNpmTasks('grunt-run-grunt');
	grunt.loadNpmTasks('grunt-run');

	var tasksMap = {
		default: {
			replace: "replace:default",
			handlebars: "handlebars:templates",
			concatTemplates: "concat:jsTemplates",
			concatCSSVariables: "concat:cssVariables",
			concatStory: "concat:jsStory",
			concatAPI: "concat:jsApi",
			concatCSS: "concat:css",
			copyNode: "copy:node",
			concatVendors: "concat:jsVendors",
			copySamplePageFiles: "copy:homeFiles",
			copyPakageFiles: "copy:packageFiles",
			compass: ['compass:cssCoreProject', 'compass:cssParentTeasers', 'compass:cssTeasers', 'compass:cssParentProject', 'compass:css', 'compass:cssUtils', 'compass:cssInsightSimulator']
		},
		editor: {
			replace: "replace:editor",
			concatVendors: "concat:jsVendorsEditor",
			copyNode: "copy:nodeEditor",
			copySamplePageFiles: "copy:editorSamplePageFiles"
		},
		solutionProcessor: {
			copySolutionProcessor: "copy:copySolutionProcessor"
		},
		// returns the task to run based on api mode. If there isn't an api specific task, return default task
		getValue: function (valueName, apiMode) {
			if (this.hasOwnProperty(apiMode)) {
				var apiModeVars = this[apiMode];
				if (apiModeVars.hasOwnProperty(valueName))
					return apiModeVars[valueName];
				else
					return this.default[valueName];
			}
			else
				return "";
		}
	};

	// initialize and return generic grunt options
	var getTaskParams = function getTaskParams() {
		var apiMode = grunt.config('apiMode') || "default";
		var replaceTaskVar = "default";

		// default package name
		grunt.config('releaseName', "<%=pkg.releaseName%>");

		// default platform is empty
		var platform = grunt.config('platform');
		if (!platform)
			grunt.config('platform', "");

		// default channel is mobile
		var channel = grunt.config('channel');
		if (!channel)
			grunt.config('channel', "default");

		// default minified suffix is empty
		var minifiedSuffix = grunt.config('minifiedSuffix');
		if (!minifiedSuffix)
			grunt.config('minifiedSuffix', "");

		switch (apiMode) {
			case "editor":
				grunt.config('releaseName', "<%=pkg.editorReleaseName%>");
				replaceTaskVar = "editor";
				break;
			default:
				replaceTaskVar = "default";
		}

		var result = {
			apiMode: apiMode,
			replaceTaskVar: replaceTaskVar
		};

		return result;
	};
	/****** Default task ******/
	grunt.registerTask('default', function () {
		var params = getTaskParams();
		var apiMode = params.apiMode;
		var replaceTaskVar = params.replaceTaskVar;

		grunt.task.run('clean');
		grunt.task.run(tasksMap[replaceTaskVar].replace);
		grunt.task.run(tasksMap.getValue("handlebars", apiMode));
		grunt.task.run(tasksMap.getValue("concatTemplates", apiMode));
		grunt.task.run(tasksMap.getValue("concatCSSVariables", apiMode));
		grunt.task.run('copy:sassVariables');
		grunt.task.run(tasksMap.getValue("compass", apiMode));
		grunt.task.run(tasksMap.getValue("concatStory", apiMode));

		grunt.task.run(tasksMap.getValue("concatAPI", apiMode));
		grunt.task.run(tasksMap.getValue("concatVendors", apiMode));
		grunt.task.run(tasksMap.getValue("concatCSS", apiMode));
		var mode = grunt.config('mode');
		if (mode!='dev') {
			grunt.task.run('cssmin');
			grunt.task.run('run:commands');
		}
		grunt.task.run(tasksMap.getValue("copyPakageFiles", apiMode));
		grunt.task.run(tasksMap.getValue("copySamplePageFiles", apiMode));
		grunt.task.run('copy:insightSimulator');
		grunt.task.run('concat:cssInsightSimulator');
		grunt.task.run(tasksMap.getValue("copyNode", apiMode));
		grunt.task.run('datauri');
        grunt.task.run('css_important:dev');
	});

	/****** Editor tasks ******/
	grunt.registerTask('editor', function () {
		grunt.config('apiMode', "editor");
		grunt.config.set('channel', "directive");
		grunt.task.run('default');
	});

	/****** Dev tasks ******/
	grunt.registerTask('dev', function () {
		grunt.config('apiMode', "default");
		grunt.config.set('mode', "dev");
		grunt.task.run('default');
	});

	/****** Processor tasks ******/
	grunt.registerTask('processor', function () {
		grunt.config('channel', "processor");
		grunt.task.run(tasksMap.getValue("copySolutionProcessor", "solutionProcessor"));
	});

	/****** Mobile tasks ******/
	grunt.registerTask('mobile', function () {
		grunt.config.set('platform', 'release/');
		grunt.task.run('default');
		grunt.task.run('compress');
	});

	/****** Prod tasks ******/
	grunt.registerTask('prod', function () {
		grunt.config.set('platform', 'release/');
		grunt.task.run('default');
		grunt.task.run('editor');
		grunt.task.run('processor');
		grunt.task.run('compress');
	});
	
}