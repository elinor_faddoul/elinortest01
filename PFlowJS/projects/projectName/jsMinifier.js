const path = require('path');
const fs = require('fs-extra');
const compressor = require('node-minify');
 
 
const srcDir = process.argv[2];

function minifyJs(srcFile){
    
    console.log('minifyjs file =' + srcFile);    
    compressor.minify({  compressor: 'gcc',  input: srcFile,  output: srcFile.replace('.js', '.min.js'),  callback: function(err, min) {}});
}

function minifyDir(dir, filelist) {
    
    if(!dir) { console.log('no src folder was supplied!!!'); return;   }
    files = fs.readdirSync(dir);                       
    files.forEach(function(file) { if (file.split(".").pop() == "js")   minifyJs(path.join(dir, file)); });
}

minifyDir(srcDir);


