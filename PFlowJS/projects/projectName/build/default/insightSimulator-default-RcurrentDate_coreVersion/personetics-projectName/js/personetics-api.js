/**
 * @preserve Personetics story API 
 * API version: v2.0
 * Client version: RcurrentDate_coreVersion
 * Generated on: 10-15-19
 */

(function(window, Personetics, $, undefined){
    var cfg = function cfg() {
        this.config = {
            //api
            "protocolVersion": "2.5",
            "enableNotifyEvents": true,
            "eventDelayInterval":2000,


            //processor
            "useProjectDefaultCurrency": false,
            "supportHtmlTagInProcessor": true,
            "dateFormat":  "MM/DD/YY",
            "amountFormat":"###,###,###",
            "currencySymbol": "$",

            //Api's contexId
            "getInsights": "sampleTopRelevant",
            "getInboxInsights": "dashboard",
            "getInsightDetails": "dashboard",
            "getNumberOfInsights": "sampleNew",
            "getInsightRating": "dashboard",
            "sendEvents": "dashboard",
            "updateInsightRating":"dashboard",
            "updateInsightFeedback":"dashboard",
            "defaultCtxId": "dashboard"
        };
        this.agentMode = "agent";
    }

    cfg.prototype.setConfig = function setConfig(config) {
        for(var configId in config) {
            if(config.hasOwnProperty(configId)) {
                this.config[configId] = config[configId];
            }
        }
    };

    cfg.prototype.getConfig = function (configId) {
        var configValue = this.config[configId];
        return configValue;
    };

    cfg.prototype.getConfigValues = function(){
        return this.config;
    };

    cfg.prototype.getApiCtxId = function(apiType, overrideCtxId){
        if(overrideCtxId === this.agentMode){
            return overrideCtxId;
        }
        var ctxId = (overrideCtxId && overrideCtxId.length) ? overrideCtxId : this.getConfig(apiType);
        ctxId = ctxId ? ctxId : this.getConfig("defaultCtxId");
        return ctxId;
    };

    Personetics.projectConfiguration = new cfg();
})(window, window.Personetics = window.Personetics || {}, jQuery);

(function(window, Personetics, $, undefined){

    var map = function map() {
        this.images = {
            "teaser_images": {
                "default": "CG100",
                "CG100": "CG100",
                "Transport": "CG100",
                "Transportion": "CG100",
                "Transportation": "CG100",
                "CG1000": "CG1000",
                "Household": "CG1000",
                "CG10000": "CG10000",
                "Groceries": "CG10000",
                "Supermarkets": "CG10000",
                "CG2000": "CG2000",
                "Services": "CG700",
                "CG300": "CG300",
                "Utilities": "CG300",
                "CG3000": "CG3000",
                "Travel": "CG3000",
                "CG500": "CG500",
                "Entertainment": "CG500",
                "CG600": "CG600",
                "Shopping": "CG600",
                "CG6000": "CG6000",
                "Education": "CG6000",
                "CG700": "CG700",
                "Finances": "CG700",
                "Finance": "CG700",
                "CG8": "CG8",
                "Taxes": "CG8",
                "CG800": "CG800",
                "Dining": "CG800",
                "CG900": "CG900",
                "Health": "CG900",
                "HazarAttentionSign": "HazardAttentionSign",
                "HazardAttentionSign": "HazardAttentionSign",
                "IntroducePersonetics": "IntroducePersonetics",
                "PersoAutomation": "PersoAutomation",
                "PersoCards": "PersoCards",
                "PersoCurrency": "PersoCurrency",
                "PersoDeposit": "PersoDeposit",
                "PersoExplain": "PersoExplain",
                "Persofees": "Persofees",
                "PersoFees": "Persofees",
                "PersoMobileBanking": "PersoMobileBanking",
                "PersoMobileWallet": "PersoMobileWallet",
                "PersoQuiz": "PersoQuiz",
                "PersoRefund": "PersoRefund",
                "PersoSavings": "PersoSavings",
                "PersoSavingsLong": "PersoSavingsLong",
            },
            "account_selector": {
                "default": "cardImage",
                "0": "cardImage"
            },
            "icons": {
                "default": "welcome",
                "travel activity": "travel_activity",
                "spend activity": "spend_activity",
                "payments": "payments",
                "debits _ purchases": "debits_purchases",
                "credit_refunds": "credit_refunds",
                "collections_past due": "collections_past_due",
                "collection_past due": "collections_past_due",
                "balance_status": "balance_status",
                "account info": "account_info"
            }
        }
    };

    map.prototype.getImageClass = function getImageClass(imageId, file){
        var urlObj ={
            isAbsoluteURL:false,
            url:imageId
        };
        if(typeof imageId != undefined && imageId != null) {
            if (imageId.indexOf("http") == 0) {
                urlObj.isAbsoluteURL = true;
                return urlObj;
            }
        }
        if(typeof file != undefined && this.images.hasOwnProperty(file)){
            var images = this.images[file];
            urlObj.url = images.hasOwnProperty(imageId)? images[imageId]: images["default"];
            return urlObj;
        }
        Personetics.log("Personetics.imageMap.getText('" + imageId + "', '" + file + "') Error: unknown " + file);
        return urlObj;
    };

    map.prototype.overrideImages = function overrideImages(file, images) {
        if(!this.images.hasOwnProperty(file)) {
            this.images[file] = images;
        }
        else {
            for(var imageID in images) {
                if(images.hasOwnProperty(imageID)) {
                    this.images[file][imageID] = images[imageID];
                }
            }
        }
    };

    Personetics.imageMap = new map();

})(window, window.Personetics = window.Personetics || {}, jQuery);


(function (window, Personetics, undefined) {
	var persoDict = function persoDict() {
		this.monthNames = {
			"en-short": ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
				"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			en: ["January", "February", "March", "April", "May", "June",
				"July", "August", "September", "October", "November", "December"],
			he: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני",
				"יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
			"he-short": ["ינו'", "פבר'", "מרץ", "אפר'", "מאי", "יונ'",
				"יול'", "אוג'", "ספט'", "אוק'", "נוב'", "דצמ'"],
			"es-short": ["Ene", "Feb", "Mar", "Abr", "May", "Jun",
				"Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
			"es": ["enero", "febrero", "marzo", "abril", "mayo", "junio",
				"julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
			"fr": ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
				"Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
			"fr-short": ["Jan", "Fév", "Mar", "Avr", "Mai", "Jun",
				"Jul", "Aoû", "Sep", "Oct", "Nov", "Déc"],
			"th":["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
				"กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
			"th-short":["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.",
				"ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
			"de":["Januar", "Februar", "März", "April", "Mai", "Juni",
				"Juli", "August", "September", "Oktober", "November", "Dezember"],
			"de-short":["Jan.", "Feb.", "März", "Apr.", "Mai", "Jun.",
				"Jul.", "Aug.", "Sept.", "Okt.", "Nov.", "Dez."]
		};

		this.weekDays = {
			en: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			es: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
			de: ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"],
			fr:["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"]
		};
	}

	Personetics.pstoryDictionary = new persoDict();

})(window, window.Personetics = window.Personetics || {});

(function (window, Personetics, undefined) {
    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.persoHelper = {};

    Personetics.utils.persoHelper.constants = {
        ANDROID_VERSIONS :{
            OLD_VERSION_MIN_TARGET: 4.4,
            OLD_VERSION_MAX_TARGET: 5.0
        }
    }
    Personetics.utils.persoHelper.getAndroidVersionNumber = function getAndroidVersionNumber(){
        var androidVersionNumber;
        var androidVersionString = getAndroidVersion();
        if(androidVersionString){
            androidVersionNumber = parseFloat(getAndroidVersion())
        }
        return androidVersionNumber;
    };

    Personetics.utils.persoHelper.isOldAndroidVersion = function isOldAndriodVersion (){
        var androidVersion = this.getAndroidVersionNumber();
        var isOldAndroidVersion = false;

        if(typeof androidVersion !== 'undefined' && androidVersion > 0){
            if(androidVersion < this.constants.ANDROID_VERSIONS.OLD_VERSION_MAX_TARGET && androidVersion >= this.constants.ANDROID_VERSIONS.OLD_VERSION_MIN_TARGET){
                isOldAndroidVersion = true;
            }
        }
        return isOldAndroidVersion;
    };

    var getAndroidVersion = function getAndroidVersion(){
        var userAgent = navigator.userAgent.toLowerCase();
        var match = userAgent.match(/android\s([0-9\.]*)/);
        return match ? match[1] : false;
    };
})(window, window.Personetics = window.Personetics || {});
(function (window, Personetics, $, undefined) {

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.accessibility = {};
    Personetics.utils.accessibility.isFocusable = function isFocusable(element) {

        //check if element is hidden from screen reader.
        if ($(element).is('[aria-hidden="true"], [aria-hidden="true"] *'))
            return false;
        //retrieve only text not nested in child tags.
        var hasText = $(element).contents().not($(element).children()).text().trim().length > 0;
        if ($(element).is(":input, [role], [aria-label], [aria-labelledby]") || hasText)
            return true;
        return false;

    };
    Personetics.utils.accessibility.firstChildFocusable = function firstChildFocusable(element) {

        if(typeof element == 'undefind' || element == null || element.length == 0){
            return false
        }
        var me = this;
        var elementChildren = element.find('*');
        var result = false;
        $.each(elementChildren, function(i, elem){
            result = me.isFocusable(elem);
            if(result == true){
                result = elem;
                return false;
            }
        });
        return result;

    };
    Personetics.utils.accessibility.formatAccessibleDate = function formatAccessibleDate (dateStr) {
        var lang = Personetics.processor.PStoryConfig.getConfig("lang");
        var formattedDate;
        if (lang === 'he' || lang === 'HE' || lang === 'He') {
            var day = Personetics.utils.date.dayName(dateStr, lang);
            var dayNumber = Personetics.utils.date.dayNumber(dateStr);
            var dateText = "";
            var month = Personetics.utils.date.monthName(dateStr, lang);
            var year = Personetics.utils.date.yearNumber(dateStr);

            (dayNumber > 10) ? dateText += "ה" + day + " " : dateText += day + " ";
            dateText += "ל" + month + " " + year;
            formattedDate = dateText;
        }
        return formattedDate;
    };

    Personetics.utils.accessibility.trackFocus = function trackFocus(element) {
        if (typeof element === "undefined" || element.length === 0) {
            return;
        }
        var usingMouse;
        var el = element.get(0);
        var preFocus = function (event) {
            usingMouse = (event.type === 'mousedown');
        };

        var addFocus = function (event) {
            if (usingMouse) {
                var className =  event.target.getAttribute("class");
                event.target.setAttribute("class",className +' personetics-mouse-focus');
            }
        };

        var removeFocus = function (event) {
            var className =  event.target.getAttribute("class");
            if(className !== null) {
                className = className.replace(/ personetics-mouse-focus/g, '');
                event.target.setAttribute("class", className);
            }
        };

        var bindEvents = function () {

            el.removeEventListener('keydown', preFocus);
            el.removeEventListener('mousedown', preFocus);
            el.removeEventListener('focusin', addFocus);
            el.removeEventListener('focusout', removeFocus);

            el.addEventListener('keydown', preFocus);
            el.addEventListener('mousedown', preFocus);
            el.addEventListener('focusin', addFocus);
            el.addEventListener('focusout', removeFocus);
        };
        bindEvents();
    };
    Personetics.utils.accessibility.addAltTextToElement = function addAltTextToElement(element, altText){
        var accessibilityElementWrapper = $('<div class="perso-accessibility-wrapper perso-accessibility-account-selector"></div>');
        var accessibilityElement = $('<span class="perso-accessibility-read"  role="text">'+ altText +'</span>');
        accessibilityElementWrapper.append(accessibilityElement);
        $(element).prepend(accessibilityElementWrapper);
        accessibilityElement.height($(element).outerHeight());
        accessibilityElement.width($(element).width());
    };

    Personetics.utils.accessibility.putFocus = function putFocus(elementForFocus) {
        var $element = $(elementForFocus);
        var focusInterval = 10; // ms, time between function calls
        var focusTotalRepetitions = 10; // number of repetitions

        $element.attr('tabindex', '0');
        $element.blur();

        var focusRepetitions = 0;
        var interval = window.setTimeout(function () {
            $element.focus();
        }, focusInterval);
    };

})(window, window.Personetics = window.Personetics || {}, jQuery);
(function(window, Personetics, undefined){

	Personetics.utils = Personetics.utils ? Personetics.utils : {};
	Personetics.utils.assert = {};
	Personetics.utils.assert.isUndefined = function isUndefined(obj) {
		return typeof obj === 'undefined';
	};

	Personetics.utils.assert.isNullOrUndefined = function isNullOrUndefined(obj) {
		return typeof obj === 'undefined' || obj == null;
	};

	Personetics.utils.assert.isTrue = function isTrue(obj) {
		var result = !Personetics.utils.assert.isUndefined(obj);
		return result && obj == true;
	};

	Personetics.utils.assert.isDefined = function isDefined(obj, nullable) {
		var result = !Personetics.utils.assert.isUndefined(obj);
		if(result && !Personetics.utils.assert.isUndefined(nullable) && nullable != null) {
			if(!nullable)
				result = obj != null;
		}
		return result;
	};

	Personetics.utils.assert.AssertIsDefined = function AssertIsDefined(obj, errorMessage) {
		if(Personetics.utils.assert.isNullOrUndefined(obj))
		{
			if(errorMessage)
				Personetics.utils.PLogger.error(errorMessage, errorMessage);
			else
				Personetics.utils.PLogger.error("Object is undefined", "Object is undefined");
		}
	};

	Personetics.utils.assert.assignDefaultValue = function assignDefaultValue(variable, defaultValueToSet) {
		var result = null;
		if (Personetics.utils.assert.isUndefined(variable))
			result = defaultValueToSet;
		else
			result = variable;
		return result;
	};

	Personetics.utils.assert.isObject = function isObject(obj)
	{
		return typeof obj === 'object';
	};

	Personetics.utils.assert.isString = function isString(obj)
	{
		return typeof obj === 'string';
	};

	Personetics.utils.assert.isArray = function isArray(obj)
	{
		return Object.prototype.toString.call(obj) === '[object Array]';
	};

	Personetics.utils.assert.isFunction = function isFunction(obj)
	{
		return typeof obj === 'function';
	};

	Personetics.utils.assert.exists =  function (first, namespace) {
		var tokens = namespace.split('.');

		var obj = first;

		for (var i = 1; i < tokens.length; i++) {
			obj = (obj == "undefined") ? obj : obj[tokens[i]];
		}

		return obj;
	};

	Personetics.utils.assert.AssertIsNullOrUndefined = function(obj, message){
		
	};

})(window, window.Personetics = window.Personetics || {});
(function (window, Personetics, $, undefined) {
    var d3 = Personetics.persoD3 || window.d3;
    Personetics.utils = Personetics.utils || {};
    Personetics.utils.d3Extensions = {};

    var persoHelper = Personetics.utils.persoHelper;


    function appendElement(parentElm, children) {
        $.each(children, function (i, elm) {
            if (typeof elm.tagName == 'undefined') {
                var el = d3.select(parentElm).append("tspan");
                el.text($(elm).text());
                return;
            }
            var el = d3.select(parentElm).append(elm.nodeName.toLowerCase());
            var attrs = elm.attributes;
            $.each(attrs, function (j, attr) {
                el.attr(attr.name, attr.value);
            });
            if ($(elm).children().length > 0) {
                appendElement(el[0][0], $(elm).get(0).childNodes);
            }
            else {
                el.text($(elm).text());
            }
        });
    };

    function getBullet(list_class) {
        var asciCode = 32;
        switch (list_class) {
            case 'disc': asciCode = 9679; break;
            case 'square': asciCode = 9632; break;
            case 'circle': asciCode = 9675; break;
            case 'check-mark': asciCode = 10003; break;
            case 'arrow-right': asciCode = 10158; break;
            case 'none':
            default: asciCode = 32; break;
        }
        return '<tspan>' + String.fromCharCode(asciCode) + '</tspan>';
    };

    function renderLabelsText(value) {
        var d3Label = value;
        if (d3Label.indexOf("<ul") == 0) {
            var listBullet = $(d3Label).get(0).classList[1];
            var bullet = getBullet(listBullet);
            d3Label = d3Label.replace(/<ul/g, '<tspan');
            d3Label = d3Label.replace(/<\/ul>/g, '</tspan>');
            d3Label = d3Label.replace(/<li/g, '<tspan>' + bullet + '<tspan li="true"');
            d3Label = d3Label.replace(/<\/li>/g, '</tspan></tspan>');
        }
        d3Label = d3Label.replace(/<u/g, '<tspan text-decoration="underline"');
        d3Label = d3Label.replace(/<\/u>/g, '</tspan>');
        d3Label = d3Label.replace(/<b|<strong/g, '<tspan style="font-weight: bold"');
        d3Label = d3Label.replace(/<\/b>|<\/strong>/g, '</tspan>');
        d3Label = d3Label.replace(/<i|<em/g, '<tspan font-style="italic"');
        d3Label = d3Label.replace(/<\/i>|<\/em>/g, '</tspan>');
        d3Label = d3Label.replace(/<span|<div|<p/g, '<tspan');
        d3Label = d3Label.replace(/<\/(span|div|p)/g, '</tspan');
        d3Label = '<tspan>' + d3Label + '</tspan>'

        appendElement(this, $(d3Label));
    };

    Personetics.utils.d3Extensions.html = function (value) {
        return this.each(typeof value === "function" ? function () {
            v = value.apply(this, arguments);
            if(typeof v != "string")
                v = d3.select(v);
            renderLabelsText.call(this, v);
        }:function () {
            renderLabelsText.call(this, value);
        });
       // return this;
    };
    d3.selection.prototype.html = Personetics.utils.d3Extensions.html;
    Personetics.utils.d3Extensions.text = function text(value) {
        return arguments.length ? this.each(typeof value === "function" ? function () {
            var v = value.apply(this, arguments);
            v = $(v).length > 0 ? $(v)[0].innerText : v;
            this.textContent = v == null ? "" : v;
        } : value == null ? function () {
            this.textContent = "";
        } : function () {
            this.textContent = $(value).length > 0 ? $(value)[0].innerText : value;
        }) : this.textContent;
    };
    Personetics.utils.d3Extensions.last = function () {
        var last = this.size() - 1;
        return d3.select(this[0][last]);
    }
    d3.selection.prototype.last = Personetics.utils.d3Extensions.last;

    if (typeof persoHelper !== 'undefined' && (persoHelper.isOldAndroidVersion())) {
        // In old android versions override d3 "text" function with d3-extension "text" function.
        d3.selection.prototype.text = Personetics.utils.d3Extensions.text;
    }
})(window, window.Personetics = window.Personetics || {}, jQuery);

(function (window, Personetics, undefined) {

    var monthNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    var monthNames = Personetics.pstoryDictionary.monthNames;
    var daysName = Personetics.pstoryDictionary.weekDays;

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.date = {};

    Personetics.utils.date.getStaticDate = function getStaticDate(dateStr){
        var staticDateStr = dateStr.split('.')[0].replace(/T/g, ' ');
        staticDateStr = staticDateStr.split('-').join('/');
        return new Date(staticDateStr);
    };
    Personetics.utils.date.dayName = function dayName(dateStr, lang){
        var dayNames = daysName[lang];
        var dayNumber = this.dayNumber(dateStr);
        var dayName;
        if(dayNumber <= 10){
            dayName = dayNames [dayNumber - 1];
        } else {
            dayName = dayNumber;
        }
        return dayName;
    };
    Personetics.utils.date.monthName = function monthName(dateStr, lang, short) {
        if (!(parseInt(dateStr) == dateStr)) {/**safari bug */
            var d = this.getStaticDate(dateStr);
            var month = d.getMonth();
            if (month != undefined && month != null && !isNaN(month)) {
                month = d.getMonth();
            }
        }
        else {
            try {
                var dn = parseInt(dateStr);
                if (dn >= 1 && dn <= 12) {
                    month = dn - 1;
                }
                else {
                    return dateStr;
                }
            } catch (e) {
                return dateStr;
            }
        }
        if (short == true)
            lang = lang + "-short";
        var names = monthNames[lang];
        if (!names) {
            if (short == true)
                names = monthNames["en-short"];
            else
                names = monthNames["en"];
        }

        return names[month];
    };

    Personetics.utils.date.dayNumber = function dayNumber(dateStr, dayFormat) {
        var parsedDateStr = dateStr.split('.')[0].replace(/T/g, ' ');
        parsedDateStr = parsedDateStr.split('-').join('/');
        dateStr = (isNaN(Date.parse(parsedDateStr))) ? dateStr : parsedDateStr;
        var result = dateStr;
        var date = this.getStaticDate(dateStr);
        var day = date.getDate();
        if (!isNaN(day))
            result = date.getDate();
        if (dayFormat && dayFormat == "DD") {
            if (result < 10)
                result = "0" + result;
        }
        return result;
    };

    Personetics.utils.date.monthNumber = function monthNumber(dateStr) {
        if (!(parseInt(dateStr) == dateStr)) {/**safari bug */
            var d = this.getStaticDate(dateStr);
            var month = d.getMonth();
            if (month != undefined && month != null && !isNaN(month)) {
                month = d.getMonth();
            }
        }
        else {
            try {
                var dn = parseInt(dateStr);
                if (dn >= 1 && dn <= 12) {
                    month = dn - 1;
                }
                else {
                    return dateStr;
                }
            } catch (e) {
                return dateStr;
            }
        }
        var monthNum = monthNumbers[month];
        var result = monthNum;
        if (monthNum < 10)
            result = "0" + result;

        return result;
    };

    Personetics.utils.date.yearNumber = function yearNumber(dateStr, isShort) {
        var result = "";
        var d = this.getStaticDate(dateStr);
        var fullYear = d.getFullYear().toString();
        if (isShort) {
            result = fullYear.substring(2);
        }
        else result = fullYear;
        return result;
    };
    Personetics.utils.date.nthDate = function nthDate(dateStr, lang) {
        dateStr = dateStr.toLowerCase();
        var shortKey =   lang +"-short";
        for(var i=0; i< monthNames[shortKey].length;i++){
            if(dateStr.indexOf(monthNames[shortKey][i].toLowerCase()) !== -1){
                break;
            }
        }
        var dayNumber = dateStr.replace(monthNames[shortKey][i].toLowerCase(),"");
        var month =  monthNames[lang][i];
        function nth(d) {
            if (d > 3 && d < 21) return 'th';
            switch (d % 10) {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }
        }
        var isnum = /^\d+$/.test(dayNumber);
        if(!isnum){
            return month;
        }
        dayNumber = parseInt(dayNumber,10);
        var result = month + " " + dayNumber + nth(dayNumber);
        return result;
    }

})(window, window.Personetics = window.Personetics || {});
(function(window, Personetics, $, undefined){

	Personetics.utils = Personetics.utils ? Personetics.utils : {};
	Personetics.utils.dictionary = {};

	/**
	 * Overrides an object with key value pairs from another.
	 * By default it overrides all keys (including null values). If you want to keep defined values, overrideDefinedVals should be false.
	 * Returns a copy of original
	 */
	Personetics.utils.dictionary.overrideObjectProperties = function(original, override, overrideDefinedVals)
	{

		Personetics.utils.assert.assignDefaultValue(overrideDefinedVals, true);

		var originalCopy = $.extend({}, true, original);

		if(Personetics.utils.assert.isNullOrUndefined(override))
			return originalCopy;
		if(Personetics.utils.assert.isNullOrUndefined(originalCopy))
			return override;

		$.each(override, function(key, value) {
			if(!originalCopy.hasOwnProperty(key) || originalCopy[key] == null || overrideDefinedVals)
				originalCopy[key] = value;
		});

		return originalCopy;
	};

	Personetics.utils.dictionary.hasItem = function hasItem(obj, key, nullable)
	{
		var hasItem = Personetics.utils.assert.isObject(obj) && Personetics.utils.string.isValidString(key) && obj.hasOwnProperty(key);
		return nullable ? hasItem : hasItem && obj[key] != null;
	};
	
	Personetics.utils.dictionary.exists = function exists(obj)
	{
		var args = Array.prototype.slice.call(arguments, 1);

		for (var i = 0; i < args.length; i++) {
			if (!obj || !obj.hasOwnProperty(args[i])) {
				return false;
			}
			obj = obj[args[i]];
		}
		return true;
	};

	Personetics.utils.dictionary.removeInvalids = function removeInvalids(obj)
	{
		var result = {};
		if(typeof obj !== 'undefined' && obj)
		{
			$.each(obj, function(key, value) {
				if(value != null)
				{
					if(Personetics.utils.assert.isObject(value) && !Personetics.utils.assert.isArray(value))
						result[key] = Personetics.utils.dictionary.removeInvalids(value);
					else
						result[key] = value;
				}
			});
		}
		return result;
	};

})(window, window.Personetics = window.Personetics || {}, jQuery);
(function (window, Personetics, $, undefined) {

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.emojisHelper = {};
    Personetics.utils.emojisHelper.parseEmoji = function parseEmoji(text) {
        var emojisCodes = extractEmojis(text);
        for(var i = 0; i<emojisCodes.length;i++){
            var emojiSymbol = parseHexToEmoji(emojisCodes[i]);
            var emojiRegExp = new RegExp(emojisCodes[i], 'g');
            text = text.replace(emojiRegExp, '<span>' + emojiSymbol + '</span>')
        }
        text = text.replace(/Emoji/g, '').replace(/{{|}}/g, '');
        var parsedText = text;
        return parsedText;
    }

    /**
     * extractEmojis
     * Input: text - for example ("You may want to pay close attention {{Emoji 1F470 1F3FF}} to your spending this week.")
     * Output: text - emojis codes (1F470 1F3FF)
     * */
    var extractEmojis = function (str) {
        var emojisCodes = [];
        var emojiExp = extractEmojiExp(str);
        for(var i = 0; i<emojiExp.length; i++){
            if (emojiExp[i].length) {
                var emojiCode = emojiExp[i];
                emojiCode = emojiCode.replace('Emoji', '');
                emojiCode = emojiCode.replace(/{{|}}/g, '');
                emojiCode = emojiCode.trim();
                if(emojisCodes.indexOf(emojiCode) === -1) {
                    emojisCodes.push(emojiCode);
                }
            }
        }
        return emojisCodes;
    }

    /**
     * parseHexToEmoji
     * Input: emojis codes
     * Output: emoji symbol in hex (&#x1F470&#x1F3FF)
     * */
    var parseHexToEmoji = function (emojisCodes) {
        var emojisCodesArr = emojisCodes.split(' ');
        var emoji = '';
        if (emojisCodesArr.length) {
            for (var i = 0; i < emojisCodesArr.length; i++) {
                emoji += '&#x' + emojisCodesArr[i];
            }
        }
        return emoji;
    }
    /**
     * Input: text
     * Output: text - for example"{{Emoji 1F470 1F3FF}}"*/
    var extractEmojiExp = function (str) {
        var emojiExpArr = [];
        var emojiExpOccurrence = (str.match(/{{Emoji/g) || []).length;
        var emojiOpenBrace = '{{Emoji';
        var emojiCloseBrace = '}}';
        for(var i=0; i<emojiExpOccurrence; i++){
            var expressionStartIndex = str.indexOf(emojiOpenBrace);
            var expressionEndIndex = str.indexOf(emojiCloseBrace) + emojiCloseBrace.length;
            var emojiExp = str.slice(expressionStartIndex, expressionEndIndex);
            str = str.replace(emojiExp,'');
            emojiExpArr.push(emojiExp);
        }
        return emojiExpArr;
    }


})(window, window.Personetics = window.Personetics || {}, jQuery);
(function(window, Personetics, $, undefined){

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.encodeDecode = {};
    Personetics.utils.encodeDecode.Base64 = {
        codex : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

        encode : function(input) {
            var output = new Personetics.utils.string.StringBuffer();

            var enumerator = new Personetics.utils.encodeDecode.Utf8EncodeEnumerator(input);
            while (enumerator.moveNext()) {
                var chr1 = enumerator.current;

                enumerator.moveNext();
                var chr2 = enumerator.current;

                enumerator.moveNext();
                var chr3 = enumerator.current;

                var enc1 = chr1 >> 2;
                var enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                var enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                var enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output.append(this.codex.charAt(enc1) + this.codex.charAt(enc2) + this.codex.charAt(enc3) + this.codex.charAt(enc4));
            }

            return output.toString();
        },

        decode : function(input) {
            var output = new Personetics.utils.string.StringBuffer();

            var enumerator = new Personetics.utils.encodeDecode.Base64DecodeEnumerator(input);
            while (enumerator.moveNext()) {
                var charCode = enumerator.current;

                if (charCode < 128)
                    output.append(String.fromCharCode(charCode));
                else if ((charCode > 191) && (charCode < 224)) {
                    enumerator.moveNext();
                    var charCode2 = enumerator.current;

                    output.append(String.fromCharCode(((charCode & 31) << 6) | (charCode2 & 63)));
                } else {
                    enumerator.moveNext();
                    var charCode2 = enumerator.current;

                    enumerator.moveNext();
                    var charCode3 = enumerator.current;

                    output.append(String.fromCharCode(((charCode & 15) << 12) | ((charCode2 & 63) << 6) | (charCode3 & 63)));
                }
            }

            return output.toString();
        }
    };

    Personetics.utils.encodeDecode.Utf8EncodeEnumerator = function Utf8EncodeEnumerator(input) {
        this._input = input;
        this._index = -1;
        this._buffer = [];

        this.current = Number.NaN;

        this.moveNext = function() {
            if (this._buffer.length > 0) {
                this.current = this._buffer.shift();
                return true;
            } else if (this._index >= (this._input.length - 1)) {
                this.current = Number.NaN;
                return false;
            } else {
                var charCode = this._input.charCodeAt(++this._index);

                // "\r\n" -> "\n"
                //
                if ((charCode == 13) && (this._input.charCodeAt(this._index + 1) == 10)) {
                    charCode = 10;
                    this._index += 2;
                }

                if (charCode < 128) {
                    this.current = charCode;
                } else if ((charCode > 127) && (charCode < 2048)) {
                    this.current = (charCode >> 6) | 192;
                    this._buffer.push((charCode & 63) | 128);
                } else {
                    this.current = (charCode >> 12) | 224;
                    this._buffer.push(((charCode >> 6) & 63) | 128);
                    this._buffer.push((charCode & 63) | 128);
                }

                return true;
            }
        }
    };

    Personetics.utils.encodeDecode.Base64DecodeEnumerator = function Base64DecodeEnumerator(input) {
        this._input = input;
        this._index = -1;
        this._buffer = [];

        this.current = 64;

        this.moveNext = function() {
            if (this._buffer.length > 0) {
                this.current = this._buffer.shift();
                return true;
            } else if (this._index >= (this._input.length - 1)) {
                this.current = 64;
                return false;
            } else {
                var enc1 = Personetics.utils.encodeDecode.Base64.codex.indexOf(this._input.charAt(++this._index));
                var enc2 = Personetics.utils.encodeDecode.Base64.codex.indexOf(this._input.charAt(++this._index));
                var enc3 = Personetics.utils.encodeDecode.Base64.codex.indexOf(this._input.charAt(++this._index));
                var enc4 = Personetics.utils.encodeDecode.Base64.codex.indexOf(this._input.charAt(++this._index));

                var chr1 = (enc1 << 2) | (enc2 >> 4);
                var chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                var chr3 = ((enc3 & 3) << 6) | enc4;

                this.current = chr1;

                if (enc3 != 64)
                    this._buffer.push(chr2);

                if (enc4 != 64)
                    this._buffer.push(chr3);

                return true;
            }
        }
    };
})(window, window.Personetics = window.Personetics || {}, jQuery);
(function(window, Personetics, $, undefined){

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.json = {};

    var JSON = window.JSON || {};
    window.JSON = JSON;

    JSON.stringify = JSON.stringify || function(obj) {
        var t = typeof (obj);
        if (t != "object" || obj === null) {
            // simple data type
            if (t == "string")
                obj = '"' + obj + '"';
            return String(obj);
        } else {
            // recurse array or object
            var n, v, json = [], arr = (obj && obj.constructor == Array);
            for (n in obj) {
                if(obj.hasOwnProperty(n)) {
                    v = obj[n];
                    t = typeof (v);
                    if (t == "string")
                        v = '"' + v + '"';
                    else if (t == "object" && v !== null)
                        v = JSON.stringify(v);
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
            }
            return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
        }
    };

    Personetics.utils.json.getJsonStringifyMethod = function getJsonStringifyMethod(){
        if($.toJSON){
            return $.toJSON;
        }
        else{
            return JSON.stringify;
        }
    };


    Personetics.utils.json.toJSON = function toJSON(value) {
        var _json_stringify = Personetics.utils.json.getJsonStringifyMethod();
        if(typeof Prototype !== 'undefined' && /*parseFloat(Prototype.Version.substr(0,3)) < 1.7 &&*/ typeof Array.prototype.toJSON !== 'undefined'){
            var _array_tojson = Array.prototype.toJSON;
            delete Array.prototype.toJSON;
            var r=_json_stringify(value);
            Array.prototype.toJSON = _array_tojson;
            return r;
        }
        else
            return _json_stringify(value);
    };

    Personetics.utils.json.replacer = function replacer(match, pIndent, pKey, pVal, pEnd) {
            var key = '<span class=json-key>';
            var val = '<span class=json-value>';
            var str = '<span class=json-string>';
            var r = pIndent || '';
            if (pKey)
                r = r + key + pKey.replace(/[": ]/g, '') + '</span>: ';
            if (pVal)
                r = r + (pVal[0] == '"' ? str : val) + pVal + '</span>';
            return r + (pEnd || '');
    };

    Personetics.utils.json.prettyPrint = function prettyPrint(obj) {
            var jsonLine = /^( *)("[\w.+-]+": )?("[^"]*"|[\w.+-]*)?([,[{])?$/mg;
            return JSON.stringify(obj, null, 3)
                .replace(/&/g, '&amp;').replace(/\\"/g, '&quot;')
                .replace(/</g, '&lt;').replace(/>/g, '&gt;')
                .replace(jsonLine, Personetics.utils.json.replacer);
    };
     Personetics.utils.json.mergeString = function merge(firstjsonString,secondJsonString) {
        var json1 = JSON.parse(firstjsonString);
        var json2 = JSON.parse(secondJsonString);
        var result = Personetics.utils.json.mergeObject(json1,json2);
        var objectString = Personetics.utils.json.toJSON(result);
        return objectString;
     }
    Personetics.utils.json.mergeObject = function merge(firstjson,secondJson) {
        var mergedJson = $.extend({}, firstjson, secondJson);
        return mergedJson;
    };

    Personetics.utils.json.mergeDeepObject = function merge(firstjson,secondJson) {
        var mergedJson = $.extend(true, {}, firstjson, secondJson);
        return mergedJson;
    };
})(window, window.Personetics = window.Personetics || {}, jQuery);

(function (window, Personetics, undefined) {

    Personetics.utils = Personetics.utils || {};

    Personetics.PLoggerModes = {
        VERBOSE: {
            name: "VERBOSE",
            value: 1
        },
        DEBUG: {
            name: "DEBUG",
            value: 2
        },
        ALERT: {
            name: "ALERT",
            value: 3
        },
        ERROR: {
            name: "ERROR",
            value: 4
        },
        PRODUCTION: {
            name: "PRODUCTION",
            value: 5
        }
    };

    var logger = function logger() {
        this.mode = Personetics.PLoggerModes.ERROR.value;
        if (typeof(jsServerContextObj) !== 'undefined') {
            this.mode = jsServerContextObj.getLogLevel();
        }
    };

    logger.prototype.setDebugMode = function setDebugMode(mode) {
        if (typeof mode !== 'undefined' && mode !== null && mode.hasOwnProperty("name") &&
            Personetics.PLoggerModes.hasOwnProperty(mode.name))
            this.mode = mode.value;
    };

    logger.prototype.verbose = function verbose(msg) {
        if (this.mode <= Personetics.PLoggerModes.VERBOSE.value)
            console.log(msg);
    };

    logger.prototype.debug = function debug(msg) {
        if (this.mode <= Personetics.PLoggerModes.DEBUG.value)
            console.log(msg);
    };

    logger.prototype.error = function (msg, exception, options) {
        if (this.mode <= Personetics.PLoggerModes.ERROR.value) {
            var shouldDisplayError = typeof options !== "undefined" && options.hasOwnProperty("shouldDisplayError") && typeof options.shouldDisplayError !== "undefined" ? options.shouldDisplayError : true;
            if (shouldDisplayError) {
                var context = Personetics.processor.PStoryConfig.getConfig("context");
                if (context === 'server') {
                    throw msg;
                } else {
                    console.error("<< ERROR >>: " + msg);
                }
            }
            if (Personetics.utils.assert.isDefined(exception, false))
                throw exception;
        }
    };

    logger.prototype.alert = function (msg) {
        if (this.mode <= Personetics.PLoggerModes.ALERT.value)
            console.warn(msg)
        alert(msg);
    };

    Personetics.utils.PLogger = new logger();

    Personetics.verbose = Personetics.utils.PLogger.verbose.bind(Personetics.utils.PLogger);
    Personetics.log = Personetics.utils.PLogger.debug.bind(Personetics.utils.PLogger);
    Personetics.debug = Personetics.utils.PLogger.debug.bind(Personetics.utils.PLogger);
    Personetics.error = Personetics.utils.PLogger.error.bind(Personetics.utils.PLogger);
    Personetics.alert = Personetics.utils.PLogger.alert.bind(Personetics.utils.PLogger);

})(window, window.Personetics = window.Personetics || {});
(function(window, Personetics, $, undefined){

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.messages = {};


    Personetics.utils.messages.errorMessage = function errorMessage(params) {
        var errorMsgTemplate = Personetics.UI.Handlebars.templates[ params ? params.templateId ? params.templateId : 'error-msg' : 'error-msg'];
        if(typeof personetics !== 'undefined' && personetics !== null && personetics.hasOwnProperty("getDeviceType")) {
            var deviceType = personetics.getDeviceType() || "web";
        }
        var html;
        if  (deviceType === 'web'){
             html = errorMsgTemplate(
                {
                    textLarge:params.textLarge,
                    textSmall:params.textSmall,
                    imgClass:params.imgClass,
                    btnText:params.btnText
                })
        }
        else {
            html = errorMsgTemplate(
                {
                    textLarge:params.textLarge,
                    textSmall:params.textSmall,
                    imgClass:params.imgClass,
                    btnText:params.btnText,

                    isMobileDevice:true//"perso-mobile-err"
                })
        }


        return html;
    };
    Personetics.utils.messages.noInsightsMessage = function noInsightsMessage(params) {
        var errorMsgTemplate = Personetics.UI.Handlebars.templates[ params ? params.templateId ? params.templateId : 'no-insights-message' : 'no-insights-message'];
        if(typeof personetics !== 'undefined' && personetics !== null && personetics.hasOwnProperty("getDeviceType")) {
            var deviceType = personetics.getDeviceType() || "web";
        }
        var html;
        if  (deviceType === 'web'){
            html = errorMsgTemplate(
                {
                    textLarge:params.textLarge,
                    textSmall:params.textSmall,
                    imgClass:params.imgClass
                })
        }
        else {
            html = errorMsgTemplate(
                {
                    textLarge:params.textLarge,
                    textSmall:params.textSmall,
                    imgClass:params.imgClass,
                    isMobileDevice:true//"mobile-no-insights"
                })
        }
        return html;
    };

    Personetics.utils.messages.noFilteredInsightsMessage = function noFilteredInsightsMessage(params) {
        var noFileredMsgTemplate = Personetics.UI.Handlebars.templates[ params ? params.templateId ? params.templateId : 'no-filtered-insights-msg' : 'no-filtered-insights-msg'];
        if(typeof personetics !== 'undefined' && personetics !== null && personetics.hasOwnProperty("getDeviceType")) {
            var deviceType = personetics.getDeviceType() || "web";
        }
        var html;
        if  (deviceType === 'web'){
            html = noFileredMsgTemplate(
                {
                    text: params.text
                });
        }
        else {
            html = noFileredMsgTemplate(
                {
                    text: params.text,
                    isMobileDevice:true//"mobile-no-insights"
                })
        }
        return html;
    };

})(window, window.Personetics = window.Personetics || {}, jQuery);

(function(window, Personetics, $, undefined){

	Personetics.utils = Personetics.utils ? Personetics.utils : {};
	Personetics.utils.network = {};
	
	Personetics.utils.network.consts = {
		
		base64EncodeResponse : false,
		requestTimeoutInMillis: 300000,
		dataType: "json",
		async: true,
		onComplete: function() {},
		onSuccess: function(data, textStatus, jqXHR) {
			Personetics.log("request success");
		},
		onFailure: function(jqXHR, textStatus, errorThrown) {
			Personetics.log("request failed: " + textStatus + ", " + errorThrown);
		}
	};


	Personetics.utils.network.doAjaxGet = function doAjaxGet(url, data, requestOptions) {

		var finalRequestOptions = Personetics.utils.dictionary.overrideObjectProperties(requestOptions, Personetics.utils.network.consts, false);

		Personetics.utils.assert.AssertIsDefined(url, "Personetics.utils.network.doAjaxGet: invalid request URL: " + url);

		var config = {
				type: "GET",
				url: url,
				dataType: finalRequestOptions.dataType,
				async: finalRequestOptions.async,
				timeout: finalRequestOptions.requestTimeoutInMillis,
				complete: finalRequestOptions.onComplete,
				success: onDoAjaxGetSuccess,
				error: onDoAjaxGetError
			};

		if(!Personetics.utils.assert.isNullOrUndefined(data))
			config.data = data;

		$.ajax(config);

		function onDoAjaxGetSuccess (response) {
			
			var processedResponse = response;
			if (finalRequestOptions.base64EncodeResponse == true ) {
				 processedResponse = Personetics.utils.network.decodeDataWithBase64(response);
				 processedResponse = $.parseJSON(processedResponse);
			}
			
			finalRequestOptions.onSuccess(processedResponse);
		}

		function onDoAjaxGetError (XMLHttpRequest, textStatus, errorThrown) {
			finalRequestOptions.onFailure (XMLHttpRequest, textStatus, errorThrown);
		}
	};

	Personetics.utils.network.doAjaxPost = function doAjaxPost(url, postData, requestOptions) {

		var finalRequestOptions = Personetics.utils.dictionary.overrideObjectProperties(requestOptions, Personetics.utils.network.consts, false);

		Personetics.utils.assert.AssertIsNullOrUndefined(url, "Personetics.utils.network.doAjaxPost: invalid request URL: " + url);

		$.ajax({
			type: "POST",
			url: url,
			data: postData,
			dataType: finalRequestOptions.dataType,
			async: finalRequestOptions.async,
			headers: finalRequestOptions.headers,
			timeout: finalRequestOptions.requestTimeoutInMillis,
			complete: finalRequestOptions.onComplete,
			success: onDoAjaxPostSuccess,
			error: onDoAjaxPostError
		});


		function onDoAjaxPostSuccess (response) {
			var processedResponse = response;
			if (finalRequestOptions.base64EncodeResponse == true ) {
				 processedResponse = Personetics.utils.network.decodeDataWithBase64(response);
				 processedResponse = $.parseJSON(processedResponse);
			}

			finalRequestOptions.onSuccess(processedResponse);
		}

		function onDoAjaxPostError (XMLHttpRequest, textStatus, errorThrown) {
			finalRequestOptions.onFailure (XMLHttpRequest, textStatus, errorThrown);
		}
	};

	Personetics.utils.network.queueAjaxPost = function queueAjaxPost(queueId, url, postData, requestOptions) {

		var finalRequestOptions = Personetics.utils.dictionary.overrideObjectProperties(requestOptions, Personetics.utils.network.consts, false);

		return $.ajaxq (queueId, {
			type: "POST",
			url: url,
			data:postData,
			dataType: finalRequestOptions.dataType,
			timeout: finalRequestOptions.requestTimeoutInMillis,
			success: onDoAjaxQueuePostSuccess,
			error: onDoAjaxQueuePostError
		});

		function onDoAjaxQueuePostSuccess (response) {
			var processedResponse = response;
			if (finalRequestOptions.base64EncodeResponse == true ) {
				processedResponse = Personetics.utils.network.decodeDataWithBase64(response);
				processedResponse = $.parseJSON(processedResponse);
			}
			
			finalRequestOptions.onSuccess(processedResponse);
		}

		function onDoAjaxQueuePostError (XMLHttpRequest, textStatus, errorThrown) {
			finalRequestOptions.onFailure (XMLHttpRequest, textStatus, errorThrown);
		}

	};

	Personetics.utils.network.removeAjaxPostQueue = function(queueId) {
		$.ajaxq(queueId);
	};

	Personetics.utils.network.wasUserAborted = function wasUserAborted(xhr, textStatus){
		return !xhr.getAllResponseHeaders() && textStatus != 'timeout';
	};

	Personetics.utils.network.createPostform = function createPostform(params, absoluteUrl, newDoc, win, pageInfo) {
		newDoc.charset = "utf-8";

		var formId = "hintForm";
		var form = newDoc.createElement("form");
		form.setAttribute("id", formId);
		form.setAttribute("method", "post");
		form.setAttribute("action", absoluteUrl);

		// form.acceptCharset='utf-8'
		for ( var i in params) {
			if (params.hasOwnProperty(i)) {
				var input = newDoc.createElement('input');
				input.type = 'hidden';
				input.name = i;
				input.value = params[i];
				form.appendChild(input);
			}
		}

		newDoc.body.appendChild(form);
		form.submit();
		newDoc.body.removeChild(form);

	}

	Personetics.utils.network.decodeDataWithBase64 = function (data) {

		data = data.replace(/(\r\n|\n|\r)/gm,"");
		data = data.replace(/"/g, ''); //  ""example"" ---> "example"
		var decodedData = Personetics.utils.encodeDecode.Base64.decode(data);
		return decodedData;
	};

})(window, window.Personetics = window.Personetics || {}, jQuery);
(function(window, Personetics, $, undefined){

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.persoBrowserDetector = {};
    var scope;
    Personetics.utils.persoBrowserDetector.init = function (){
        scope=this;
        this.browser = searchString(dataBrowser) || "An unknown browser";
        this.version = searchVersion(navigator.userAgent) || searchVersion(navigator.appVersion) || "an unknown version";
        this.OS = searchString(dataOS) || "an unknown OS";
    };

    var searchString = function(data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            scope.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1){
                    return data[i].identity;
                }
            } else if (dataProp){
                return data[i].identity;
            }
        }
    }

    var searchVersion = function(dataString) {
        var index = dataString.indexOf(scope.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index + scope.versionSearchString.length + 1));
    }

    Personetics.utils.persoBrowserDetector.getBrowserData = function(){
        return {browser: this.browser,
            version: this.version,
            os: this.OS,
            deviceType: (isTablet.any()) ? "perso-tablet" : (isMobile.any()) ? "perso-mobile" : "perso-desktop"
        }
    }

    var dataBrowser = [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome",
            versionSearch: "Chrome"
        }, {
            string: navigator.userAgent,
            subString: "OmniWeb",
            versionSearch: "OmniWeb/",
            identity: "OmniWeb"
        }, {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        }, {
            prop: window.opera,
            identity: "Opera",
            versionSearch: "Version"
        }, {
            string: navigator.vendor,
            subString: "iCab",
            identity: "iCab",
            versionSearch: "iCab",
        }, {
            string: navigator.vendor,
            subString: "KDE",
            identity: "Konqueror",
            versionSearch: "Konqueror",
        }, {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox",
            versionSearch: "Firefox",
        }, {
            string: navigator.vendor,
            subString: "Camino",
            identity: "Camino",
            versionSearch: "Camino",
        }, { // for newer Netscapes (6+)
            string: navigator.userAgent,
            subString: "Netscape",
            identity: "Netscape",
            versionSearch: "Netscape",
        }, {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        }, {
            string: navigator.userAgent,
            subString: "Gecko",
            identity: "Mozilla",
            versionSearch: "rv"
        },
        {
            string: navigator.userAgent,
            subString: "Mozilla",
            identity: "Mozilla",
            versionSearch: "Mozilla"
        },{ // for older Netscapes (4-)
            string: navigator.userAgent,
            subString: "Mozilla",
            identity: "Netscape",
            versionSearch: "Mozilla"
        }
    ];
    var dataOS = [
        {
            string: navigator.platform,
            subString: "Win",
            identity: "Windows",
            versionSearch: "Windows"
        }, {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac",
            versionSearch: "Mac"
        }, {
            string: navigator.userAgent,
            subString: "iPhone",
            identity: "iPhone/iPod/iPad",
            versionSearch: "iPhone/iPod/iPad/rv"
        }, {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux",
            versionSearch: "Linux"
        }
    ];
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    }
    var isTablet = {
        Android: function(){
            return navigator.userAgent.match(/Tablet|Kindle/i);
        },
        iOS: function (){
            return navigator.userAgent.match(/iPad/i);
        },
        any: function(){
            return (isTablet.Android() || isTablet.iOS());
        }
    };
    Personetics.utils.persoBrowserDetector.init();

})(window, window.Personetics = window.Personetics || {}, jQuery);

(function (window, Personetics,$, undefined) {

    Personetics.messenger = Personetics.messenger || {};
    Personetics.messenger.MSG_SECURITY_ERROR="perso-story-widget";
    Personetics.messenger.MSG_RENDERED_STORY="perso-rendered-story-widget";
    Personetics.messenger.listen = function listen(msg, cb) {
        $(this).bind(msg,cb);
    };
    Personetics.messenger.removeListener = function removeListener(msg, cb) {
        $(this).unbind(msg,cb);
    };
    Personetics.messenger.dispatch = function dispatch(msg,data) {
        $(this).trigger(msg,data);
    };



})(window, window.Personetics = window.Personetics || {},jQuery);

'use strict';

/**
 * Copyright Marc J. Schmidt. See the LICENSE file at the top-level
 * directory of this distribution and at
 * https://github.com/marcj/css-element-queries/blob/master/LICENSE.
 */
(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        define(factory);
    } else if (typeof exports === "object") {
        module.exports = factory();
    } else {
        root.PersoResizeDetector = factory();
    }
}(typeof window !== 'undefined' ? window : this, function () {

    // Make sure it does not throw in a SSR (Server Side Rendering) situation
    if (typeof window === "undefined") {
        return null;
    }
    // Only used for the dirty checking, so the event callback count is limited to max 1 call per fps per sensor.
    // In combination with the event based resize sensor this saves cpu time, because the sensor is too fast and
    // would generate too many unnecessary events.
    var requestAnimationFrame = window.requestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        function (fn) {
            return window.setTimeout(fn, 20);
        };

    /**
     * Iterate over each of the provided element(s).
     *
     * @param {HTMLElement|HTMLElement[]} elements
     * @param {Function}                  callback
     */
    function forEachElement(elements, callback){
        var elementsType = Object.prototype.toString.call(elements);
        var isCollectionTyped = ('[object Array]' === elementsType
            || ('[object NodeList]' === elementsType)
            || ('[object HTMLCollection]' === elementsType)
            || ('[object Object]' === elementsType)
            || ('undefined' !== typeof jQuery && elements instanceof jQuery) //jquery
            || ('undefined' !== typeof Elements && elements instanceof Elements) //mootools
        );
        var i = 0, j = elements.length;
        if (isCollectionTyped) {
            for (; i < j; i++) {
                callback(elements[i]);
            }
        } else {
            callback(elements);
        }
    }

    /**
    * Get element size
    * @param {HTMLElement} element
    * @returns {Object} {width, height}
    */
    function getElementSize(element) {
        if (!element.getBoundingClientRect) {
            return {
                width: element.offsetWidth,
                height: element.offsetHeight
            }
        }

        var rect = element.getBoundingClientRect();
        return {
            width: Math.round(rect.width),
            height: Math.round(rect.height)
        }
    }

    /**
     * Class for dimension change detection.
     *
     * @param {Element|Element[]|Elements|jQuery} element
     * @param {Function} callback
     *
     * @constructor
     */
    var PersoResizeDetector = function(element, callback) {
        /**
         *
         * @constructor
         */
        function EventQueue() {
            var q = [];
            this.add = function(ev) {
                q.push(ev);
            };

            var i, j;
            this.call = function() {
                for (i = 0, j = q.length; i < j; i++) {
                    q[i].call();
                }
            };

            this.remove = function(ev) {
                var newQueue = [];
                for(i = 0, j = q.length; i < j; i++) {
                    if(q[i] !== ev) newQueue.push(q[i]);
                }
                q = newQueue;
            };

            this.length = function() {
                return q.length;
            }
        }

        /**
         *
         * @param {HTMLElement} element
         * @param {Function}    resized
         */
        function attachResizeEvent(element, resized) {
            if (!element) return;
            if (element.resizedAttached) {
                element.resizedAttached.add(resized);
                return;
            }

            element.resizedAttached = new EventQueue();
            element.resizedAttached.add(resized);

            element.persoResizeDetector = document.createElement('div');
            element.persoResizeDetector.dir = 'ltr';
            element.persoResizeDetector.className = 'perso-resize-detector';
            var style = 'position: absolute; left: -10px; top: -10px; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;';
            var styleChild = 'position: absolute; left: 0; top: 0; transition: 0s;';

            element.persoResizeDetector.style.cssText = style;
            element.persoResizeDetector.innerHTML =
                '<div class="perso-resize-detector-expand" style="' + style + '">' +
                    '<div style="' + styleChild + '"></div>' +
                '</div>' +
                '<div class="perso-resize-detector-shrink" style="' + style + '">' +
                    '<div style="' + styleChild + ' width: 200%; height: 200%"></div>' +
                '</div>';
            element.appendChild(element.persoResizeDetector);

            var position = window.getComputedStyle(element).getPropertyPriority('position');
            if ('absolute' !== position && 'relative' !== position && 'fixed' !== position) {
                element.style.position = 'relative';
            }

            var expand = element.persoResizeDetector.childNodes[0];
            var expandChild = expand.childNodes[0];
            var shrink = element.persoResizeDetector.childNodes[1];
            var dirty, rafId, newWidth, newHeight;
            var size = getElementSize(element);
            var lastWidth = size.width;
            var lastHeight = size.height;

            var reset = function() {
                //set display to block, necessary otherwise hidden elements won't ever work
                var invisible = element.offsetWidth === 0 && element.offsetHeight === 0;

                if (invisible) {
                    var saveDisplay = element.style.display;
                    element.style.display = 'block';
                }

                expandChild.style.width = '100000px';
                expandChild.style.height = '100000px';

                expand.scrollLeft = 100000;
                expand.scrollTop = 100000;

                shrink.scrollLeft = 100000;
                shrink.scrollTop = 100000;

                if (invisible) {
                    element.style.display = saveDisplay;
                }
            };
            element.persoResizeDetector.resetSensor = reset;

            var onResized = function() {
                rafId = 0;

                if (!dirty) return;

                lastWidth = newWidth;
                lastHeight = newHeight;

                if (element.resizedAttached) {
                    element.resizedAttached.call();
                }
            };

            var onScroll = function() {
                var size = getElementSize(element);
                var newWidth = size.width;
                var newHeight = size.height;
                dirty = newWidth != lastWidth || newHeight != lastHeight;

                if (dirty && !rafId) {
                    rafId = requestAnimationFrame(onResized);
                }
                if (element.resizedAttached) {
                    element.resizedAttached.call();
                }
                reset();
            };

            var addEvent = function(el, name, cb) {
                if (el.attachEvent) {
                    el.attachEvent('on' + name, cb);
                } else {
                    el.addEventListener(name, cb);
                }
            };
            addEvent(expand, 'scroll', onScroll);
            addEvent(shrink, 'scroll', onScroll);
            
			// Fix for custom Elements
			requestAnimationFrame(reset);
        }

        forEachElement(element, function(elem){
            attachResizeEvent(elem, callback);
        });

        this.detach = function(ev) {
            PersoResizeDetector.detach(element, ev);
        };

        this.reset = function() {
            element.persoResizeDetector.resetSensor();
        };
    };

    PersoResizeDetector.reset = function(element, ev) {
        forEachElement(element, function(elem){
            elem.persoResizeDetector.resetSensor();
        });
    };

    PersoResizeDetector.detach = function(element, ev) {
        forEachElement(element, function(elem){
            if (!elem) return;
            if(elem.resizedAttached && typeof ev === "function"){
                elem.resizedAttached.remove(ev);
                if(elem.resizedAttached.length()) return;
            }
            if (elem.persoResizeDetector) {
                if (elem.contains(elem.persoResizeDetector)) {
                    elem.removeChild(elem.persoResizeDetector);
                }
                delete elem.persoResizeDetector;
                delete elem.resizedAttached;
            }
        });
    };

    return PersoResizeDetector;

}));


(function(window, Personetics, $, undefined){

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.print = {};

    Personetics.utils.print.printHtmlToPdf = function printHtmlToPdf(config) {
        var pdf = new jsPDF('p', 'pt', 'a4');
        pdf.addFont('Roboto');
        pdf.setFont('Roboto');
        var element = config.parent;
        if(!config.page || !config.page.el){
            var children = config.parent.getElementsByTagName("*");
            config.beforeExportCallback && config.beforeExportCallback(children);
            pdf.internal.scaleFactor = config.internalScaleFactor;//3.75;
            addHtml(element, pdf, config);
        }else {
            var elemFromHtml = config.page.el;
            //config.parent.appendChild(elemFromHtml);
            var children = config.parent.getElementsByTagName("*");
            config.beforeExportCallback && config.beforeExportCallback(children);
            html2canvas(elemFromHtml, {
                allowTaint: false,
                useCORS: true,
            }).then(function (canvas) {
                pdf.internal.scaleFactor = config.page.internalScaleFactor;
                var newCanvas = copyCanvas(canvas, elemFromHtml.clientWidth, elemFromHtml.clientHeight);
                config.parent.appendChild(newCanvas);
                var x = config.x ? config.x : 0;
                var y = config.y ? config.y : 0;
                pdf.addHTML(newCanvas, x, y, {
                    pagesplit: true,
                    allowTaint: false,
                    useCORS: true
                }, function () {
                    if (newCanvas.parentElement) {
                        config.parent.removeChild(newCanvas);
                    }
                    elemFromHtml.parentNode.removeChild(elemFromHtml);
                    pdf.addPage();
                    pdf.internal.scaleFactor = config.internalScaleFactor;//3.75;
                    addHtml(element, pdf, config);
                });
            });
        }
    }

    function addHtml(element, pdf, config){
        var newCanvas = createCanvas({width:element.clientWidth,height:element.clientHeight,totalHeight:config.height});
        var x = config.x ? config.x : 0;
        var y = config.y ? config.y : 0;
        pdf.addHTML(element, x, y, {
                pagesplit: true,
                canvas: newCanvas
            },
            function () {
                if (newCanvas.parentElement) {
                    config.parent.removeChild(newCanvas);
                }
                // $.each(pdf.internal.pages, function (index, value) {
                //     if (value) {
                //         console.log(value);
                //     }
                // });
                pdf.save(config.fileName + config.format);
                config.callback && config.callback();
            });
    }
    Personetics.utils.print.printHtmlToPdfWithLoop = function printHtmlToPdf(config) {
        var pdf = new jsPDF('p', 'pt', 'a4');
        var element = config.parent;
        if(!config.page || !config.page.html){
            pdf.internal.scaleFactor = 3.75;
            var frame = config.parent.getElementsByClassName("pstory-dialog-content");
                var newCanvas = createCanvas({
                    width: element.clientWidth,
                    height: element.clientHeight,
                    totalHeight: config.height
                });
                pdf.addHTML(frame[0], {pagesplit: true, canvas: newCanvas}, function () {
                    if (newCanvas.parentElement) {
                        config.parent.removeChild(newCanvas);
                    }
                    loopElements(pdf, frame, 1, config);
                });
        }else {
            pdf.internal.scaleFactor = 2.5;
            var elemFromHtml = document.createElement("div");
            elemFromHtml.innerHTML = config.page.html;
            var newElement = elemFromHtml.firstChild;
            newElement.style.width = config.page.width;
            config.parent.appendChild(newElement);
            var children = newElement.getElementsByTagName("*");
            for (var i = 0; i < children.length; i++) {
                if (window.getComputedStyle(children[i], null).getPropertyValue("background-image") !== "none") {
                    children[i].style.backgroundImage = "none";
                }
            }
            html2canvas(newElement, {
                allowTaint: false,
                useCORS: true,
            }).then(function (canvas) {
                pdf.internal.scaleFactor = 1;
                var newCanvas = copyCanvas(canvas, newElement.clientWidth, newElement.clientHeight);
                config.parent.appendChild(newCanvas);
                pdf.addHTML(newCanvas, 0, 0, {
                    pagesplit: true, allowTaint: false,
                    useCORS: true
                }, function () {
                    if (newCanvas.parentElement) {
                        config.parent.removeChild(newCanvas);
                    }
                    newElement.parentNode.removeChild(newElement);
                    pdf.addPage();
                    pdf.internal.scaleFactor = 3.75;
                    var canvas = createCanvas({
                        width: element.clientWidth,
                        height: element.clientHeight,
                        totalHeight: config.height
                    });
                    pdf.addHTML(element, {pagesplit: true, canvas: canvas}, function () {

                        pdf.save(config.fileName + config.format);
                        config.callback && config.callback();
                    });
                });
            });
        }
    }


    function createCanvas(options){
        var canvas = document.createElement('canvas');
        canvas.width = options.width * 3;
        canvas.height = options.totalHeight * 3;
        canvas.style.width = options.width + 'px';
        canvas.style.height = options.height + 'px';
        var context = canvas.getContext('2d');
        context.scale(3, 3);
        context.fillStyle ="rgba(255, 255, 255, 1)";
        context.fillRect(0,0,canvas.width, canvas.height);
        return canvas;
    }

    function updateCanvas(canvas, options){
        //var canvas = document.createElement('canvas');
        canvas.width = options.width * 3;
        canvas.height = options.totalHeight * 3;
        canvas.style.width = options.width + 'px';
        canvas.style.height = options.height + 'px';
        var context = canvas.getContext('2d');
        context.scale(3, 3);
        context.fillStyle ="rgba(255, 255, 255, 1)";
        context.fillRect(0,0,canvas.width, canvas.height);
    }

    function copyCanvas(canvas, width, height){
        var newCanvas = document.createElement('canvas');
        newCanvas.width = width * 3;
        newCanvas.height = height * 3;
        newCanvas.style.width = width + 'px';
        newCanvas.style.height = height + 'px';
        var destCtx = newCanvas.getContext('2d');
        destCtx.scale(3, 3);
        destCtx.fillStyle = "rgba(255, 255, 255, 1)";
        destCtx.fillRect(0, 0, newCanvas.width, newCanvas.height);
        destCtx.drawImage(canvas, 0, 0);
        return newCanvas;
    }

    function loopElements(pdf, elemnts, index, config) {
        if (index == elemnts.length) {
            pdf.save(config.fileName + config.format);
            config.callback && config.callback();
            return;
        }
        pdf.addPage();
        html2canvas(elemnts[index], {
            allowTaint: false,
            useCORS: true,
        }).then(function (canvas) {
            pdf.internal.scaleFactor = 1;
            var newCanvas = copyCanvas(canvas, elemnts[index].clientWidth, elemnts[index].clientHeight);
            config.parent.appendChild(newCanvas);
            pdf.addHTML(newCanvas, 0, 0, {
                pagesplit: true, allowTaint: false,
                useCORS: true
            }, function () {
                if (newCanvas.parentElement) {
                    config.parent.removeChild(newCanvas);
                }
                loopElements(pdf, elemnts, index + 1, config);
            });
        });
    }
})(window, window.Personetics = window.Personetics || {}, jQuery);
(function(window, Personetics, $, undefined){

	Personetics.utils = Personetics.utils ? Personetics.utils : {};
	Personetics.utils.scope = {};

	// Returns a wrapper that binds "this" inside "fn" to "scope"
	Personetics.utils.scope.bind = function bind(scope, fn) {
		return function() {
			return fn.apply(scope, arguments);
		};
	};

	Personetics.bind = Personetics.utils.scope.bind;

	Personetics.utils.scope.Callback = function Callback(f, parameters) {
		this.parameters=parameters;
		this.f=f;
		var me=this;
		this.invoke=function () {

			me.f.apply( me, me.parameters );
			// me.f(me.parameters);
		}
	};
})(window, window.Personetics = window.Personetics || {}, jQuery);
(function (window, Personetics, $, undefined) {

	Personetics.utils = Personetics.utils ? Personetics.utils : {};
	Personetics.utils.string = {};
	Personetics.utils.string.isString = function isString(str) {
		return typeof str === 'string';
	};

	Personetics.utils.string.isEmpty = function isString(str) {
		return Personetics.utils.string.isString(str) && str.length == 0;
	};

	Personetics.utils.string.isValidString = function isValidString(str) {
		return Personetics.utils.assert.isString(str) && str != null && str.length > 0;
	};

	Personetics.utils.string.truncateLongWords = function truncateLongWords(setnece, maxWordLength) {
		if (typeof setnece !== 'undefined' && setnece && setnece.length > 0 && setnece.replace(/ /g, '').length > 0) {
			var words = setnece.split(' ');
			var processedWords = [];
			$.each(words, function (i, word) {
				if (word.length > maxWordLength) {
					processedWords.push(word.substring(0, maxWordLength - 6) + '...');
				}
				else {
					processedWords.push(word);
				}
			});
			return processedWords.join(' ');
		}
		else {
			return "";
		}
	};

	Personetics.utils.string.stripTags = function stripTags(input) {
		var tagsToReplace = ["script"];

		var result = input;

		if (typeof input !== 'undefined' && input != null && input.length > 0) {
			$.each(tagsToReplace, function (i, tagToReplace) {
				var tagRegex = new RegExp("<" + tagsToReplace + ".*>.*</" + tagToReplace + ">", "ig");
				result = result.replace(tagRegex, "");
			});
		}
		return result;
	};

	Personetics.utils.string.replaceNewLine = function replaceNewLine(element) {
		if (!element)
			return element;

		var typeOf = typeof (element);

		if (typeOf == 'string') {

			var regex = new RegExp("^<([A-Z][A-Z0-9]*)[^>]*>(.*?)</\\1>$", "gim");
			var res = regex.exec(element.replace(/\n/g, ""));
			if (res) {
				element = element.replace(/\n/g, "");
			} //html code
			else { element = element.replace(/\n/g, "<br />"); }
		} else if (typeOf == 'object')
			$.each(element, function (key, value) {

				if (!value)
					return;

				if (typeof (key) == 'string' && key.toLowerCase().indexOf('speaking') != -1) // skip
					// speaking
					return;
				var typeOfValue = typeof (value);
				element[key] = Personetics.utils.string.replaceNewLine(value);
			});

		return element;
	};

	Personetics.utils.string.StringBuffer = function StringBuffer() {
		this.buffer = [];

		this.append = function append(string) {
			this.buffer.push(string);
			return this;
		};

		this.toString = function toString() {
			return this.buffer.join("");
		};
	};

	Personetics.utils.string.decodeHTMLSpecialCharecters = function decodeHTMLSpecialCharecters(input) {
		var decodedString = input.replace("&amp;", "&");
		/* add here any other replacments if needed in the future*/
		return decodedString;
	};

	Personetics.utils.string.stripSpecialChars = function stripSpecialChars(val) {
		return val.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
	};

	Personetics.utils.string.removeClass = function removeClass(className, clazz) {
		var _classname = className;
		//support objects for svg
		if (typeof className === 'object')
			_classname = _classname.animVal;
		_classname = _classname.replace(new RegExp('(?:^|\\s)'+ clazz + '(?:\\s|$)'), ' ');
		return _classname;
	};

	Personetics.utils.string.addClass = function addClass(className, clazz) {
		className= Personetics.utils.string.removeClass(className,clazz);
		var _classname = className;
		//support objects for svg
		if (typeof className === 'object')
			_classname = _classname.animVal;
		_classname += " "+ clazz;
		return _classname;
	};

    Personetics.utils.string.textWidth = function(text) {
        var div = $('#perso-text-width');
        if (div.length == 0)
            div = $('<div id="perso-text-width" style="display: none;"></div>').appendTo($('body'));
        div.html(text);
        var itemWidth=div.width();
        div.remove();
        return(itemWidth);
    };


})(window, window.Personetics = window.Personetics || {}, jQuery);

(function(window, Personetics, $, undefined){

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.url = {};

    /**
     * Returns value of specific request parameter identified by the name parameter
     */
    Personetics.utils.url.getParameterByName = function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    /**
     * Returns full address of given relative URL without the file name and query string
     */
    Personetics.utils.url.getAbsoluteUrl = function getAbsoluteUrl(localurl){
        
        if (/^(https?|file|ftps?|mailto|javascript|data:image\/[^;]{2,9};):/i.test(localurl))
            return localurl; // localurl is already absolute
            
            var href = window.location.href;
            var pathArray = href.split('/');
            var newPathname = "";
    
            //for ( var i = 0; i < 3/*pathArray.length - 1*/; i++) {
                for ( var i = 0; i < pathArray.length - 1; i++) {
                if (i != 0)
                    newPathname += "/";

                newPathname += pathArray[i];
            }
            return newPathname + '/' + localurl;
    };

    /**
     * Opens a new browser tab using post request
     */
    Personetics.utils.url.openTabWithPost = function openTabWithPost(localUrl, windowoption, params){
        var absoluteUrl = Personetics.utils.url.getAbsoluteUrl(localUrl);

        var newWin = window.open("","_blank");
        newWin.opener = null;
        newWin.focus();
        var newDoc = newWin.document;

        Personetics.utils.network.createPostform(name, params, absoluteUrl, newDoc);
    };

    /**
     * Creates a new iframe and loads it with a post request
     */
    Personetics.utils.url.iframeWithPost = function iframeWithPost(url, name, params){
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", url);
        form.setAttribute("target", name);

        for (var i in params) {
            if (params.hasOwnProperty(i)) {
                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = i;
                input.value = params[i];
                form.appendChild(input);
            }
        }

        document.body.appendChild(form);

        // note I am using a post.htm page since I did not want to make double
        // request to the page
        // it might have some Page_Load call which might screw things up.
        // window.open("post.htm", name, windowoption);

        form.submit();

        document.body.removeChild(form);
    };

    Personetics.utils.url.urlCheck = function urlCheck(s) {
        var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        return regexp.test(s);
    };

    Personetics.utils.url.encodePostParams = function encodePostParams (parameterObject) {
        var postParamObject = {};
        /* if contains in key the string 64 - encode it and add it to the object */
        $.each(parameterObject, Personetics.bind(this, function(key, value){
            if (key.indexOf("64") >= 0){
                /*add base64 encoding*/
                var paramsJson = Personetics.utils.json.toJSON(parameterObject[key]);
                var str64 = Personetics.utils.encodeDecode.Base64.encode(paramsJson);
                parameterObject[key] = str64;
            } 
        }));

         return parameterObject;
    };
})(window, window.Personetics = window.Personetics || {}, jQuery);

(function(window, Personetics, $, undefined){
    Personetics.story = Personetics.story || {};
    Personetics.story.PWebAnalytics = Base.extend({
        reportWidgetActive: function(options){},
        reportWidgetLoaded: function(options){},
        reportOverlayLoaded: function(options){},
        reportRatingsSent: function(options){}
    });

})(window, window.Personetics = window.Personetics || {}, jQuery);

(function(window, Personetics, $, undefined){

    Personetics.utils = Personetics.utils ? Personetics.utils : {};

    Personetics.utils.PEvent = Base.extend({
        initialize: function(){
            this.eventMessage = {
                "type" : "sendEvents",
                "eventInfo": {},
                "checkInPastEvent": true
            }
        },

        // eventMessage: {
        //     "type" : "notifyEvent",
        //     "eventInfo": {},
        //     "checkInPastEvent": true
        // },

        setParam: function(paramName, paramValue){
            this.eventMessage.eventInfo.params[paramName] = paramValue;
        },

        getJsonMessageEvent: function () {
            return JSON.stringify(this.eventMessage);
        },

        onSuccessCallback: function(){},

        onErrorCallback: function(){}
    });
})(window, window.Personetics = window.Personetics || {}, jQuery);
(function(window, Personetics, $, undefined){

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.PEventsBuffer = function() {

        this.sendEventsTypesMapping = {
            "account-selector":"AccountSelector",
            "bar-chart":"BarChart",
            "pie-chart":"PieChart",
            "tabs":"Button",
            "buttons":"Button",
            "Navigation":"NavigateButton"
        }
        /* Event that were sent to PServer*/
        this.pastEvents = [];

        /* Event that are in Q and waiting to be sent to PServer*/
        this.eventToSend = [];

        /* Delay (seconds) between each time we send all events in "eventToSend" to PServer */
        this.eventDelayInterval = Personetics.projectConfiguration.getConfig("eventDelayInterval");

        this.start = function(){
            var me = this;
            this.intervalId = setInterval(function () {
                me.sendEvents();
            },
            this.eventDelayInterval);
        };

        this.stop = function(){
            if(this.intervalId)
                clearInterval(this.intervalId);
        };

        /* Adding events to the "eventToSend" Q */
        this.addEventToSend = function (event, success_callback, error_callback) {

            // Check if event already registered
            var shouldAddEvent = this.shouldSendEvent(event);
            if (shouldAddEvent == true) {
                this.eventToSend.push({
                    eventData: event,
                    onSuccessCallback: success_callback,
                    onErrorCallback: error_callback
                });
                if(event.eventMessage.eventInfo.type === "Navigation")
                {
                    this.sendEvents();
                }
            }
        };

        /* Send all events in buffer */
        this.sendEvents = function () {
            var me = this;
            var didSendEvents = false;
            var requestMsg ={};
            var eventsInfoList = [];
            if(this.eventToSend.length)
            {
                requestMsg.type = this.eventToSend[0].eventData.eventMessage.type;
                requestMsg.lang = Personetics.processor.PStoryConfig.getConfig("lang");
                requestMsg.protocolVersion = this.eventToSend[0].eventData.eventMessage.protocolVersion;
            }
            $.each(this.eventToSend, function(key, pevent) {
                if(typeof me.sendEventsTypesMapping[pevent.eventData.eventMessage.eventInfo.type] !== "undefined")
                {
                    pevent.eventData.eventMessage.eventInfo.type = me.sendEventsTypesMapping[pevent.eventData.eventMessage.eventInfo.type];
                }
                eventsInfoList.push(pevent.eventData.eventMessage.eventInfo);
                /* Add event to Q - pastEvents */
                me.pastEvents.push(pevent);
            });
            if(eventsInfoList.length) {
                didSendEvents = true;
                requestMsg.eventsInfoList = eventsInfoList;
                personetics.pserverProxy.notifyEvent(
                    requestMsg,
                    this.onSuccess,
                    this.onError);
            }
            /* Remove all event from Q - eventToSend */
            if (didSendEvents == true) {
                this.eventToSend = [];
            }

        };
        this.onSuccess = function()
        {

        };
        this.onError = function()
        {

        };
        /*
         Check if event should be added to the "sending Q"
         The function will check both Qs - "eventToSend" & "pastEvents"
         In case the current event exist - it wont send it
         */
        this.shouldSendEvent = function (pevent) {

            /*
             Go over all the events that were already been sent -
             if the current event is equal - Dont send it again.
             */

            var shouldSendEvent = true;
            var currentEventInfo = JSON.stringify(pevent.eventMessage.eventInfo);

            if(pevent.eventMessage.checkInPastEvent == true){
                $.each(this.pastEvents, function(key, aPastEvent) {
                    var pastEventInfo = aPastEvent.eventData.eventMessage.eventInfo;
                    if (JSON.stringify(pastEventInfo) === currentEventInfo) {
                        shouldSendEvent = false;
                        return false;
                    }
                });
            }

            /*
             In case the event was sent - stop the search and return - false = DONT SEND AGAIN
             else continue searching in the second Q
             */
            if (shouldSendEvent == false)
                return shouldSendEvent;


            /*
             Go over all the events that are in the Q and about to be send  -
             if the current event is equal - Dont add it again.
             */
            $.each(this.eventToSend, function(key, anEventToSend) {
                var pastEventInfo = anEventToSend.eventData.eventMessage.eventInfo;
                if (JSON.stringify(pastEventInfo) === JSON.stringify(currentEventInfo)) {
                    shouldSendEvent = false;
                    return false;
                }
            });

            return shouldSendEvent;
        };

        this.onEventSendSuccess = function () {
            //TODO: ?
        };

        this.onEventSendError = function () {
            Personetics.log("Error on sending events to PServer....");
        };
    };

})(window, window.Personetics = window.Personetics || {}, jQuery);
(function(window, Personetics, $, undefined){

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    Personetics.utils.PEventOptInOut = Personetics.utils.PEvent.extend({
        initialize: function(){
            this.eventMessage = {
                "type" : "sendEvents",
                    "eventInfo": {
                    "type": "opt-in",
                        "params":{
                        // "isOptIn": true
                    }
                },
                "checkInPastEvent": false
            }
        },

        setOptInParam: function(isOptIn){
            var mode = "in";
            if(isOptIn == true)
                mode = "in";
            else
                mode = "out";

            this.eventMessage.eventInfo.params["isOptIn"] = mode;
        },

        setSentTime: function () {
            this.eventMessage.eventInfo.params["timestamp"] = new Date();
        },

        getJsonMessageEvent: function () {
            return JSON.stringify(this.eventMessage);
        },

        onSuccessCallback: function(){
        },

        onErrorCallback: function(){}
    });
})(window, window.Personetics = window.Personetics || {}, jQuery);
(function (window, Personetics, $, undefined) {

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    var pEventReporting = Personetics.utils.PEvent.extend({

        initialize: function () {
            this.params = {
                "eventType": "sendEvents",
                "checkInPastEvent": "false",
                "protocolVersion": Personetics.projectConfiguration.getConfig("protocolVersion"),
                "userId": "",
                "type": "",
                "value": "",
                "insightId": "",
                "instanceId": ""
            }
        },

        setValue: function (parameters) {
            var me = this;
            if (typeof parameters !== 'object' || parameters === null)
                return false;
            $.each(parameters, function (key, value) {
                if (me.params.hasOwnProperty(key) && value != 'undefined' && value != null) {
                    me.params[key] = value;
                }
            })
        },

        createMessage: function () {
            var params = this.params;
            var eventMessage = {
                "type": params.eventType,
                "userId": params.userId,
                "checkInPastEvent": params.checkInPastEvent,
                "protocolVersion": params.protocolVersion,
                "eventInfo": {
                    "type": params.type,
                    "eventDateTime": this.getFormattedDate(),
                    "params": {
                        "value": params.value,
                        "insightId": params.insightId,
                        "instanceId": params.instanceId
                    }
                },

            }
            return eventMessage;
        },

        notifyEvent: function () {
            var pevent = {
                eventMessage: this.createMessage()
            }
            personetics.notifyEvent(pevent);
        },
        getFormattedDate: function () {
            var currentDate = new Date();
            var currentDateString = (currentDate.getUTCMonth() + 1) + "/" + currentDate.getUTCDate() + "/" + currentDate.getUTCFullYear() + " " + currentDate.getUTCHours() + ":" + currentDate.getUTCMinutes() + ":" + currentDate.getUTCSeconds();
            return currentDateString;
        }

        /*getJsonMessageEvent: function () {
            return JSON.stringify(this.eventMessage);
        },

        onSuccessCallback: function(){
        },

        onErrorCallback: function(){
        }*/
    });

    Personetics.utils.PEventReporting = new pEventReporting();

})(window, window.Personetics = window.Personetics || {}, jQuery);

(function (window, Personetics, $, undefined) {

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    var pEventReporting = Personetics.utils.PEvent.extend({

        initialize: function () {
            this.params = {
                "eventType": "sendEvents",
                "checkInPastEvent": "false",
                "protocolVersion": Personetics.projectConfiguration.getConfig("protocolVersion"),
                "userId": "",
                "type": "",
                "value": "",
                "insightId": "",
                "instanceId": ""
            }
        },

        setValue: function (parameters) {
            var me = this;
            if (typeof parameters !== 'object' || parameters === null)
                return false;
            $.each(parameters, function (key, value) {
                if (me.params.hasOwnProperty(key) && value != 'undefined' && value != null) {
                    me.params[key] = value;
                }
            })
        },

        createMessage: function () {
            var params = this.params;
            var date = new Date();
            if (params.eventType == "sendEvents")   
            {         
                var datestr = date.toISOString();
                date = Personetics.utils.date.monthNumber(datestr) + '/' + Personetics.utils.date.dayNumber(datestr, "DD") + '/' + Personetics.utils.date.yearNumber(datestr);
            }
            var eventMessage = {
                "type": params.eventType,
                "userId": params.userId,
                "checkInPastEvent": params.checkInPastEvent,
                "protocolVersion": params.protocolVersion,
                "eventInfo": {
                    "type": params.type,
                    "eventDateTime": this.getFormattedDate(),
                    "params": {
                        "value": params.value,
                        "insightId": params.insightId,
                        "instanceId": params.instanceId
                    }
                },

            }
            return eventMessage;
        },

        notifyEvent: function () {
            var pevent = {
                eventMessage: this.createMessage()
            }
            personetics.notifyEvent(pevent);
        },
        getFormattedDate: function () {
            var currentDate = new Date();
            var currentDateString = (currentDate.getUTCMonth() + 1) + "/" + currentDate.getUTCDate() + "/" + currentDate.getUTCFullYear() + " " + currentDate.getUTCHours() + ":" + currentDate.getUTCMinutes() + ":" + currentDate.getUTCSeconds();
            return currentDateString;
        }

        /*getJsonMessageEvent: function () {
            return JSON.stringify(this.eventMessage);
        },

        onSuccessCallback: function(){
        },

        onErrorCallback: function(){
        }*/
    });

    Personetics.utils.PEventReporting = new pEventReporting();

})(window, window.Personetics = window.Personetics || {}, jQuery);

(function(window, Personetics, $, undefined){

    var dict = function dict() {
        this.texts = {
            "en": {
                "lblaccSelectAll": "All accounts",
                "lblaccSelectAccounts": "Accounts",
                "lblaccSelectChoose": "Choose account",
                "lblaccSelectAccount": "Account No.",
                "lblaccSelectAccountAccessibility": "Account number",
                "lblAccessibiliotyAccountSelectorText": "This is an account selector",
                "lbljanuary": "January",
                "lblfebruary": "February",
                "lblmarch": "March",
                "lblapril": "April",
                "lblmay": "May",
                "lbljune": "June",
                "lbljuly": "July",
                "lblaugust": "August",
                "lblseptember": "September",
                "lbloctober": "October",
                "lblnovember": "November",
                "lbldecember": "December",
                "lbljanShort": "JAN",
                "lblfebShort": "FEB",
                "lblmarShort": "MAR",
                "lblaprShort": "APR",
                "lblmayShort": "MAY",
                "lbljunShort": "JUN",
                "lbljulShort": "JUL",
                "lblaugShort": "AUG",
                "lblsepShort": "SEP",
                "lbloctShort": "OCT",
                "lblnovShort": "NOV",
                "lbldecShort": "DEC",
                "lblstoryRating": "How helpful was this insight?",
                "lblfeedbackprompt1": "What didn't you like about this insight?",
                "lblfeedbackprompt2": "What didn't you like about this insight?",
                "lblfeedbackprompt3": "Tell us your opinion of this insight",
                "lblfeedbackprompt4": "Great! Tell us why you liked this insight",
                "lblfeedbackprompt5": "Great! Tell us why you liked this insight",
                "lblfeedbackdoubleclick": " ,double tap to edit",
                "lblfeedbackpressclick": " ,press enter to edit",
                "btnsubmit": "Submit",
                "lblreactionFeedback": "Thank you for the feedback!",
                "txlistAccNumber": "Account",
                "txlistDescription": "Transaction",
                "txlistDate": "Date",
                "txlistAmount": "Amount",
                "lblgoToInbox": "View All",
                "lbllogout": "Log Out",
                "lblmode": "Mode",
                "lblpreviewMode": "Preview",
                "lblbubbleMode": "Bubble",
                "ttlinbox": "Inbox",
                "ttlinboxstory": "Insights Inbox",
                "ttlinboxAll": "All",
                "ttlinboxUnread": "Unread",
                "ttlinboxAllInsights": "All insights",
                "ttlinboxUnreadInsights" : "Unread insights",
                "ttlinboxFlagged": "Flagged",
                "ttlinboxHighlighted": "Highlighted",
                "insightsNoInsighsTexts": "Nothing to report",
                "insightsTitle": "Insights",
                "insightsOptOutTeaserText": "You've chosen not to receive insights",
                "insightsLoadInsights": "Checking for Insights",
                "ttlsettings": "Advanced Settings",
                "lblsettingsServer": "Server",
                "lblsettingsChannel": "Channel",
                "lblpasswordMode": "Password Required",
                "ttlloginPage": "Sign In",
                "lbluser": "User",
                "lblpassword": "Password",
                "btnlogin": "Continue",
                "btnadvanced": "Advanced",
                "insightsNoInsightsAccessibilityText": "There are no new insights",
                "insightsOptOutTeaserAccessibilityText": "You have chosen not to receive insights",
                "accessFloatingButton": "Launcher",
                "insightsErrorText": "There's been a problem loading Insights. Check back soon.",
                "storyErrorText": "There's been a problem loading story. Check back soon.",
                "accessibilityStoryBackButtonText": "back",
                "teaserErrorText": "There's been a problem loading teaser. Check back soon",
		        "accessibilityPieTitle": "This is a Pie chart – it shows a breakdown of sums",
	    	    "accessibilityNextCategory": "Double tap to view next category.",
                "accessibilityPreviousCategory": "Double tap to view previous category.",
                "accessibilitySelectedCategoryTitle": "Selected category",
                "accessibilitySliceLbl":"slice",
                "accessibilityOfLbl": "of",
                "accessibilityBarChartTitle": "this is a bar chart",
                "accessibilityBarChartPureTitle": "A dynamic bar chart is displayed, to navigate between the available months in the chart, select the months available.",
                "accessibilityPinGraphTitle": "this is a pin graph",
                "accessibilityLineGraphTitle": "this is a line chart",
                "accessibilityLineGraphHighlightedDot": "highlighted dot",
                "accessibilitySeries": "series",
                "accessibilitySeriesIn": "series in",
                "accessibilitySeriesOut": "series out",
                "accessibilityOutOfTxt": "out of",
                "accessibilitySelected": "selected",
                "accessibilityNotSelected": "not selected",
                "accessibilityHighlighted": "highlighted",
                "accessibilityPinGraph":"A pin chart list of transactions’ amount per date is displayed.",
                "accessibilityTableTitle": "This table presents a list of transactions",
                "accessibilityTableStart": "Table start",
                "accessibilityTableEnd": "Table end.",
                "accessibilityRatingLevelTitle" : "rating level",
                "accessibilityRadioButton":"Radio button",
                "accessibilityChecked": "checked",
                "accessibilityUnchecked": "unchecked",
                "multiTeasersTitleText": "Smart Engage",
                "accessibilityNextTeaser": "click to select next teaser",
                "accessibilityPrevTeaser": "click to select prev teaser",
                "lblbalance":"Balance",
                "lblaccSelectNextAcc":"show next account",
                "lblaccSelectPreviousAcc":"show previous account",
                "accessibilityIntroducePersonetics":"Story: Personetics Engage is a new breed of banking solution that puts your needs first. It provides timely and useful insights that keep you informed and help you stay on top of your financial affairs.  Account Activity: We will discover your account activity trends, highlight important events, and flag unusual events.Cash flow forecast: We will predict your cash flow, inform you ahead of time if your balance may not be enough to cover upcoming expenses and remind you about upcoming payments. Financial Education: We will suggest specific steps to increase savings, reduce debt, and improve your financials. In addition, we can help you create and automate a savings plan to meet your financial objectives.  Spending Analysis: We will help you compare your spending between months and across categories, so you can see where your money goes and how your spending changes. Customize your Insights: Get more out of Personetics by rating the insights that you receive. That way, we can adjust what we share with you and focus on what matters most.",
                "noInsigthsTextFirstMsg":"There are no insights for you right now",
                "noInsigthsTextSecondMsg":"We will let you know when you receive new ones",
                "nothingToShowHere":"We didn't find anything to show here",
                "inboxTechnicalIssues":"Something is wrong. Please try again or contact Personetics support",
                "lblScheduled":"Scheduled",
                "lblEstimated":"Estimated",
                "lblAnticipated": "anticipated",
                "whoops":"Whoops",
                "invalidInsigthsErrMsg":"This insight seems to be invalid.Please Choose a diffrent one or contact Personetics support.",
                "wrong":"Wrong",
                "right":"Right",
                "OhNo":"Oh No!",
                "lblIn":"in",
                "lblOut":"out",
                "loading": "Loading...",
                "close": "Close",
                "feedBy": "Feed by",
                "continue": "Continue",
                "previous": "Previous",
                "noJSONsAvailable": "No JSONs are available in this path",
                "chooseInsights": "Choose one of the insights",
                "TryAgain":"Try Again",
                "lblTab":"tab",
                "lblOf": "of",
                "lastPaymentlbl":"Last Payment: ",
                "lblInsight": "insight",
                "closeInboxBtnlbl": "close inbox button ,double tap to close inbox"
            }
        }
    };

    dict.prototype.getText = function getText(textId, fallbackText){
        var result = fallbackText || textId;
        var langDict;
        var lang = this.lang;
        if(this.texts.hasOwnProperty(lang))
            langDict = this.texts[lang];
        else
            langDict = this.texts.en;

        if(langDict.hasOwnProperty(textId))
            result = langDict[textId];

        if(result == textId)
            Personetics.log("Personetics.dictionary.getText('" + textId + "', '" + lang + "') Error: failed to find text");

        return result;
    };

    dict.prototype.overrideLanguageTexts = function overrideLanguageTexts(lang, texts) {
        if(!this.texts.hasOwnProperty(lang)) {
            this.texts[lang] = texts;
        }
        else {
            for(var textId in texts) {
                if(texts.hasOwnProperty(textId)) {
                    this.texts[lang][textId] = texts[textId];
                }
            }
        }
    };

    dict.prototype.setLanguage = function setLanguage(langToConfig){
        this.lang = langToConfig;
    };

    Personetics.dictionary = new dict();

})(window, window.Personetics = window.Personetics || {}, jQuery);

(function(window, Personetics, $, undefined){
	Personetics.API = Personetics.API || {};
	Personetics.API.PServerProxy = Base.extend({
		initialize: function() {
			this.okStatus = "200";
			this.delay = 3000;
			this.requestDataType = "json";
			this.pserverURL = "";
			this.servletName = ""//"/execute";
			this.requestHeaders = {
				"Content-Type": "application/json; charset=UTF-8"
			};
			this.requestParams = {
			};
			this.config = {
			};
			this.postData = {
			};
		},

		start: function(config){
			this.config = $.extend(true, this.config, config);

			if(this.config.protocolVersion)
				this.postData["protocolVersion"] = this.config.protocolVersion;
			if(this.config.lang)
				this.postData["lang"] = this.config.lang;
			if(this.config.deviceType)
				this.postData["deviceType"] = this.config.deviceType;

			this.requestHeaders = $.extend(true, this.requestHeaders, config.requestHeaders);
			this.requestParams = $.extend(true, this.requestParams, config.requestParams);
			
			if (config.jwt)
				this.config.requestHeaders["Authorization"] = "Bearer " + config.jwt;

			if(config.userId)
				this.config.requestHeaders["authToken"] = config.userId;

			this.pserverURL = config.pserverUrl;

			this.base64EncodeResponse = true; // always decode by default
			if(typeof config.base64EncodeResponse !== 'undefined' || config.base64EncodeResponse === null)
				this.base64EncodeResponse = config.base64EncodeResponse;

			this.requestDataType = this.base64EncodeResponse ? "text" : "json";
		},
		createURL: function(data, requestParams){

			var result = this.pserverURL + this.servletName;

			if(requestParams){
				result += "?" + $.param(requestParams);
			}
			
			return result;
		},

		notifyEvent: function(postParams, success, failure){
			var options = {
				postData: postParams
			};
			this.getData(options, success, failure);
		},

		getDataSuccessCallback: function(response, successCallback, failureCallback){
			if(response) {
				var json = response;

				if(this.base64EncodeResponse){
					json = JSON.parse(Personetics.utils.encodeDecode.Base64.decode(response));
				}
				else{
					if(Personetics.utils.assert.isString(response))
						json = JSON.parse(response);
				}

				Personetics.log("---response:---\n" + JSON.stringify(json));

				var status = json.status;
				var ok = json.ok;

				var isOk = (status && status == this.okStatus) || (ok && ok == true);

				if (!json || !isOk) {
					this.onError(failureCallback, json);
				}
				else {
					if ($.isFunction(successCallback)) {
						successCallback(json);
					}
				}
			}
			else{
				this.onError(failureCallback, response);
			}
		},

		getDataFailureCallback: function(XMLHttpRequest, textStatus, errorThrown, successCallback, failureCallback){
			Personetics.utils.PLogger.error(textStatus);
			this.onError(failureCallback,textStatus);//failureCallback(textStatus);
		},

		onError: function (errorCallback, errorData){
			var bridge = personetics.getJSBridge();

			if (bridge.hasOwnProperty("sessionError") &&
				$.isFunction(bridge.sessionError))
					bridge.sessionError(errorData);
			if (typeof errorCallback !== "undefined" && $.isFunction(errorCallback)){
				errorCallback(errorData);
			}
		},

		prepareData: function(options){
			var data = $.extend(true,{}, this.postData, options.postData);
			var requestParams = options.requestParams;

			var reqUrl = this.createURL(data, requestParams);
			var postData = JSON.stringify(data);

			var result = {
				reqUrl: reqUrl,
				postData: postData
			};

			return result;
		},

		sendRequest: function(preparedData, successCallback, failureCallback){
			Personetics.log("---request url: " + preparedData.reqUrl + " data: ", JSON.stringify(preparedData.data));

			var me = this;
			Personetics.utils.network.doAjaxPost(preparedData.reqUrl, preparedData.postData, {

				dataType: this.requestDataType,
				headers: me.requestHeaders,
				onSuccess: function (response) {
					me.getDataSuccessCallback(response, successCallback, failureCallback);
				},
				onFailure: function (XMLHttpRequest, textStatus, errorThrown) {
					me.getDataFailureCallback(XMLHttpRequest, textStatus, errorThrown, successCallback, failureCallback);
				}
			});
		},

		getData: function(options, successCallback, failureCallback){
            options.postData["ctxId"] = Personetics.projectConfiguration.getApiCtxId(options.postData.type, this.config.ctxId);
			var preparedData = this.prepareData(options);
			this.sendRequest(preparedData, successCallback, failureCallback);
		}
	});
})(window, window.Personetics = window.Personetics || {}, jQuery);

(function(window, Personetics, $, undefined){
	Personetics.API = Personetics.API || {};
	Personetics.API.PServerProxyNative = Personetics.API.PServerProxy.extend({

		initialize: function(){
			Personetics.API.PServerProxy.prototype.initialize.call(this);

			this.sentRequestsTypeToCallbacks = {};
		},

		getData: function(data, successCallback, failureCallback){
			var postParam = {};
			var postData = data.postData;
			var type = postData.type;
            $.each(this.postData, function(id, name){
                postParam[id] = name;
            });
            delete this.postData.eventsInfoList;
			$.each(postData, function(id, name){
				postParam[id] = name;
			});

			//add ctxId to specific APIs
            postParam["ctxId"] = Personetics.projectConfiguration.getApiCtxId(data.postData.type, this.config.ctxId);

			var requestId = Personetics.utils.encodeDecode.Base64.encode("reqPersonetics" + type + Date.now());

			var bridge = personetics.getJSBridge();
			if (Personetics.utils.assert.isDefined(bridge, false) &&
				bridge.hasOwnProperty("sendRequestToPServer") &&
				Personetics.utils.assert.isFunction(bridge.sendRequestToPServer)) {

				this.registerRequestById(requestId, type, successCallback, failureCallback);
                bridge.sendRequestToPServer(this.requestHeaders,postParam, requestId);
			}
		},

		handlePServerResponse: function(data, requestId){
			if(this.base64EncodeResponse){
				data = Personetics.utils.encodeDecode.Base64.decode(data);
				data = $.parseJSON(data);
			}
			var success = (data.hasOwnProperty("ok") && data.ok == true) || data.status == 200;
			if(this.sentRequestsTypeToCallbacks.hasOwnProperty(requestId)) {
				var callbacks = this.sentRequestsTypeToCallbacks[requestId];
				if (success == true && typeof callbacks.onSuccess !== 'undefined')
					callbacks.onSuccess(data);
				else{
					this.onError(callbacks.onError, data);
				}
			}
		},

		registerRequestById: function(requestId, type, success_callback, error_callback){
			this.sentRequestsTypeToCallbacks[requestId] = {
				type: type,
				onSuccess: success_callback,
				onError: error_callback
			};
		}

	});
})(window, window.Personetics = window.Personetics || {}, jQuery);

(function (window, Personetics, $, undefined){
    //Personetics.UI = Personetics.UI || {};
    var Handlebars = Personetics.UI.Handlebars;
    Personetics.UI.PWidgetCtrl = Base.extend({
        initialize: function initialize(config) {
            this.config = config;
            this.widgetId = this.config.widgetId;
            this.parentCtrl = config.parentCtrl;
        },

        sendWidgetEvent: function sendWidgetEvent(params){
            params.widgetId = this.widgetId;
            if(typeof personetics !== 'undefined' && personetics !== null && personetics.hasOwnProperty("sendWidgetEvent"))
                personetics.sendWidgetEvent(this, params);
            else
                Personetics.utils.PLogger.debug("personetics JS API is not available");
        },

        getTemplateHtml: function(templateId,ctxObj) {
            var tmpl = this.getTemplate(templateId);
            var html = tmpl(ctxObj);
            return html;
        },
        getTemplate: function(templateId) {
            var template = this.getPrecompiledTemplate(templateId);
            if (!template) {
                throw "Unknown template '" + templateId + "'";
            }
            return template;
        },
        getPrecompiledTemplate: function(templateId) {
            var id = templateId;
            if (id.substr(0,1) == "#")
                id =  id.substr(1);
            var tmpl = Handlebars.templates[id];
            return tmpl;
        }
    });

})(window, window.Personetics = window.Personetics || {}, jQuery);

(function(window, Personetics, $, undefined){
    Personetics.UI = Personetics.UI || {};
    var factory = Base.extend({

        widgetFactoryMap: {},
        counter: 0,

        registerWidget: function(type, config, name) {

            // backwards compatibility
            if(typeof name !== 'undefined')
                this.registerWidgetDeprecated(type, config, name);
            else {

                var entry = this.widgetFactoryMap[type];
                if (!entry) {
                    entry = {
                        type: type,	// widget type
                        name: config.widgetName,
                        widthModes: {} // additional widgets based on screen width
                    };
                    this.widgetFactoryMap[type] = entry;
                }

                // map widget by width mode
                if (config.widthMode) {
                    entry.widthModes[config.widthMode] = config.widgetClass;
                }
                // default block mapping
                else {
                    entry.widgetClass = config.widgetClass;
                }
            }
        },

        createWidget: function(type, config) {

            var entry = this.widgetFactoryMap[type];
            var widgetClass = null;

            if (entry) {

                if(entry.class){
                    return this.createWidgetDeprecated(type, config);
                }
                else {
                    if (config.hasOwnProperty("widthMode") && typeof config.widthMode !== 'undefined'
                        && config.widthMode !== null && config.widthMode.length > 0) {
                        widgetClass = entry.widthModes[config.widthMode];
                    }

                    if (!widgetClass)
                        widgetClass = entry.widgetClass;
                }
            }

            if (widgetClass)
                return new widgetClass(config);
            else
                throw "Personetics.UI.PWidgetFactory.createWidget() - Invalid widget type '" + type + "'";
        },

        registerWidgetDeprecated: function (type, clazz, name){
            if(type && type.length > 0 && clazz){
                this.widgetFactoryMap[type] = {
                    class: clazz,
                    name: name || type
                };
            }
        },

        createWidgetDeprecated: function(type, config){
            if(this.widgetFactoryMap.hasOwnProperty(type)) {
                var clazz = this.widgetFactoryMap[type].class;
                var widgetId = type + "_" + Date.now() + "_" + this.counter;
                this.counter++;
                config.widgetId = widgetId;
                return new clazz(config);
            }
            else {
                throw "Personetics.UI.PWidgetFactory.createWidget() - Invalid widget type '" + type +  "'";
            }
        }
    });

    Personetics.UI.PWidgetFactory = new factory();

})(window, window.Personetics = window.Personetics || {}, jQuery);

(function (window, Personetics, $, undefined) {
    Personetics.API = Personetics.API || {};
    Personetics.API.personetics = new function () {

        this.isInitialized = false;

        this.pserverProxyClassName = null;

        this.config = {
            protocolVersion: Personetics.projectConfiguration.getConfig("protocolVersion"),
            enableNotifyEvents: Personetics.projectConfiguration.getConfig("enableNotifyEvents")
        };

        this.urls = {
            INSIGHTS_SERVLET: "/insights/",
            STORY_SERVLET: "/story/",
            EXECUTE_SERVLET: "/execute",
            SERVICE_SERVLET: "/service/",
            INIT_PERSONETICS: "initPersonetics",
            GET_INSIGHTS: "getInsights",
            GET_INBOX_INSIGHTS: "getInboxInsights",
            GENERATE_INSIGHTS: "generateInsights",
            GET_INSIGHT_RATINGS: "getInsightRating",
            UPDATE_INSIGHT_RATINGS: "updateInsightRating",
            UPDATE_INSIGHT_FEEDBACK: "updateInsightFeedback",
            GET_INSIGHT_STORY: "getInsightStory",
            GET_INSIGHT_DETAILS: "getInsightDetails",
            GET_INSIGHT_STORY_DATA: "getInsightStoryData",
            GET_INSIGHT_STORY_DEFINITION: "getInsightStoryDefinition",
            GET_INSIGHT_STORY_TEXTS: "getInsightStoryTexts",
            GET_STORY_QUERY_DATA: "getStoryQueryData",
            GET_MESSAGE_HISTORY: "messageHistory"
        };

        /**
         * personetics.initPersonetics
         * @param config
         * @param onSuccess
         * @param onFailure
         */
        this.initialize = function (config, onSuccess, onFailure) {
            window.skinSetup = new SkinSetup();
            window.skinSetup.init();
            this.config = $.extend(true, this.config, config);
            this.config.requestHeaders = this.config.requestHeaders || {};
            this.config.requestHeaders["authToken"] = this.config.userId;

            // only initialize once
            if (!this.isInitialized) {
                Personetics.utils.PLogger.setDebugMode(config.debugMode);
                Personetics.log("personetics.initialize() called");

                // PServer request manager
                var pserverProxyClassName = this.pserverProxyClassName;
                if (pserverProxyClassName) {
                    this.pserverProxy = new pserverProxyClassName();
                }
                else {
                    if (typeof Personetics.API.PServerProxyNative !== 'undefined' && Personetics.API.PServerProxyNative) {
                        this.pserverProxy = new Personetics.API.PServerProxyNative();
                    } else {
                        this.pserverProxy = new Personetics.API.PServerProxy();
                    }
                }
                this.pserverProxy.start(this.config);

                // Events delegate - use default if not provided by customer
                if (this.config.hasOwnProperty("pDelegate") && this.config.pDelegate) {
                    this.eventsDelegate = this.config.pDelegate;
                }
                else {
                    this.eventsDelegate = new EventDelegate();
                }

                // Notify events manager
                    this.eventBuffer = new Personetics.utils.PEventsBuffer();
                    this.eventBuffer.start();

                // Skin
                this.skinController = new Personetics.story.skinController();
                this.skinController.registerWidgets();

                // Do initPersonetics call or invoke success callback
                if (this.config.hasOwnProperty("callInitPersonetics") && this.config.callInitPersonetics == true) {
                    this.initPersonetics(function (response) {
                        this.isInitialized = true;
                        onSuccess(response);
                    }.bind(this),
                        onFailure);
                } else {
                    this.isInitialized = true;
                    if (onSuccess)
                        onSuccess();
                }
            }
            else {
                onSuccess();
            }
        };

        /**
         * personetics.notifyEvent
         * @param eventObj
         * @param onSuccess
         * @param onFailure
         */
        this.notifyEvent = function (eventObj, onSuccess, onFailure) {
            if (this.eventBuffer && ((this.config.hasOwnProperty("enableNotifyEvents") && this.config.enableNotifyEvents == true)
                    || eventObj.eventMessage.eventInfo.hasOwnProperty("type") && eventObj.eventMessage.eventInfo.type === "Navigation")) {
                this.eventBuffer.addEventToSend(eventObj, onSuccess, onFailure);
            }
        };

        /**
         * personetics.startWidget
         * @param config
         */
        this.startWidget = function (config) {

            if (typeof config === 'undefined' || config === null)
                throw { "message": "personetics.startWidget Error: invalid widget configuration object" };

            Personetics.log("personetics.startWidget() called");

            var el = $("body");
            if (config.hasOwnProperty("el"))
                el = config.el;


            var onError = function (error) {
                Personetics.log("Failed to call initPersonetics");
                var firstWidget = Personetics.UI.PWidgetFactory.createWidget("master", config);
                firstWidget.start(el);
            };

            var onPersoneticsInitSuccess = function () {
                var widgetType = "master";
                var firstWidget = Personetics.UI.PWidgetFactory.createWidget(widgetType, config);
                firstWidget.start(el);
                if(config.hasOwnProperty("callbackOnSuccess") && typeof config.callbackOnSuccess !== "undefined")
                {
                    config.callbackOnSuccess();
                }
            };

            // if startWidget is called before initialize, initialize personetics first
            if (!this.isInitialized) {
                config.base64EncodeResponse = false;
                this.initialize(config, onPersoneticsInitSuccess, onError);
            }
            else {
                onPersoneticsInitSuccess();
            }
        };

        this.initPersonetics = function initPersonetics(onSuccess, onFailure) {
            var type = this.urls.INIT_PERSONETICS;
            var data = {
                type: type,
                mode: this.config.mode,
                predefinedFile: this.config.predefinedFile,
                isPredefined: this.config.isPredefined
            };

            var options = {
                postData: data
            };

            this.pserverProxy.getData(options, onSuccess, onFailure);
        };
        this.getConfig = function getConfig() {
            return this.config;
        };

        // this.setConfig = function setConfig(apiConfig){
        //     Personetics.projectConfiguration.setConfig(apiConfig);
        // };
        /**
         * personetics.getInsights
         * @param bankInfo
         * @param onSuccess
         * @param onFailure
         */
        this.getInsights = function getInsights(bankInfo, onSuccess, onFailure, autoGenerate) {
            var type = this.urls.GET_INSIGHTS;
            var data = {
                type: type,
                bankInfo: bankInfo
            };

            if (typeof autoGenerate !== 'undefined' && autoGenerate != null) {
                data.autoGenerate = autoGenerate;
            }

            var options = {
                postData: data
            };

            this.pserverProxy.getData(options, onSuccess, onFailure);
        };

        /**
         * personetics.getInboxInsights
         * @param bankInfo
         * @param onSuccess
         * @param onFailure
         */
        this.getInboxInsights = function getInbox(bankInfo, onSuccess, onFailure) {
            var type = this.urls.GET_INBOX_INSIGHTS;
            var data = {
                type: type,
                bankInfo: bankInfo
            };

            var options = {
                postData: data
            };

            this.pserverProxy.getData(options, onSuccess, onFailure);
        };

        this.getInsightStory = function getInsightStory(insightData, onSuccess, onFailure) {
            var type = this.urls.GET_INSIGHT_STORY;
            var servletName = this.urls.STORY_SERVLET;

            if (!this.config.useServiceServlet)
                insightData["servletName"] = servletName;



            if (insightData.hasOwnProperty("id") && insightData.hasOwnProperty("insightId")) {
                insightData["type"] = type;
                var options = {
                    postData: insightData
                };

                this.pserverProxy.getData(options, onSuccess, onFailure);
            } else {
                throw "personetics.getInsightStory(): Usage: getInsightStory(id, insightId)";
            }
        };

        this.getInsightDetails = function getInsightDetails(insightData, onSuccess, onFailure) {
            if (insightData.hasOwnProperty("insightId")) {
                var type = this.urls.GET_INSIGHT_DETAILS;

                insightData["type"] = type;

                var options = {
                    postData: insightData
                };

                this.pserverProxy.getData(options, onSuccess, onFailure);
            }
            else {
                throw "personetics.getInsightDetails(): Usage: getInsightDetails(userId, insightId)";
            }
        };

        this.getInsightStoryData = function getInsightStoryData(insightData, onSuccess, onFailure) {
            var type = this.urls.GET_INSIGHT_STORY_DATA;

            var data = {
                type: type
            };

            var options = {
                postData: data
            };

            if (insightData.hasOwnProperty("id") && insightData.hasOwnProperty("insightId")) {
                this.pserverProxy.getData(options, onSuccess, onFailure);
            } else {
                throw "personetics.getInsightStoryData(): Usage: getInsightStoryData(id, insightId)";
            }
        };

        this.getInsightStoryDefinition = function getInsightStoryDefinition(insightData, onSuccess, onFailure) {
            var type = this.urls.GET_INSIGHT_STORY_DEFINITION;

            var data = {
                type: type
            };

            var options = {
                postData: data
            };

            if (insightData.hasOwnProperty("id") && insightData.hasOwnProperty("insightId")) {
                this.pserverProxy.getData(options, onSuccess, onFailure);
            } else {
                throw "personetics.getInsightStoryDefinition(): Usage: getInsightStoryDefinition(id, insightId)";
            }
        };

        this.getInsightStoryTexts = function getInsightStoryTexts(insightData, onSuccess, onFailure) {
            var type = this.urls.GET_INSIGHT_STORY_TEXTS;

            var data = {
                type: type
            };

            var options = {
                postData: data
            };

            if (insightData.hasOwnProperty("id") && insightData.hasOwnProperty("insightId")) {
                this.pserverProxy.getData(options, onSuccess, onFailure);
            } else {
                throw "personetics.getInsightStoryTexts(): Usage: getInsightStoryTexts(id, insightId)";
            }
        };

        this.getInsightMessages = function getInsightMessages(data, onSuccess, onFailure) {
            var type = this.urls.GET_MESSAGE_HISTORY;

            var options = {
                postData: {
                    type: type
                }
            };

            this.pserverProxy.getData(options, type, onSuccess, onFailure);
        };

        this.generateInsights = function generateInsights(bankInfo, onSuccess, onFailure) {
            var type = this.urls.GENERATE_INSIGHTS;
            var data = {
                bankInfo: bankInfo
            };

            var options = {
                postData: data
            };

            this.pserverProxy.getData(options, type, onSuccess, onFailure);
        };

        this.getInsightRating = function getInsightRating(data, onSuccess, onFailure) {
            var type = this.urls.GET_INSIGHT_RATINGS;
            data.type = type;
            var options = {
                postData: data
            };

            if (data.hasOwnProperty("id")) {
                this.pserverProxy.getData(options, onSuccess, onFailure);
            } else {
                throw "personetics.getInsightRating(): Usage: getInsightRating(id)";
            }
        };

        this.updateInsightRating = function updateInsightRating(data, onSuccess, onFailure) {
            var type = this.urls.UPDATE_INSIGHT_RATINGS;

            data.type = type;
            var options = {
                postData: data
            };

            if (data.hasOwnProperty("id") && data.hasOwnProperty("rating")) {
                this.pserverProxy.getData(options, onSuccess, onFailure);
            } else {
                throw "personetics.updateInsightRating(): Usage: updateInsightRating(id, rating)";
            }
        };
        this.updateInsightFeedback = function updateInsightFeedback(data, onSuccess, onFailure) {
            var type = this.urls.UPDATE_INSIGHT_FEEDBACK;
            data.type = type;

            var options = {
                postData: data
            };

            if (data.hasOwnProperty("id")) {
                this.pserverProxy.getData(options, onSuccess, onFailure);
            } else {
                throw "personetics.updateInsightFeedback(): Usage: updateInsightFeedback(id, rating)";
            }
        };

        this.getQueryData = function (data, onSuccess, onFailure) {
            var type = this.urls.GET_STORY_QUERY_DATA;
            var servletName = this.urls.STORY_SERVLET;

            data.type = type;
            if (!this.config.useServiceServlet)
                data.servletName = servletName;

            var options = {
                postData: data
            };

            if (data.hasOwnProperty("queryName") && data.hasOwnProperty("queryParams")) {
                this.pserverProxy.getData(options, onSuccess, onFailure);
            } else {
                throw "personetics.getQueryData(): Usage: getQueryData(queryName, queryParams)";
            }
        };

        this.handlePServerResponse = function (data, requestId) {
            if (this.pserverProxy.hasOwnProperty("handlePServerResponse") && $.isFunction(this.pserverProxy.handlePServerResponse))
                this.pserverProxy.handlePServerResponse(data, requestId)
        };

        this.sendWidgetEvent = function sendWidgetEvent(widget, eventName, params) {
            var bridge = this.getJSBridge();
            if (bridge) {
                bridge.widgetEvent(widget, eventName, params);
            }
        };

        this.getJSBridge = function () {
            return this.eventsDelegate;
        };

        this.getSkinController = function () {
            return this.skinController;
        };

        this.getDeviceType = function () {
            return this.config.deviceType;
        };

        this.getProjectConfigValue = function getProjectConfigValue(configId) {
            return Personetics.projectConfiguration.getConfig(configId);
        };

        this.setProjectConfiguration = function setProjectConfiguration(apiConfig) {
            Personetics.projectConfiguration.setConfig(apiConfig);
        }

    };
    window.personetics = Personetics.API.personetics;

})(window, window.Personetics = window.Personetics || {}, jQuery);

(function (window, Personetics, $, undefined) {

    personetics.startWidget = function (config) {
        if (typeof config === 'undefined' || config === null)
            throw { "message": "personetics.startWidget Error: invalid widget configuration object" };

        if (window.skinSetup) {
            var filter = config.filter;
            if (typeof filter === 'undefined' || filter === null)
                config.filter = config.widgetType != "teaser-widget-carousel";
        }

        var initPersoneticsConfig = $.extend(true, {}, config);

        var el = $("body");
        if (config.hasOwnProperty("el"))
            el = config.el;

        if (typeof EventDelegate !== 'undefined' && EventDelegate !== null && $.isFunction(EventDelegate))
            initPersoneticsConfig.eventsDelegate = new EventDelegate();

        initPersoneticsConfig.base64EncodeResponse = false;

        initPersoneticsConfig.requestHeaders = initPersoneticsConfig.requestHeaders || {};
        initPersoneticsConfig.requestHeaders["authToken"] = config.userId;

        var onError = function (error) {
            Personetics.log("Failed to call initPersonetics");
            var firstWidget = Personetics.UI.PWidgetFactory.createWidget("master", config);
            firstWidget.start(el);
        };

        var onPersoneticsInitSuccess = function (response) {
            if (response !== undefined) {
                if(response.hasOwnProperty('projectConfiguration') && response.projectConfiguration !== undefined)
                    Personetics.projectConfiguration.setConfig(response.projectConfiguration);
            }
            var widgetType = "master";
            var firstWidget = Personetics.UI.PWidgetFactory.createWidget(widgetType, initPersoneticsConfig);
            firstWidget.start(el);
        };

        personetics.initialize(initPersoneticsConfig,
            onPersoneticsInitSuccess,
            onError);
    };
})(window, window.Personetics = window.Personetics || {}, jQuery);