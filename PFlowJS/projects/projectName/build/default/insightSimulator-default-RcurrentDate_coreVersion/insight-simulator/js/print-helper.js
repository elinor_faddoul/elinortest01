var stylesToAddToInline =["fill",
    "background-size",
    "background-position",
    "background-repeat",
    "background-image",
    "background-color",
    "box-shadow",
    "letter-spacing",
    "font-weight",
    "font-size",
    "font-family",
    "text-align",
    "-webkit-appearance",
    "webkit-tap-highlight-color",
    "text-anchor",
    "color",
    "padding-top",
    "background",
    "text-overflow",
    "overflow",
    "display",
    "line-height",
    "white-space",
    "margin-left",
    "transform",
    "top",
    "box-sizing"];

var transform_X = null;

function beforeExportToPdf(){
    window.parent.window.postMessage(
        {'func':'beforeExportToPdf','options': $("body").html() } ,
        '*'
    );
}

function exportToPdf(options) {
    var pstoryFrame = $(".pstory-frame");
    var body = $("body");
    if ($(".perso-carousel-item.pstory-button.perso-selected").length > 1 && $(".pstory-header-button[aria-selected='true']").attr("data-all") !== "true") {//} && pstoryFrame.width() >= 790) {
        var transformValues = $('.perso-carousel-inner-wrapper').css('transform').split(',');
        transform_X = transformValues[4];
        transformValues[4] = 0;
        transformValues.join(',');
        $('.perso-carousel-inner-wrapper').css('transform', transformValues);
        $(".pstory-all-accounts-txt").find("span").css("color", "transparent");
        var px = pstoryFrame.width() < 790 ? "px" : "";
        $(".perso-carousel-item").css("left", transform_X + px);
        $(".carousel-hide-clone.perso-selected").hide();
    }
    if (pstoryFrame.width() < 790 && $(".pstory-header-button[aria-selected='true']").attr("data-all") === "true") {
        $(".pstory-all-accounts-txt").css({top: "17%", transform: "translateY(0)"});
    }
    var height = $(".perso-ratings-wrapper").height() + $(".pstory-container").height() + ($(".pstory-title-outer-wrapper").height() > 0 ? $(".pstory-title-outer-wrapper").height() + 20 : $(".pstory-title-wrapper").height()) + parseInt($(".pstory-title").css("paddingTop"), 10);
    updateRatingElement();
    body.find("line").each(function () {
        var el = $(this);
        if (el.css("stroke")) {
            el.css("stroke", el.css("stroke"));
        }
    });
    var widgetWrapper = $(".personetics-widgets-wrapper");
    options.page.internalScaleFactor = widgetWrapper.width() == 320 ? 0.8 : 1;
    if (options.page.html) {
        options.page.el = document.createElement("div");
        var titleElem = "<div style='width: auto;margin: auto;text-align: center;padding-top: 50px;margin-bottom: 50px;font-size: 22px;'>" + options.fileName + "</div>";
        options.page.el.innerHTML = titleElem + options.page.html;
        var newElement = options.page.el.lastChild;
        newElement.style.width = options.page.width;
        newElement.style.margin = "auto";
        options.page.el.style.height = "500px";
        var teaser = options.page.el.getElementsByClassName("perso-multi-teaser-template")[0];
        if(teaser){
            teaser.style.backgroundColor = "#fff";
            teaser.style.overflow = "visible";
            var headerContent = $(teaser).find(".perso-teaser-header .pstory-block-content");
            headerContent.css({top: 0, transform: "translateY(0)"});
        }
    }
    var params = {
        el: $(".personetics-widgets-wrapper"),
        x: widgetWrapper.width() == 320 ? 175 : 0,
        y: widgetWrapper.width() == 320 ? 20 : 0,
        fileName: options.fileName,
        height: height,
        parent: document.body,
        callback: resetAfterPrint,
        format: ".pdf",
        internalScaleFactor: 3.75,
        page: options.page,
        beforeExportCallback: function () {
            if(options.page.el) {
                document.body.appendChild(options.page.el);
            }
            var children = document.body.getElementsByTagName("*");
            makeBackgroundImgAsBase64(children);
            addInlineStyle(children);
        }
    }
    pstoryFrame.animate({scrollTop: 0}, 500, function () {
        setTimeout(function () {
            Personetics.utils.print.printHtmlToPdf(params);
        }, 300);
    });
}

function makeBackgroundImgAsBase64(children){
    for (var i = 0; i < children.length; i++) {
        var background_url = window.getComputedStyle(children[i], null).getPropertyValue("background-image");
        if (background_url !== "none") {
            if(background_url.indexOf("data:") == -1){
                var pathArr = background_url.split("/");
                var className = pathArr[pathArr.length - 1].split(".")[0]
                children[i].className = children[i].className + " " + className;
            }
        }
    }
}

function addInlineStyle(children){
    for (var i = 0; i < children.length; i++) {
        var el = children[i];
        var computedStyle = getComputedStyle(children[i]);
        for(var k=0; k<stylesToAddToInline.length; k++){
            var propertyValue = computedStyle.getPropertyValue(stylesToAddToInline[k]);
            if(propertyValue) {
                if(stylesToAddToInline[k] === "letter-spacing"){
                    el.style[stylesToAddToInline[k]] = 0;
                }else {
                    el.style[stylesToAddToInline[k]] = propertyValue;
                }
            }
        }
    }
}

function updateRatingElement(){
    var parent = $(".perso-ratings-icons");
    var labels = $(".perso-ratings-icons label");
    var input = $(".perso-ratings-icons input[type='radio']");
    if($(".personetics-widgets-wrapper").width() == 320){
        var ratingsIcons = $(".perso-ratings-icons");
        ratingsIcons.width(ratingsIcons.width() + 2);
        labels.each(function () {
            $(this).hide();
        });
        input.each(function () {
            var el = $(this);
            var div = $("<div class='temp-rating'></div>");
            parent.append(div);
            getCssProperties(this, div.get(0));
            div.css("width", el.css("width"));
            div.css("height", el.css("height"));
            div.css("margin-left", el.css("margin-left"));
            div.show();
            el.hide();
        });
    }else {
        input.each(function () {
            $(this).hide();
        });
        labels.each(function () {
            var el = $(this);
            var div = $("<div class='temp-rating'></div>");
            parent.append(div);
            getCssProperties(this, div.get(0));
            div.css("width", el.css("width"));
            div.css("height", el.css("height"));
            div.css("margin-left", $("#" + el.attr("for")).css("margin-left"));
            div.show();
            el.hide();
        });
    }
}
function getCssProperties(element, target){
    var computedStyle = getComputedStyle(element);
    for(var k=0; k<stylesToAddToInline.length; k++){
        var propertyValue = computedStyle.getPropertyValue(stylesToAddToInline[k]);
        if(propertyValue) {
            target.style[stylesToAddToInline[k]] = propertyValue;
        }
    }
}
function resetAfterPrint(){
    $(".pstory-frame").removeAttr("style");
    window.parent.window.postMessage(
        {'func':'reloadStory'},
        '*'
    );
}