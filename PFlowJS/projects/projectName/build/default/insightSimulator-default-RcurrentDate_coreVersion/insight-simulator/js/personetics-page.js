if (window.addEventListener) {
    window.addEventListener ("message", receive, false);
}
else {
    if (window.attachEvent) {
        window.attachEvent("onmessage",receive, false);
    }
}
function receive(event) {
    var params = event.data;
    var fn = window[params.func];
    if(typeof fn === 'function') {
        fn(params.options);
    }
}

var onError = function onError(el) {
    el.empty();
    var params = {
        textLarge:Personetics.dictionary.getText("whoops"),
        textSmall:Personetics.dictionary.getText("invalidInsigthsErrMsg"),
        imgClass:"perso-insight-simulator-invalid"
    };
    var html = Personetics.utils.messages.errorMessage(params);
    el.append(html);
};

var onNoInsightSelect = function (el) {
    var validJsonsNumber = Personetics.utils.url.getParameterByName("validJsonsNumber");
    var text;
    if (validJsonsNumber == 0) {
        text = Personetics.dictionary.getText("noJSONsAvailable");
    }
    else {
        text = Personetics.dictionary.getText("chooseInsights");
    }
    el.empty();
    el.append("<div class='story-widget-no-insight'><p class='perso-story-no-insight'>" + text + "</p></div>");
};

var setStaicRating = function setStaicRating() {
    window.testData = {
        ratings: {
            rating: 3
        }
    };
};

function renderTeaser(options) {
    var el = $("body");
    var payload = options.payload;
    if (typeof payload === "undefined" || payload === null) {
        onNoInsightSelect(el);
        return;
    }
    try {
        payload = JSON.parse('[' + payload + ']');
    } catch (e) {
        onError(el);
        return;
    }
    var teaserPaylod = null;
    for (var i = 0; i < payload.length; i++) {
        if (payload[i].hasOwnProperty('insights')) {
            if (payload[i].hasOwnProperty("insights") && typeof payload[i].insights !== "undefined" && payload[i].insights.length) {
                for (var key in payload[i]) {
                    teaserPaylod = teaserPaylod || {};
                    if (key === "insights") {
                        teaserPaylod[key] = [];
                        teaserPaylod[key].push(payload[i].insights[0]);
                    }
                    else {
                        teaserPaylod[key] = payload[i][key];
                    }
                }
            }
            break;
        }
    }
    
    if (teaserPaylod) {
        teaserConfig = {
            el: el,
            widgetType: 'teaser-widget',
            lang: options.lang,
            payload: teaserPaylod,
            filter: false
        };
        personetics.startWidget(teaserConfig);
    }
    else
        onError(el);
};

var callStartWidget = function callStartWidget(config) {
    try {
        personetics.startWidget(config);
    } catch (e) {
        Personetics.utils.PLogger.error(e);
        onError(config.el);
    }
};

function getPayload(options){
    /**
     * Call start widget with params:
     * widgetType - story-widget
     * payload - get payload param
     */
    if(typeof SkinSetup !== 'undefined'){
        window.skinSetup = new SkinSetup();
        window.skinSetup.init();
    }

    var el = $("body");
    var config = {};
    var lang = options.lang;
    var payload = options.payload;
    if (typeof payload === "undefined" || payload === null) {
        onNoInsightSelect(el);
        return;
    }

    try {
        payload = JSON.parse('[' + payload + ']');
    } catch (e) {
        Personetics.utils.PLogger.error(e);
        onError(el);
        return;
    }
    var storyPayload = null;
    for(var i=0; i<payload.length; i++) {
        if (!payload[i].hasOwnProperty("insights")) {
            storyPayload = JSON.stringify(payload[i]);
            storyPayload = Personetics.utils.encodeDecode.Base64.encode(storyPayload);
            break;
        }
    }
    if(storyPayload === null)
    {
        onError(el);
        return;
    }
    config = {
        payload: storyPayload,
        isPayloadEncoded: true,
        widgetType: "story-widget",
        el: el,
        lang: lang,
        //debugMode: Personetics.PLoggerModes.DEBUG
    };
    setStaicRating();
    callStartWidget(config);

    window.parent.window.postMessage(
        {'func':'iFrameOnLoad'},
        '*'
    );
}



$(document).ready(function () {
    /**
     * Call start widget with params:
     * widgetType - story-widget
     * payload - get payload param from url
     */

    if(typeof SkinSetup !== 'undefined'){
        window.skinSetup = new SkinSetup();
        window.skinSetup.init();
    }


    var el = $("body");
    var config = {};
    var lang = Personetics.utils.url.getParameterByName("lang");
    var payload = Personetics.utils.url.getParameterByName("payload");
    if (typeof payload === "undefined" || payload === null) {
        onNoInsightSelect(el);
        return;
    }

    try {
        payload = JSON.parse('[' + payload + ']');
    } catch (e) {
        Personetics.utils.PLogger.error(e);
        onError(el);
        return;
    }
    var storyPayload = null;
    for(var i=0; i<payload.length; i++) {
        if (!payload[i].hasOwnProperty("insights")) {
            storyPayload = JSON.stringify(payload[i]);
            storyPayload = Personetics.utils.encodeDecode.Base64.encode(storyPayload);
            break;
        }
    }
    if(storyPayload === null)
    {
        return;
    }
    config = {
        payload: storyPayload,
        isPayloadEncoded: true,
        widgetType: "story-widget",
        el: el,
        lang: lang
    };
    setStaicRating();
    callStartWidget(config);
});
