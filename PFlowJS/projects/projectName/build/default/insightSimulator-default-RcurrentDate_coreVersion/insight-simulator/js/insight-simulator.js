if (window.addEventListener) {
    window.addEventListener("message", receive, false);
}
else {
    if (window.attachEvent) {
        window.attachEvent("onmessage", receive, false);
    }
}

function receive(event) {
    var params = event.data;
    var fn = window[params.func];
    if (typeof fn === 'function') {
        fn(params.options);
    }
}

var displayedFiles = [];
var validPathTemplate = /^[^\/]*(\/){1}[^\/]*(.json){1}$/;
var insigthSelected = false;

function prepareJson(json) {
    return encodeURIComponent(json);
};

function emptyInsightView() {
    $("iframe").attr("src", "insight-simulator/personetics-page.html?validJsonsNumber=" + displayedFiles.length);
};

function displayDirectoryName(file) {
    //File path is relative to selected folder
    //Get directory name by file path
    if (file.webkitRelativePath) {
        var directoryName = file.webkitRelativePath.match(/[^\/]*/);
        if (typeof directoryName !== "undefined")
            $(".perso-directory-chooser-text").text(directoryName[0]);
    } else {
        $(".perso-directory-chooser-text").text(file.name);
    }
};

function displayUrlFiles(input) {
    $.each(input.files, function (index, file) {
        if (!file.webkitRelativePath || file.webkitRelativePath.match(validPathTemplate)) {
            displayedFiles.push(file);
            var fileName = file.name.replace(".json", "");
            $(".perso-json-list-container").append("<input type='button' class='perso-list-item' value='" + fileName + "'/>");
        }
    })
};
var langs = {
    "English": "en",
    "French": "fr",
    "Hebrew": "he",
    "Spanish": "es"
};

var onError = function onError(el) {
    el.empty();
    var params = {
        textLarge: Personetics.dictionary.getText("whoops"),
        textSmall: Personetics.dictionary.getText("invalidInsigthsErrMsg"),
        imgClass: "perso-insight-simulator-invalid"
    };
    var html = Personetics.utils.messages.errorMessage(params);
    el.append(html);
};

var initLanguage = function initLanguage(langContEl) {
    var langListEl = langContEl.find(".perso-insight-lang-list");
    var langValueEl = langContEl.find(".perso-insight-lang-value");
    for (var key in langs) {
        var selectedClass = "";
        if (!langValueEl.text()) {
            langValueEl.text(key)
            selectedClass = "perso-selected"
        }
        langListEl.append("<div class='perso-insight-lang-item " + selectedClass + "' perso-lang='" + langs[key] + "'>" + key + "</div>")
    }
    langListEl.bind("click", ".perso-insight-lang-item", function (e) {
        langListEl.find(".perso-insight-lang-item").removeClass("perso-selected");
        var target = $(e.target);
        target.addClass("perso-selected");
        langValueEl.text(target.text());
        var index = $(".perso-json-list-container .perso-list-item.perso-selected").index();
        if (index !== -1) {
            renderAll(index, target.attr("lang"))
        }
    });
    langContEl.bind("click", ".perso-insight-action-arrow-icon", function (e) {
        var actionContElem = $("#perso-insight-action-cont");
        if (actionContElem.hasClass("perso-show-lang-list")) {
            actionContElem.removeClass("perso-show-lang-list")
        } else {
            actionContElem.attr("class", "perso-show-lang-list");
        }
        $("section").addClass("perso-show-list");
    });
};

function beforeExportToPdf(htmlElement){
    var title = $("#perso-insight-simulator-footer").text();
    $("iFrame.perso-story-iframe").get(0).contentWindow.postMessage({
        'func': 'exportToPdf',
        'options':
            {
                fileName: title ? title : document.title,
                page: {
                    html: htmlElement,
                    width: "320px"
                }
            }
    }, '*');
    $("#perso-insight-action-cont").removeClass("perso-show-print-list perso-show-print-list-with-delay")
}  

var initPrint = function initPrint(printContEl) {
    var printListEl = printContEl.find(".perso-insight-print-list");
    printListEl.bind("click", "button", function (event) {
        if (printListEl.find("#singleInsight").attr("checked")) {
            if ($(event.target).hasClass("perso-print")) {
                $("iFrame.perso-teaser-iframe").get(0).contentWindow.postMessage({
                    'func': 'beforeExportToPdf',
                    'options': { }
                }, '*');
            }
        }
    });

    $(".perso-insight-action-header.perso-print").bind("click", function (e) {
        var actionContElem = $("#perso-insight-action-cont");
        var addDelay = actionContElem.hasClass("perso-show-lang-list");
        if (actionContElem.hasClass("perso-show-print-list") || actionContElem.hasClass("perso-show-print-list-with-delay")) {
            actionContElem.removeClass("perso-show-print-list perso-show-print-list-with-delay")
        } else {
            actionContElem.attr("class", addDelay ? "perso-show-print-list-with-delay" : "perso-show-print-list");
        }
        $("section").addClass("perso-show-list");
    });
};

var iFrameOnLoad = function iFrameOnLoad(iFrame) {
    $("#perso-insight-simulator-story").find(".perso-spinner-wrapper").addClass("perso-hide-spinner");
    if ($(window).height() < 1580 && insigthSelected) {
        setTimeout(function () {
            $("section").removeClass("perso-show-list");
            $("#perso-insight-action-cont").removeClass("perso-show-print-list perso-show-print-list-with-delay perso-show-lang-list");
        }, 200);
        insigthSelected = false;
    }
}

var renderAll = function renderAll(index, lang) {
    $("#perso-insight-simulator-story").find(".perso-spinner-wrapper").removeClass("perso-hide-spinner");
    var reader = new FileReader();
    reader.onload = function (e) {
        $("iFrame.perso-story-iframe").get(0).contentWindow.postMessage({
            'func': 'getPayload',
            'options': {payload: e.target.result, lang: lang}
        }, '*');
        $("iFrame.perso-teaser-iframe").get(0).contentWindow.postMessage({
            'func': 'renderTeaser',
            'options': {payload: e.target.result, lang: lang}
        }, '*');
    };
    reader.readAsText(displayedFiles[index]);
    $("#singleInsight").attr("disabled", false);
    $("#singleInsight").attr("checked", true);
}

var filterInsights = function filterInsights() {
    var $searchForm, $listContainer, $items, $allFilterdItems, $msgWrapper, textValue, i;

    $searchForm = $('#serach-insights-input');
    $items = $('.perso-list-item');
    $listContainer = $('.perso-json-list-container');
    $allFilterdItems = $('.perso-json-list-container>.perso-list-item.perso-filtered');
    $msgWrapper = $listContainer.find('.perso-no-matched-items-wrapper');
    textValue = $searchForm.val().toLowerCase().trim();

    if (!$listContainer.hasClass('perso-filter-on')) {
        $listContainer.addClass('perso-filter-on');
    }

    $allFilterdItems.removeClass('perso-filtered');

    if ($items.length) {
        for (i = 0; i < $items.length; i++) {
            if ($items[i].value.toLowerCase().indexOf(textValue) > -1) {
                $($items[i]).addClass('perso-filtered');
            }
        }

        if ($msgWrapper.hasClass('perso-show-msg') && $('.perso-list-item.perso-filtered').length > 0) {
            $msgWrapper.removeClass('perso-show-msg');
        }

        //if there is no result show the message
        if ($('.perso-list-item.perso-filtered').length == 0) {
            $msgWrapper.addClass('perso-show-msg');
        }
    }
}

function reloadStory(){
    $(".perso-list-item.perso-selected").click();
}

var bindEvents = function bindEvents(langContEl){
    var $noItemsMsgWrapper = $('<div class="perso-no-matched-items-wrapper"></div>');
    var $noItemsMsg = $('<div class="perso-no-matched-items">No item match your search!</div>');
    $noItemsMsgWrapper.append($noItemsMsg);


    $("#perso-directory-chooser").change(function (e) {
        var persoJsonListContainerEl = $(".perso-json-list-container");
        var $msgWrapper = persoJsonListContainerEl.find('.perso-no-matched-items-wrapper');
        persoJsonListContainerEl.empty();
        displayedFiles = [];
        if (this.files && this.files[0]) {
            displayDirectoryName(this.files[0]);
            displayUrlFiles(this);
            persoJsonListContainerEl.append($noItemsMsgWrapper);
        }
        else {
            $(".perso-directory-chooser-text").text("invalid folder");
        }
        if (displayedFiles.length) {
            $('.perso-no-matched-items-wrapper').removeClass('perso-show-msg');
            var lang = langContEl.find(".perso-insight-lang-item.perso-selected").attr("lang");
            var listItemElem = persoJsonListContainerEl.find(".perso-list-item").eq(0);
            listItemElem.addClass("perso-selected").focus();
            $("#perso-insight-simulator-footer").text(listItemElem.val());
            $("section").addClass("perso-show-list");
            renderAll(0, lang);
        }
        else {
            emptyInsightView();
            $("#singleInsight").attr("disabled", true);
            $("#singleInsight").attr("checked", false);
            $("#allInsights").attr("disabled", true);
            $("#allInsights").attr("checked", false);
            $('.perso-no-matched-items-wrapper').addClass('perso-show-msg');
        }
    });

    $(".perso-json-list-container").bind("click", ".perso-list-item", function (e) {
        var target = $(e.target);
        insigthSelected = true;
        if (!target.hasClass("perso-list-item")) {
            return;
        }
        target.siblings().removeClass("perso-selected");
        target.addClass("perso-selected");
        $("#perso-insight-simulator-footer").text(target.val());
        var lang = langContEl.find(".perso-insight-lang-item.perso-selected").attr("perso-lang");
        var index = target.index();
        renderAll(index, lang)
    });

    $(".perso-json-list-header").bind("click", function (e) {
        $("section").toggleClass("perso-show-list");
        $("#perso-insight-action-cont").removeClass("perso-show-print-list perso-show-print-list-with-delay perso-show-lang-list");
    });

    $(".perso-hide-list-on-click").bind("click", function (e) {
        if ($(window).height() < 1580) {
            $("section").removeClass("perso-show-list");
            $("#perso-insight-action-cont").removeClass("perso-show-print-list perso-show-print-list-with-delay perso-show-lang-list");
        }
    });

    $("#serach-insights-input").bind("keyup", filterInsights);

}

$(document).ready(function () {
    $(".perso-device-item").bind("click", function() {
        $(".perso-btn").removeClass("perso-active");
        $(this).find('.perso-btn').addClass("perso-active");
        var newWidth = $(this).find('.perso-btn').data("width");
        $("section .perso-demo-phone .perso-story-iframe").css("width",newWidth);
    });
    var langContEl = $("#perso-insight-lang-cont");
    initLanguage(langContEl);
    initPrint($("#perso-insight-print-cont"));
    bindEvents(langContEl);
});

