var eventDelegate;
EventDelegate = function(){
    this.navigateToPage = function(params){
        Personetics.utils.PLogger.debug("Customer : navigateToPage, Params : " + JSON.stringify(params));

        var data = {
            requestString: "navigateToPage",
            data: params
        };
        alert(JSON.stringify(data));
    }
    this.widgetEvent = function widgetEvent(widget, params) {
        if (params.actionType) {
            if (params.actionType == "externalNavigation") {
                this.navigateToPage(params);
            } else if (params.actionType == "externalNavigationUrl") {
                window.location.href = params.payload.navigateTarget;
            }
        }
    }
};
