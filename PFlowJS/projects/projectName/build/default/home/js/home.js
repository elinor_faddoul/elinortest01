var home = function home() {
    this.url = "../personetics/execute";
    this.initialize = function initialize() {};
    this.initialize();

    // init skin setup
    if(typeof SkinSetup !== 'undefined') {
        window.skinSetup = new SkinSetup();
        window.skinSetup.init();
    }
    //TODO reset the teaserTabSelected value in ios also.
    if(typeof navigator.userAgent.match(/iPhone|iPad|iPod/i) == 'undefind'){
        localStorage.removeItem("teaserTabSelected");
    }
};
var homeWeb = function homeWeb() {

    this.initialize = function initialize() {

        this.config = {
            deviceType: "web",
            ctxId: "dashboard",
            payload: payload,
            isPayloadEncode: true,
            // debugMode: Personetics.PLoggerModes.DEBUG
        };

        if (typeof payload != 'undefined' && payload !== null) {
            payload = Personetics.utils.encodeDecode.Base64.encode(JSON.stringify(payload));
        }
        else {
            var payload = null;
        }
        // init skin setup
        window.skinSetup = new SkinSetup();
        window.skinSetup.init();

        localStorage.removeItem("teaserTabSelected");
    };
    this.initialize();

    this.getUrlParams = function getUrlParams() {
        var getParams = {
            userId: Personetics.utils.url.getParameterByName("userId"),
            pserverUrl: Personetics.utils.url.getParameterByName("pserverUrl"),
            isPredefined: Personetics.utils.url.getParameterByName("isPredefined"),
            lang: Personetics.utils.url.getParameterByName("lang"),
            teasersView: Personetics.utils.url.getParameterByName("teasersView"),
            config: Personetics.utils.url.getParameterByName("config")
        };
        return getParams;
    };

    this.callStartWidget = function callStartWidget(config) {

        if(config.widgetType != 'teaser-widget-carousel'){
            //simulate generateInsights call
            var headers = {
                "authToken": config.userId,
                "Content-Type": "application/json; charset=UTF-8"
            };

            var postParams = JSON.stringify({
                "type": "generateInsights",
                "protocolVersion": Personetics.projectConfiguration.getConfig("protocolVersion"),
                "userId": this.userId,
                "ctxId": config.ctxId,
                "lang": this.lang
            });

            var el = config.el;

            el.append("<div>Generating insights...</div>");

            $.ajax({
                url: config.pserverUrl,
                type: "POST",
                dataType: "json",
                headers: headers,
                data: postParams,
                success: function(){
                    //el.empty();
                    personetics.startWidget(config);
                },
                error: function(){
                    //el.empty();
                    $(el).children().not(".resize-sensor").remove();
                    el.append("<div>Generate insights request failed</div>");
                    console.log("Generate insights request failed");
                }
            });
        }
        else{
            config.ctxId = "sampleTopRelevant";
            personetics.startWidget(config);
        }
    };

    this.onDocumentReady = function onDocumentReady() {
        var getParams = this.getUrlParams();

        var browserData = Personetics.utils.persoBrowserDetector.getBrowserData();
        $('.perso-navbar').addClass(browserData.deviceType);
        this.config.el = $("#perso-widget-container");
        this.config.el.addClass(browserData.deviceType);
        this.config.userId = getParams.userId;
        this.config.widgetType = (getParams.teasersView == 'Carousel') ? 'teaser-widget-carousel' : 'teaser-widget';
        this.config.pserverUrl = getParams.pserverUrl;
        this.config.lang = this.lang;

        if (typeof getParams.userId !== 'undefined') {
            if (getParams.isPredefined && window.testData) {
                this.config.payload = this.buildPayLoad();
                this.config.payload.insights = window.testData.richTeaser;
                this.config.el.empty();
                personetics.startWidget(this.config);
            } else{
                this.callStartWidget(this.config);
            }

        }


        $('.close-button').click(function () {
            var deviceType = personetics.getDeviceType();
            var bridge = personetics.getJSBridge();
            if (deviceType != "web") {
                bridge.sessionEnded();
            } else {
                window.location = "login.html";
            }
        });

    };

    this.buildPayLoad = function(){
        var payload = {
            lang: this.lang,
            deviceType: "web",
            ctxId: "dashboard",
            insights:null,
            isPayloadEncode: true
        };
        return payload;
    };
};

var homeApp;
$(document).ready(function () {
    var channel = Personetics.utils.url.getParameterByName("channel");
    if (channel === 'web') {
        homeApp=new homeWeb();
        homeApp.onDocumentReady();
    }else{
        homeApp=new home();
    }
});
