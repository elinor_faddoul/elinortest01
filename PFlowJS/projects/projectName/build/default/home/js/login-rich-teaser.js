//$("#pserverUrl").val(); not in use for web

var loginPage = function loginPage(){
    this.initialize = function initialize(){
        this.url = "../personetics/execute";
        this.insightsTemplate = "<div class='perso-teaser' data-id='{{id}}'>{{text}}</div>";
        this.config = {
            lang: "en",
            deviceType: "web",
            ctxId: "dashboard",
            widgetType: "master-teaser",
        };
    };
    this.initialize();

    this.onDocumentReady = function onDocumentReady(){
        this.bindEvents();
    };

    this.bindEvents = function bindEvents(){
        var me = this;
        $("#initPersoneticsButton").click(function () {
            me.userId = $("#userId").val();
            me.sendGetInsights("getInsights");
        });

        $("#inboxPersoneticsButton").click(function () {
            me.userId = $("#userId").val();
            me.sendGetInsights("getInboxInsights");
        });
    };

    this.sendGetInsights = function(type){
        var isPredefined = $('#isUsePredefinedCheckbox').attr('checked');
        if(isPredefined && window.testData) {
            this.config = $.extend(this.config, {insights : window.testData.insights.insights, userId:this.userId, ok: true});
            this.onGetInsightsSuccess(this.config);
        }
        else{
            var headers = this.getHeaders();
            var postParams = this.postParams(type);
            this.doAjaxCall(headers, postParams, Personetics.bind(this, this.onGetInsightsSuccess), Personetics.bind(this, this.onFailure));
        }
    };

    this.getHeaders = function getHeaders(){
        var headers = {
            "authToken": this.userId,
            "Content-Type": "application/json; charset=UTF-8"
        };
        return headers;
    };

    this.postParams = function postParams(type){
        var postParams = JSON.stringify({
            "type": type || "getInsights",
            "protocolVersion": "2.5",
            "userId": this.userId,
            "ctxId": "dashboard",
            "lang": "en",
            "autoGenerate": true
        });
        return postParams;
    };

    this.doAjaxCall = function(headers, postParams, successCallback, errorCallback){
        $.ajax({
            url: this.url,
            type: "POST",
            dataType: "json",
            headers: headers,
            data: postParams,
            success: successCallback,
            error: errorCallback
        });
    };

    this.onFailure = function onFailure(){
        alert("Error");
    };

    this.onGetInsightsSuccess = function onGetInsightsSuccess(response){
        var me = this;
        if(response.ok === true){
            personetics.startWidget(this.config);
        }
        else{
            //TODO fail??
        }
        $('#insightsContainer').bind('click', '.perso-rich-teaser-container', function (e) {
            var clickedItem = $(e.target).parents('.perso-insights-container');
            /**
             * Remove comment to make click work*/
            // me.onTeaserClick(clickedItem);
        });
    };

    this.onTeaserClick = function onTeaserClick(itemClicked){
        var id = $(itemClicked).attr("data-id");
        var userId = this.userId;

        if(userId && userId.length > 0 && id && id.length > 0)
            window.location = "home.html?userId=" + userId + "&insightId=" + id + "&pserverUrl=" + this.url;
        else
            alert("Please fill out all inputs");
    };
};

var myLogin = new loginPage();
$(document).ready(function(){
    myLogin.onDocumentReady();
});
