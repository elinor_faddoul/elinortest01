var EventDelegate = function(){

    this.sessionEnded = function(config){
        Personetics.utils.PLogger.debug("Customer: sessionEnded");
        var channel = personetics.getDeviceType();
        if (channel === 'web'){
            if(config.ExistInbox){
                window.location = "../home/login.html";
            }else {
                config.el = $("#perso-widget-container");
                config.widgetType = Personetics.utils.url.getParameterByName("teasersView") == 'Carousel' ?
                    "teaser-widget-carousel" : "teaser-widget";
                personetics.startWidget(config);
            }
        }
        else{
            var data = {
                requestString: "sessionEnded",
                widgetType:config.widgetType
            };

            this.sendRequestToPServer(null,data);
        }
    };

    this.registered = function() {
        Personetics.utils.PLogger.debug("Customer: Registration completed");
    };

    this.sessionStarted = function(){
        Personetics.utils.PLogger.debug("Customer : sessionStarted");
    };

    /**
     * Send data to native interface
     * @param requestId
     * @param data
     */
    this.sendRequestToPServer = function(requestHeaders,data, requestId){

        Personetics.utils.PLogger.debug("Personetics Events Delegate requestDataFromNative: " + JSON.stringify(data) + ", requestId: " + requestId);

        var requestString = data.hasOwnProperty("requestString") ? data.requestString : "sendRequestToPServer";
        var json = {requestData: data, requestId: requestId};

        var channel = personetics.getDeviceType();
        if (channel === 'ios') {
            this.callIphoneBridge(requestHeaders,json, requestString, requestId);
			/* Added for backward support to UIWebView */
            this.callIUIWebViewPhoneBridge(requestHeaders,json, requestString, requestId);
        }
        else if (channel === 'android') {
            this.callAndroidBridge(requestHeaders,json, requestString, requestId);
        }
        else{
            if (channel === 'web'){
                this.sendAjaxRequest(requestHeaders,data,requestId);
            }
            else
                throw "invalid deviceType '" + channel + "'";
        }

    };
    this.sendAjaxRequest=function(requestHeaders,json, requestId){
        Personetics.utils.network.doAjaxPost("../../personetics/execute",JSON.stringify(json), {
            dataType: "json",
            headers: requestHeaders,
            onSuccess: function (response) {
                personetics.handlePServerResponse(response, requestId);
            },
            onFailure: function (XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    }
    /**
     * Receive data from native
     * @param requestId
     * @param data
     */
    this.handlePServerResponse = function(data, requestId){
        var decodedData = Personetics.utils.encodeDecode.Base64.decode(data);
        try {
            decodedData = $.parseJSON(decodedData);
        } catch (error) {
            decodedData = {
                ok: false,
                status: '600',
                statusMessage: "A network error occurred - check your network and try again."
            };
        }

        personetics.handlePServerResponse(decodedData, requestId);
    };


    this.callIphoneBridge = function (requestHeaders, json, requestString, reqId) {

        if (typeof window.webkit == "undefined" || typeof window.webkit.messageHandlers == "undefined" || typeof window.webkit.messageHandlers.personeticsApple == "undefined")
            return;

        var iosBridge = window.webkit.messageHandlers.personeticsApple;
        if(typeof iosBridge !== 'undefined' && iosBridge !== null) {
            var requestBody = json; //encodeURIComponent(JSON.stringify(json);

            var finalJson = {
                "sender" : "personetics",
                "requestType" : requestString,
                "requestInformation" : requestBody
            };

            var finalMessage = JSON.stringify(finalJson);
            window.webkit.messageHandlers.personeticsApple.postMessage(finalMessage);
        }
    }

    this.callIUIWebViewPhoneBridge = function (requestHeaders,json, requestString, reqId) {
        var queryString = "personetics://" + requestString + "/" + encodeURIComponent(JSON.stringify(json));
        var iframe = document.createElement("iframe");
        iframe.setAttribute("src", queryString);
        document.documentElement.appendChild(iframe);
        iframe.parentNode.removeChild(iframe);
        iframe = null;
    };

    this.callAndroidBridge = function (requestHeaders,json, requestString, reqId) {
        if(typeof PersoneticsAndroid !== 'undefined' && PersoneticsAndroid !== null) {
            var jsonString = JSON.stringify(json.requestData);
            switch (requestString) {
                case 'sessionEnded' :
                    PersoneticsAndroid.sessionEnded(jsonString, reqId);
                    break;
                case 'sessionError' :
                    PersoneticsAndroid.sessionError(jsonString, reqId);
                    break;
                case 'navigateToPage' :
                    PersoneticsAndroid.navigateToPage(jsonString);
                    break;
                case 'sendRequestToPServer' :
                    PersoneticsAndroid.sendRequestToPServer(jsonString, reqId);
                    break;
                case 'personeticsEvent' :
                    PersoneticsAndroid.personeticsEvent(jsonString);
                    break;
            }
        }

    };

    this.navigateToPage = function(params){
        Personetics.utils.PLogger.debug("Customer : navigateToPage, Params : " + JSON.stringify(params));

        var data = {
            requestString: "navigateToPage",
            data: params
        };

        this.sendRequestToPServer(null,data);
    };

    this.sessionError = function(error){
        var data = {
            requestString: "sessionError",
            data: error
        };
        var dataError = error;
        try {
            JSON.parse(error);
        } catch (e) {
            if(dataError === null || typeof dataError !== 'object')
                dataError = {status: '600', statusMessage: "A network error occurred - check your network and try again."};
            dataError = JSON.stringify(dataError);
        }
        Personetics.utils.PLogger.debug("Customer : sessionError, Params: " + dataError);
        data.data = dataError;
        this.sendRequestToPServer(null,data);
    };

    this.widgetEvent = function widgetEvent(widget, params) {
        if (params.actionType) {
            if (params.actionType == "externalNavigation") {
                this.navigateToPage(params);
            } else if (params.actionType == "externalNavigationUrl") {
                window.location.href = params.payload.navigateTarget;
            } else{
                this.handleWidgetAction(widget, params);
            }
        }
        var eventType = params.eventType;
        switch (eventType) {
            case "teaserClick":
                var channel = personetics.getDeviceType();
                if (channel === 'ios' || channel === 'android') {
                    Personetics.utils.PLogger.debug("Customer: teaserClick");
                    var data = {
                        requestString: "personeticsEvent",
                        data: params
                    };
                    this.sendRequestToPServer(null,data);
                }
                else {
                    var startWidgetConfig = params;
                    startWidgetConfig.widgetType = "story-widget";
                    startWidgetConfig.el = $("#perso-widget-container");
                    personetics.startWidget(startWidgetConfig);
                    break;
                }
            default: console.log('widget event' + JSON.stringify(params));
        }
    };

    this.handleWidgetAction = function handleWidgetActions(widget, params){
        var channel = personetics.getDeviceType();
        if (channel === 'ios' || channel === 'android') {
            Personetics.utils.PLogger.debug("Customer: openStory");
            var data = {
                requestString: "personeticsEvent",
                data: params
            };
            this.sendRequestToPServer(null,data);
        }
        else {
            switch(params.actionType){
                case "openStory":
                    var startWidgetConfig = params.payload;
                    startWidgetConfig.widgetType = "story-widget";
                    startWidgetConfig.el = $("body");
                    personetics.startWidget(startWidgetConfig);
                    break;
                default: console.log('widget event' + JSON.stringify(params.payload));
            }
        }
    }

};
