(function (window, Personetics, $, undefined) {
	Personetics.story = Personetics.story || {};
	Personetics.story.ProjectNameSkinControler = Personetics.story.ProductSkinControler.extend({
        registerBlocks: function () {
            Personetics.story.ProductSkinControler.prototype.registerBlocks.call(this);
            this.registerBlock("account-selector", {
                blockClass: Personetics.story.NewAccountSelectorBlockProjectName
            });
        },
    });
    
    // register this skin controller as application skin controller
    Personetics.story.skinController = Personetics.story.ProjectNameSkinControler;
})(window, window.Personetics = window.Personetics || {}, jQuery);