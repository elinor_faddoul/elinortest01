(function(window, Personetics, $, Handlebars, undefined){

this["Personetics"] = this["Personetics"] || {};
this["Personetics"]["UI"] = this["Personetics"]["UI"] || {};
this["Personetics"]["UI"]["Handlebars"] = this["Personetics"]["UI"]["Handlebars"] || {};
this["Personetics"]["UI"]["Handlebars"]["templates"] = this["Personetics"]["UI"]["Handlebars"]["templates"] || {};

this["Personetics"]["UI"]["Handlebars"]["templates"]["error-msg"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "class=\"perso-mobile-err\"";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\"personetics-error-container\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isMobileDevice : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n    <div class=\"personetics-error-img "
    + alias4(((helper = (helper = helpers.imgClass || (depth0 != null ? depth0.imgClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"imgClass","hash":{},"data":data}) : helper)))
    + "\"></div>\n    <div class=\"personetics-error-larg\">"
    + alias4(((helper = (helper = helpers.textLarge || (depth0 != null ? depth0.textLarge : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"textLarge","hash":{},"data":data}) : helper)))
    + "</div>\n    <div class=\"personetics-error-small\">"
    + alias4(((helper = (helper = helpers.textSmall || (depth0 != null ? depth0.textSmall : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"textSmall","hash":{},"data":data}) : helper)))
    + "</div>\n    <div class=\"personetics-no-insights-btn\"><span class=\"personetics-btn-text\">"
    + alias4(((helper = (helper = helpers.btnText || (depth0 != null ? depth0.btnText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"btnText","hash":{},"data":data}) : helper)))
    + "</span></div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["flow-selector-tmpl"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <option value=\""
    + alias4(((helper = (helper = helpers.key || (data && data.key)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"key","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<select id=\"flow-selector\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</select>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["no-filtered-insights-msg"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "class=\"perso-mobile-msg\"";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div id=\"personetics-no-insights-filter-container\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isMobileDevice : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n    <div class=\"personetics-nothing-toshow-msg\">"
    + container.escapeExpression(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + "</div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["no-insights-message"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "class=\"perso-mobile-err\"";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\"personetics-no-insights-container\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isMobileDevice : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n    <div class=\"personetics-no-insights-img "
    + alias4(((helper = (helper = helpers.imgClass || (depth0 != null ? depth0.imgClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"imgClass","hash":{},"data":data}) : helper)))
    + "\"></div>\n    <div class=\"personetics-no-insights-small\">"
    + alias4(((helper = (helper = helpers.textSmall || (depth0 != null ? depth0.textSmall : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"textSmall","hash":{},"data":data}) : helper)))
    + "</div>\n    <div class=\"personetics-no-insights-larg\">"
    + alias4(((helper = (helper = helpers.textLarge || (depth0 != null ? depth0.textLarge : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"textLarge","hash":{},"data":data}) : helper)))
    + "</div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-account-selector"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "			<div class=\"perso-header-container\">\n				<input type=\"button\" value=\"All accounts\" class=\"pstory-header-button perso-selected\" data-all=\"true\" pstory-id=\"all\" />\n				<input type=\"button\" value=\"Choose account\" class=\"pstory-header-button\" data-all=\"false\" />\n\n				<div class=\"pstory-header-button perso-on-select perso-no-select\" data-all=\"false\" pstory-id=\"all\"></div>\n				<!--pstory-id for first time-->\n				<div class=\"pstory-header-button perso-on-select perso-select\" data-all=\"true\"></div>\n			</div>\n			<div class=\"perso-options-container perso-show-all perso-all-accounts\">\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "				<div class=\"perso-options-container\">\n";
},"5":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "						<div class=\"pstory-button "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" pstory-event=\""
    + alias4(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"event","hash":{},"data":data}) : helper)))
    + "\" role=\""
    + alias4(alias5((depths[1] != null ? depths[1].role : depths[1]), depth0))
    + "\">\n							<div class=\"perso-card perso-card_"
    + alias4(((helper = (helper = helpers.typeImage || (depth0 != null ? depth0.typeImage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"typeImage","hash":{},"data":data}) : helper)))
    + " perso-cards"
    + alias4(((helper = (helper = helpers.numCards || (depth0 != null ? depth0.numCards : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numCards","hash":{},"data":data}) : helper)))
    + "\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n								<div class=\"perso-card-img\"></div>\n								<span class=\"perso-account-name\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</span>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.number : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "							</div>\n							<div id=\"pop-"
    + alias4(alias5(depth0, depth0))
    + "\" class=\"pstory-button-txt-wrapper\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.text2 : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "								<span class=\"pstory-button-txt\">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n							</div>\n						</div>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "perso-selected";
},"8":function(container,depth0,helpers,partials,data) {
    var helper;

  return "								<span class=\"perso-account-number\">Account No. "
    + container.escapeExpression(((helper = (helper = helpers.number || (depth0 != null ? depth0.number : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"number","hash":{},"data":data}) : helper)))
    + "</span>//TODO: dictionary\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "								<span class=\"pstory-button-txt perso-bold\">"
    + ((stack1 = ((helper = (helper = helpers.text2 || (depth0 != null ? depth0.text2 : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"text2","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "						<span class=\"perso-bold\">"
    + ((stack1 = ((helper = (helper = helpers["perso-all-accounts-txt2"] || (depth0 != null ? depth0["perso-all-accounts-txt2"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"perso-all-accounts-txt2","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\"pstory-block-account-selector\" class=\"pstory-block-frame pstory-block-account-selector "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" data-show-header-tabs="
    + alias4(((helper = (helper = helpers.showHeaderTabs || (depth0 != null ? depth0.showHeaderTabs : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"showHeaderTabs","hash":{},"data":data}) : helper)))
    + ">\n	<div class=\"pstory-block-container\">\n		<div class=\"pstory-block-content\" tabindex=\"0\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showHeaderTabs : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(3, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "					<div class=\"perso-cards-wrapper\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "					</div>\n					<div class=\"pstory-all-accounts-txt\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.allAccountsTxt2 : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "						<span class=\"perso-amount-wrapper\">"
    + ((stack1 = ((helper = (helper = helpers.allAccountsTxt || (depth0 != null ? depth0.allAccountsTxt : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"allAccountsTxt","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n					</div>\n				</div>\n				<div class=\"pstory-comment\">"
    + ((stack1 = ((helper = (helper = helpers.comment || (depth0 != null ? depth0.comment : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"comment","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n			</div>\n		</div>\n	</div>";
},"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-bar-chart-collapse"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-block-frame pstory-block-bar-chart-collapse pstory-inner-"
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-contanier\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-button-collapse\">\n            	<div class=\"pstory-inner-title\">\n            		"
    + alias4(((helper = (helper = helpers.txt || (depth0 != null ? depth0.txt : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"txt","hash":{},"data":data}) : helper)))
    + "\n        		</div>\n        		<div class=\"pstory-button-collapse-icon\"></div>\n        	</div>\n            <div class=\"pstory-bar-chart-collapse-content\">\n           		<div class=\"perso-image\"></div>\n       		</div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-bar-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-block-frame pstory-block-bar-chart pstory-block-bar-chart "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " perso-"
    + alias4(((helper = (helper = helpers.direction || (depth0 != null ? depth0.direction : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"direction","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-bar-chart\"></div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-buttons-new"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.navigateTarget : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.program(4, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <a href=\"#!\"  role=\"text\" aria-label=\""
    + alias4(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + ", link\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"pstory-link-navigateTo perso-button-animation pstory-button perso-ripple-closed\" action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" pnavigate-id=\""
    + alias4(((helper = (helper = helpers.navigateTarget || (depth0 != null ? depth0.navigateTarget : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"navigateTarget","hash":{},"data":data}) : helper)))
    + "\" >"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\n";
},"4":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.toggleState : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.program(8, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                        <input type=\"button\" role=\"text\"  aria-label=\""
    + alias4(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + ", button\" class=\"pstory-button pstory-toggle-button "
    + alias4(((helper = (helper = helpers.toggleState || (depth0 != null ? depth0.toggleState : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"toggleState","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" value=\""
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" pstory-type-text=\"Toggle "
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" pstory-id=\""
    + alias4(((helper = (helper = helpers.toggleState || (depth0 != null ? depth0.toggleState : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"toggleState","hash":{},"data":data}) : helper)))
    + "\"/>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "perso-selected";
},"8":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[1] != null ? depths[1].typeRadio : depths[1]),{"name":"if","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.program(12, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                            <div class=\"pstory-radio-button-wrapper\">\n                                <div class=\"pstory-button perso-button-animation pstory-radio-button "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  perso-ripple-closed\" tabindex=\"-1\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" pstory-event=\""
    + alias4(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"event","hash":{},"data":data}) : helper)))
    + "\">\n                                    <input id=\"pstory-radio-"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" name=\"radio-"
    + alias4(container.lambda((depths[1] != null ? depths[1].id : depths[1]), depth0))
    + "\" type=\"radio\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "/>\n                                    <label for=\"pstory-radio-"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" role=\"text\">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\n                                </div>\n                            </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "checked";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression, alias2=depth0 != null ? depth0 : (container.nullContext || {}), alias3=helpers.helperMissing, alias4="function";

  return "                            <button id=\"pop-"
    + alias1(container.lambda(depth0, depth0))
    + "\" role=\"text\" aria-label=\""
    + alias1(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"text","hash":{},"data":data}) : helper)))
    + ", button\"  class=\"pstory-button pstory-regular-button "
    + ((stack1 = helpers["if"].call(alias2,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " perso-ripple-closed\" action-id=\""
    + alias1(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" pstory-type-text=\"Button "
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" pstory-id=\""
    + alias1(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" pstory-event=\""
    + alias1(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"event","hash":{},"data":data}) : helper)))
    + "\">\n                                "
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n                            </button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\"pstory-block-buttons_"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"pstory-block-frame pstory-block-buttons "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " pstory-block-buttons-"
    + alias4(((helper = (helper = helpers.buttonType || (depth0 != null ? depth0.buttonType : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonType","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n    </div>\n</div>";
},"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-buttons"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.navigateTarget : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.program(4, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <a href=\"#!\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"pstory-link-navigateTo pstory-button perso-ripple-closed\" action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" pnavigate-id=\""
    + alias4(((helper = (helper = helpers.navigateTarget || (depth0 != null ? depth0.navigateTarget : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"navigateTarget","hash":{},"data":data}) : helper)))
    + "\" >"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\n";
},"4":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.toggleState : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.program(8, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                        <input type=\"button\" class=\"pstory-button pstory-toggle-button "
    + alias4(((helper = (helper = helpers.toggleState || (depth0 != null ? depth0.toggleState : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"toggleState","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" pstory-type-text=\"Toggle "
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" pstory-id=\""
    + alias4(((helper = (helper = helpers.toggleState || (depth0 != null ? depth0.toggleState : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"toggleState","hash":{},"data":data}) : helper)))
    + "\"/>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "perso-selected";
},"8":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[1] != null ? depths[1].typeRadio : depths[1]),{"name":"if","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.program(12, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                            <div class=\"pstory-button pstory-radio-button "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  perso-ripple-closed\"  tabindex=\"0\" action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"  pstory-event=\""
    + alias4(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"event","hash":{},"data":data}) : helper)))
    + "\">\n                                <input id=\"pstory-radio-"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" name=\"radio-"
    + alias4(container.lambda((depths[1] != null ? depths[1].id : depths[1]), depth0))
    + "\" type=\"radio\" action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "/>\n                                <label for=\"pstory-radio-"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" role=\"text\">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\n                            </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "checked";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression, alias2=depth0 != null ? depth0 : (container.nullContext || {}), alias3=helpers.helperMissing, alias4="function";

  return "                            <button id=\"pop-"
    + alias1(container.lambda(depth0, depth0))
    + "\" class=\"pstory-button pstory-regular-button "
    + ((stack1 = helpers["if"].call(alias2,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " perso-ripple-closed\" action-id=\""
    + alias1(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" pstory-type-text=\"Button "
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" pstory-id=\""
    + alias1(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" pstory-event=\""
    + alias1(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"event","hash":{},"data":data}) : helper)))
    + "\">\n                                "
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n                            </button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\"pstory-block-buttons_"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"pstory-block-frame pstory-block-buttons "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n    </div>\n</div>";
},"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-compare-block"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div class=\"perso-compare\" role=\"text\">\n                            <div class=\"perso-compare-text perso-label\">"
    + ((stack1 = ((helper = (helper = helpers.label1 || (depth0 != null ? depth0.label1 : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label1","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n                            <div class=\"perso-compare-text perso-value\">"
    + ((stack1 = ((helper = (helper = helpers.label2 || (depth0 != null ? depth0.label2 : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label2","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n                        </div>\n                <div id=\""
    + ((stack1 = ((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "_wrapper\" class=\"perso-compare-bar-wrapper\">\n                    <div id=\""
    + ((stack1 = ((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" class=\"perso-compare-bar\"></div>\n                </div>\n\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"pstory-block-frame pstory-block-compareBlock "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-contanier\">\n        <div class=\"pstory-block-content\">\n            <div class=\"perso-compareBlock-wrapper\" role=\"text\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-date-box"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-block-frame pstory-block-date-box "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-contanier\">\n        <div class=\"pstory-block-content\" tabindex=\"0\">\n            <div class=\"pstory-block-date-month\">"
    + alias4(((helper = (helper = helpers.month || (depth0 != null ? depth0.month : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"month","hash":{},"data":data}) : helper)))
    + "</div>\n            <div class=\"pstory-block-date-day\">"
    + alias4(((helper = (helper = helpers.day || (depth0 != null ? depth0.day : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"day","hash":{},"data":data}) : helper)))
    + "</div>\n        </div>\n    </div>\n</div>\n\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-entity-selector"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                        <div class=\"pstory-button perso-carousel-item pstory-entity-selector-item "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " pstory-entity-"
    + alias4(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data}) : helper)))
    + "\" action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\"\n                            pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                            <div class=\"pstory-entity-icon "
    + alias4(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data}) : helper)))
    + "\">\n                            </div>\n                        </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "perso-selected";
},"4":function(container,depth0,helpers,partials,data) {
    return "                    <div class=\"perso-carousel-index-dot\">\n                        <div class=\"perso-carousel-index-dot-circle\"></div>\n                    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"pstory-block-frame pstory-block-entity-selector "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-contanier\">\n        <div class=\"pstory-block-content\">\n            <div class=\"perso-options-container perso-carousel-animate perso-carousel-wrapper\">\n                <div class=\"perso-carousel-outer-wrapper\">\n                    <div class=\"perso-carousel-inner-wrapper\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </div>\n                </div>\n                <div class=\"pstory-entity-selector-text\"></div>\n                <div class=\"perso-carousel-index-wrapper\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n                <div class=\"pstory-acc-carousel-selected\"></div>\n                <input type=\"button\" class=\"pstory-carousel-arrow pstory-carousel-arrow-prev\" aria-label=\"prev\" />\n                <input type=\"button\" class=\"pstory-carousel-arrow pstory-carousel-arrow-next\" aria-label=\"next\" />\n            </div>\n\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-entity-sub-transactions"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.icon : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.program(9, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div class=\"perso-textbox perso-textbox-with-image "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Link : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + " \" role=\"text\">\n                            <div class=\"perso-textbox-image "
    + ((stack1 = ((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\"></div>\n                            <div class=\"perso-value\">"
    + ((stack1 = ((helper = (helper = helpers.device || (depth0 != null ? depth0.device : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"device","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n                            <div class=\"perso-last-payment\">"
    + ((stack1 = container.lambda(((stack1 = (depths[1] != null ? depths[1].texts : depths[1])) != null ? stack1.lastModifiedlbl : stack1), depth0)) != null ? stack1 : "")
    + "</div>\n                            <div class=\"perso-label\">"
    + ((stack1 = ((helper = (helper = helpers.amountLabel || (depth0 != null ? depth0.amountLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"amountLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Link : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "perso-with-button";
},"5":function(container,depth0,helpers,partials,data) {
    return "perso-without-button";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                                <div class=\"perso-button-wrapper\">\n                                    <a id=\""
    + ((stack1 = ((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" class=\"pstory-link-navigateTo pstory-button perso-ripple-closed\"\n                                       action-id=\""
    + ((stack1 = ((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\n                                </div>\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div class=\"perso-textbox perso-textbox-without-image "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Link : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "\" role=\"text\">\n                            <div class=\"perso-value\">"
    + ((stack1 = ((helper = (helper = helpers.device || (depth0 != null ? depth0.device : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"device","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n                            <div class=\"perso-label\">"
    + ((stack1 = ((helper = (helper = helpers.amountLabel || (depth0 != null ? depth0.amountLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"amountLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Link : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"pstory-block-frame pstory-block-entity-sub-transactions\">\n    <div class=\"pstory-block-contanier\">\n        <div class=\"pstory-block-content\">\n            <div class=\"perso-textboxes-wrapper "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" role=\"text\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>";
},"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-hbox-columns"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <div class=\"pstory-hbox-column-frame "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" data-v-align=\""
    + alias4(((helper = (helper = helpers["v-align"] || (depth0 != null ? depth0["v-align"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"v-align","hash":{},"data":data}) : helper)))
    + "\" data-width=\""
    + alias4(((helper = (helper = helpers.width || (depth0 != null ? depth0.width : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"width","hash":{},"data":data}) : helper)))
    + "\" >\n            <div class=\"pstory-hbox-blocks-container\"></div>\n        </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"pstory-block-hbox-columns-frame "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.columns : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-hbox"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"pstory-block-frame pstory-block-hbox\">\n    <div class=\"pstory-block-contanier\">\n        <div class=\"pstory-block-content\">\n            <div class=\"perso-hbox-block-content\"></div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-image"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"pstory-block-content  perso-absolute-url\" tabindex=\"-1\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"background-image:url('"
    + alias4(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data}) : helper)))
    + "')\"></div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"pstory-block-content perso-image perso-"
    + alias4(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data}) : helper)))
    + "\" tabindex=\"-1\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"pstory-block-frame pstory-block-image "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-contanier\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isAbsoluteURL : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-layout"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"pstory-block-frame pstory-block-layout\">\n    <div class=\"pstory-block-contanier\">\n        <div class=\"pstory-block-content\">\n            <div class=\"perso-layout-block-content\" id=\""
    + container.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data}) : helper)))
    + "\"></div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-line-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"pstory-block-frame pstory-block-line-chart pstory-block-line-chart "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <div class=\"pstory-line-chart\"></div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-map"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"pstory-block-frame pstory-block-map\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-map-frame\"></div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-month-picker"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <li class=\"perso-month-picker-item\" pstory-category=\""
    + alias4(((helper = (helper = helpers.category || (depth0 != null ? depth0.category : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"category","hash":{},"data":data}) : helper)))
    + "\" role=\"text\" aria-label=\""
    + alias4(((helper = (helper = helpers.accLabel || (depth0 != null ? depth0.accLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accLabel","hash":{},"data":data}) : helper)))
    + "\" tabindex=\"-1\">"
    + alias4(((helper = (helper = helpers.displayLabel || (depth0 != null ? depth0.displayLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"displayLabel","hash":{},"data":data}) : helper)))
    + "</li>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"perso-month-picker-outer-wrapper\">\n    <div class=\"perso-month-picker-wrapper\">\n\n        <ul class=\"perso-month-picker-container\" role=\"tablist\">\n            <div class=\"pstory-month-picker-selected-tab\"></div>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.series : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </ul>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-pie-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"pstory-block-frame pstory-block-pie-chart\" >\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"0\">\n            <div class=\"pstory-pie-chart\"></div>\n            <div class=\"pstory-pie-data\">\n                <div class=\"pstory-next-arrow\"></div>\n                <div class=\"pstory-pie-info\">\n                </div>\n                <div class=\"pstory-previous-arrow\"></div>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-pinGraph-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-block-frame pstory-block-pinGraph-chart_"
    + alias4(((helper = (helper = helpers.index || (depth0 != null ? depth0.index : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + " pstory-block-pinGraph-chart "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-pinGraph-chart\">\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-pure-bar-chart"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.indicatorLegend : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                        <div class=\"perso-legend-wrapper\">\n                            <div class=\"perso-bar-chart-legend\"></div>\n                            <div class=\"perso-bar-chart-legend-txt\">"
    + ((stack1 = ((helper = (helper = helpers.indicatorLegend || (depth0 != null ? depth0.indicatorLegend : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"indicatorLegend","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n                        </div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "perso-comparable-chart";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.series : depth0),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                                <div class=\"perso-series-wrapper\">\n                                    <span class=\"perso-tooltip\"></span>\n                                    <div class=\"perso-series\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                                    </div>\n                                </div>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "                                            <div class=\"perso-bar-wrapper\">\n                                                <div class=\"perso-bar"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\"\n                                                     pstory-category=\""
    + container.escapeExpression(((helper = (helper = helpers.category || (depth0 != null ? depth0.category : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"category","hash":{},"data":data}) : helper)))
    + "\">\n                                                </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.indicator : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                                            </div>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return " perso-selected";
},"11":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                                                    <div class=\"perso-bar perso-indicator\"\n                                                         pstory-category=\""
    + container.escapeExpression(((helper = (helper = helpers.category || (depth0 != null ? depth0.category : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"category","hash":{},"data":data}) : helper)))
    + "\">\n                                                    </div>\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.series : depth0),{"name":"each","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "                                <div class=\"perso-bar-wrapper perso-simple-bar\">\n                                <div class=\"perso-bar "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.program(17, data, 0),"data":data})) != null ? stack1 : "")
    + " pstory-category=\""
    + container.escapeExpression(((helper = (helper = helpers.category || (depth0 != null ? depth0.category : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"category","hash":{},"data":data}) : helper)))
    + "\">\n                                </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.indicator : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                            </div>\n";
},"15":function(container,depth0,helpers,partials,data) {
    return "perso-selected\" aria-selected=\"true\"";
},"17":function(container,depth0,helpers,partials,data) {
    return "\n                                    \"";
},"19":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                                    <div class=\"perso-bar perso-indicator\" pstory-category=\""
    + container.escapeExpression(((helper = (helper = helpers.category || (depth0 != null ? depth0.category : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"category","hash":{},"data":data}) : helper)))
    + "\">\n                                    </div>\n";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.indicatorLegend : depth0),{"name":"if","hash":{},"fn":container.program(22, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"22":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                            <div class=\"perso-legend-wrapper perso-series-legend\">\n                                <div class=\"perso-bar-chart-legend\"></div>\n                                <div class=\"perso-bar-chart-legend-txt\">"
    + ((stack1 = ((helper = (helper = helpers.indicatorLegend || (depth0 != null ? depth0.indicatorLegend : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"indicatorLegend","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n                            </div>\n";
},"24":function(container,depth0,helpers,partials,data) {
    return "                <div class=\"perso-month-amount is-series\"></div>\n";
},"26":function(container,depth0,helpers,partials,data) {
    return "                <div class=\"perso-month-amount\"></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-block-frame pstory-block-bar-chart perso-pure-bar-chart "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " perso-"
    + alias4(((helper = (helper = helpers.direction || (depth0 != null ? depth0.direction : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"direction","hash":{},"data":data}) : helper)))
    + "\" >\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"0\">\n            <div class=\"pstory-bar-chart\">\n"
    + ((stack1 = container.invokePartial(partials.monthPicker,depth0,{"name":"monthPicker","data":data,"indent":"                ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.isSeries : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                <div class=\"perso-bar-chart-wrapper perso-accessibility-wrapper "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isSeries : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" role=\"text\" tabindex=\"-1\">\n                    <span class=\"perso-accessibility-read perso-accessibility-bar-seleced-text\"></span>\n                    <div class=\"perso-bar-chart-container\">\n                    <div class=\"perso-bars-wrapper\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isSeries : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(13, data, 0),"data":data})) != null ? stack1 : "")
    + "                    </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isSeries : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n            </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isSeries : depth0),{"name":"if","hash":{},"fn":container.program(24, data, 0),"inverse":container.program(26, data, 0),"data":data})) != null ? stack1 : "")
    + "        </div>\n    </div>\n</div>\n</div>";
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-pure-pinGraph-chart"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "                        <div class=\"perso-pinGraph-raw\" tabindex=\"-1\" >\n                            <div class=\"perso-raw-wrapper\" role=\"text\">\n                                <span class=\"perso-raw-item perso-date-txt\"></span>\n                                <div class=\"perso-raw-item perso-bar-wrapper\">\n                                    <span class=\"perso-pinGraph-bar\"></span>\n                                    <span class=\"perso-raw-item perso-pinGraph-amount\"></span>\n                                </div>\n                            </div>\n                        </div>\n                        <div aria-hidden=\"true\"></div><!--it's required for accessibility-->\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-block-frame pstory-block-pinGraph-chart_"
    + alias4(((helper = (helper = helpers.index || (depth0 != null ? depth0.index : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + " pstory-block-pinGraph-chart "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <div class=\"pstory-pinGraph-chart\">\n                <div class=\"perso-pinGraph-container\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.series : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-selectOne"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	            <div class=\"pstory-selectOne-wrapper\">\n		            <span class=\"pstory-selectOne\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"\n		                   pstory-event=\""
    + alias4(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"event","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.txt || (depth0 != null ? depth0.txt : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"txt","hash":{},"data":data}) : helper)))
    + "</span>\n                    <input type=\"button\" class=\"pstory-button pstory-selectOne-icon\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"\n		                   pstory-event=\""
    + alias4(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"event","hash":{},"data":data}) : helper)))
    + "\"/>\n	            </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-selectOne\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-spline-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"pstory-block-frame pstory-block-spline-chart\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-spline-chart\"></div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                    <tr>\n                        <td class=\"perso-loading-table\">"
    + container.escapeExpression(((helper = (helper = helpers.loadingText || (depth0 != null ? depth0.loadingText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"loadingText","hash":{},"data":data}) : helper)))
    + "</td>\n                    </tr>\n";
},"3":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.twoCols : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0, blockParams, depths),"inverse":container.program(10, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showHeaders : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                            <tr>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.displayedCols : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                            </tr>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                                    <th role=\"cell\">"
    + ((stack1 = container.lambda(depth0, depth0)) != null ? stack1 : "")
    + "</th>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "                            <tr>\n                                <td class=\"perso-td-first\" role=\"cell\">"
    + ((stack1 = alias1((depth0 != null ? depth0["0"] : depth0), depth0)) != null ? stack1 : "")
    + "</td>\n                                <td align=\"right\">\n                                    <span class=\"perso-td-second\" role=\"cell\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0["1"] : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                </td>\n                            </tr>\n";
},"10":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showHeaders : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"11":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                            <tr>\n                                <th role=\"cell\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.displayedCols : depth0)) != null ? stack1["1"] : stack1), depth0))
    + "</th>                           \n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.fourCols : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.program(14, data, 0),"data":data})) != null ? stack1 : "")
    + "                            </tr>\n";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                                <th align=\"right\" role=\"cell\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.displayedCols : depth0)) != null ? stack1["3"] : stack1), depth0))
    + "</th>\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                                    <th align=\"right\" role=\"cell\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.displayedCols : depth0)) != null ? stack1["2"] : stack1), depth0))
    + "</th>\n";
},"16":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=container.lambda, alias2=depth0 != null ? depth0 : (container.nullContext || {});

  return "                            <tr>\n                                <td>\n                                    <span class=\"perso-td-date\" role=\"cell\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0["0"] : depth0), depth0)) != null ? stack1 : "")
    + "\n                                        </span>\n                                    <span class=\"perso-td-first\" role=\"cell\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0["1"] : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                </td>\n                                <td align=\"right\">\n                                    <span class=\"perso-td-second\" role=\"cell\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0["2"] : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n"
    + ((stack1 = helpers["if"].call(alias2,(depths[1] != null ? depths[1].fourCols : depths[1]),{"name":"if","hash":{},"fn":container.program(17, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                                </td>\n"
    + ((stack1 = helpers["if"].call(alias2,(depths[1] != null ? depths[1].fourCols : depths[1]),{"name":"if","hash":{},"fn":container.program(19, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                            </tr>\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                                        <span class=\"perso-td-last\" role=\"cell\">\n                                            "
    + ((stack1 = container.lambda((depth0 != null ? depth0["3"] : depth0), depth0)) != null ? stack1 : "")
    + "\n                                        </span>\n";
},"19":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                                    <!--<td align=\"right\">\n                                        <span class=\"perso-td-last\" role=\"cell\">\n                                            "
    + ((stack1 = container.lambda((depth0 != null ? depth0["3"] : depth0), depth0)) != null ? stack1 : "")
    + "\n                                        </span>\n                                    </td>-->\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <table class=\"pstory-table\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(3, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "            </table>\n        </div>\n    </div>\n</div>";
},"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-tabs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                            <li  class=\"pstory-tab "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isDesktopMode : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + " "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" aria-selected=\""
    + alias4(((helper = (helper = helpers.selected || (depth0 != null ? depth0.selected : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"selected","hash":{},"data":data}) : helper)))
    + "\" role=\"tab\" tabindex=\""
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</li>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "pstory-button";
},"4":function(container,depth0,helpers,partials,data) {
    return "pstory-button pstory-header-button";
},"6":function(container,depth0,helpers,partials,data) {
    return "perso-selected";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\"pstory-block-tabs_"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"pstory-block-frame pstory-block-tabs "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n                <div class=\"perso-header-container\">\n                    <div class=\"pstory-tab-animated-border-bottom\"></div>\n                    <ul class=\"perso-tablist\" role=\"tablist\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </ul>\n                </div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-textboxes"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "perso-with-image";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.image : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div class=\"perso-textbox perso-textbox-with-image\" role=\"text\">\n                            <div class=\"perso-textbox-image "
    + ((stack1 = ((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\"></div>\n                            <div class=\"perso-value\">"
    + ((stack1 = ((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n                            <div class=\"perso-label\">"
    + ((stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.action : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        </div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                                <div class=\"perso-button-wrapper\">\n                                    <a id=\""
    + ((stack1 = ((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" class=\"pstory-link-navigateTo pstory-button perso-ripple-closed\"\n                                       action-id=\""
    + ((stack1 = ((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" pnavigate-id=\""
    + ((stack1 = ((helper = (helper = helpers.navigateTarget || (depth0 != null ? depth0.navigateTarget : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"navigateTarget","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.buttonLabel || (depth0 != null ? depth0.buttonLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\n                                </div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div class=\"perso-textbox\" role=\"text\">\n                            <div class=\"perso-value\">"
    + ((stack1 = ((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n                            <div class=\"perso-label\">"
    + ((stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.action : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"pstory-block-frame pstory-block-textboxes "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" >\n    <div class=\"pstory-block-contanier\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <div class=\"perso-textboxes-wrapper "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.hasImage : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" role=\"presentation\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-txt-note"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "<div class=\"pstory-block-frame pstory-block-text "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-contanier\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <div class=\"pstory-block-note-icon\"></div>\n            <div class=\"pstory-block-note-txt\" role=\"text\">\n                "
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-txt"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"pstory-block-content\" tabindex=\"-1\" aria-level=\"2\" role=\"heading\">\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"pstory-block-content\" tabindex=\"-1\" role=\"text\">\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "<div class=\"pstory-block-frame pstory-block-text "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" >\n    <div class=\"pstory-block-contanier\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.needAccessibility : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "            "
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-frame"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-frame "
    + alias4(((helper = (helper = helpers.langClass || (depth0 != null ? depth0.langClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"langClass","hash":{},"data":data}) : helper)))
    + " perso-"
    + alias4(((helper = (helper = helpers.directionClass || (depth0 != null ? depth0.directionClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"directionClass","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.cssClass || (depth0 != null ? depth0.cssClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cssClass","hash":{},"data":data}) : helper)))
    + "\" data-id=\"pstory-frame_"
    + alias4(((helper = (helper = helpers.storyId || (depth0 != null ? depth0.storyId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"storyId","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-container\"></div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-perso-post"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"perso-post-frame\">\n    <div class=\"perso-post-container\">\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-perso-rating"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"perso-ratings-wrapper\">\n   <div class='perso-ratings-text' role=\"text\" tabindex=\"-1\" aria-label=\""
    + alias4(((helper = (helper = helpers.ratingTitle || (depth0 != null ? depth0.ratingTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ratingTitle","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.ratingTitle || (depth0 != null ? depth0.ratingTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ratingTitle","hash":{},"data":data}) : helper)))
    + "</div>\n   <div class='perso-ratings-icons' data-rating-max='5'></div>\n</div>\n\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-perso-teasers"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return "<div class=\"perso-teaser\" id=\""
    + alias1(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.teaser : depth0)) != null ? stack1.title : stack1), depth0))
    + "</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.posts : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-pfeed"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div>\n    <div class=\"perso-header\">\n        "
    + alias4(((helper = (helper = helpers.feedByText || (depth0 != null ? depth0.feedByText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"feedByText","hash":{},"data":data}) : helper)))
    + " <!--<img src=\"resources/images/perso-logo.png\" class=\"perso-logo\">-->\n        <svg  xmlns:svg=\"http://www.w3.org/2000/svg\" xmlns=\"http://www.w3.org/2000/svg\" width=\"187\" height=\"37\" viewBox=\"0 0 187 37\">\n            <image width=\"187\" height=\"37\" preserveAspectRatio=\"none\" id=\"image10\" x=\"0\" y=\"0\"\n                    xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALsAAAAlCAYAAAAa/JhZAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjRCQjk1MzY1QkU4NjExRTU4OTQzRThGMDA3MzBBMTYxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjRCQjk1MzY2QkU4NjExRTU4OTQzRThGMDA3MzBBMTYxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NEJCOTUzNjNCRTg2MTFFNTg5NDNFOEYwMDczMEExNjEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NEJCOTUzNjRCRTg2MTFFNTg5NDNFOEYwMDczMEExNjEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5K1DUkAAAMWElEQVR42uxcTXLruBGGX7x/yjqVMl05gOVdMrMwdQLLJ7B8AlknkHUCWScQfQLLJxC9mEl2pg+QMl9+1o/ZT8oBlK+tVhsNgpRdzzNPqGJJokigu/Hh60YD5J7ZlV0JlIM//HFpP1JxOv/y73/2PqrMfzpIXuT95Zf/FlbWyn3f33XndwfexH4k4nRlAVH8BnTr2o+5BXi+v/+7O/vp9Bzb8zdWv2wH9u+vDBwAJFPbo/crB3rHAd2CfGRBPraHA/69PS6cvpbtyx3Yv30HuU55cbW7stUgnuF7akHfs4Af2u9TB3j7fb4D+7d1uS4e7uD3wgL+7AOKOiIZedjzAeV0MmYgDxerjxGuuRCmsvZ93A91gGMcKFba4xETk6JBh7qJwikJALeS2TrKNwZOiknUCWS+t20sfi1AR+k7PazceQOdu6KvCs22LFY/8AEF9fli+KoN2KFjl80PKiZj7P0J6qB7yxB2LJt32c8bsDrh4GiPVd53MY9HMVlKVHStCW3rorgw8fzt7hm5CcMbTbbmnmyBM8qhYsCU6ehkWbz14IsIXR4U2zgiuKi599Iew0A/ucEykYPG3nvlidW18pJtUe5z9V8F5DtX9HvRE3WUnjr6AKl2fwH8ZRx/DAvOE02d/PbcJezkrqs+MYVuI4BuIIRT/gmCGY9R5wFhVxMJDIht2fHBA/SVjCSb6wDXlj2ecP0U8o/x/QGd9G7ZD1H/OGCbfoS+45p+cvZY2uun38hbjWuATvH1g8QAwHlbc38X/faENlcFA4fYfITTjtU/O1Z35LqHRp630HNjlNu6vkYOGjfajgNudwDBO/AmZ+RalTCA1+tkusZ1txHGv4j1NKizj3AgQXv3kmlYqHHLdFiA+ULlWIaKYK2HSLtuxNvODu/N7DX9EcSAref3zFbLBvdq3jvBYKLQJv/7l3Ll5fbxpwm4xA6LuX3F5TFdwxkElgo7Y9/B9fYFw4+RGvKVc1ZXAuD3agy7AHArxoS1RoNrjAH6AF7Lx8jODj0B1KXwiJeegdnxMLOUZx7Q9xEDr++5ZoqJ73uHaaHwl+Z9qUd3nu48DehnMB9LxcT5VYGuV96YPjQa+SoZwoKhEjZMEYf5yg0AkCOUSIRLii1pDdDdHIIb4FZh/QJMTBOeIgLkxBbjmmzArb32uEEa8cwTPh4ok++gN7LXjWAbadMhGN4B4Eph6tYroiCArgLUEQ00Fs+PCejC9l1Fx8rTD0dtEhD7rkHXQXUsiMoXCrvJ2Ty/rxAGuGwJdhMAus+tJorx2mRplp76KgZyzt79wMCXk1FHAIUAc9fj4Xz6ZsLOFQO89Dqjd2T1oRIGnUn5MNgWHlxoxcX1EyQRqhBrx5RPDRomoTPF7acICXyza36NDCG0rIPGnj7QXEVcZ8C8c9/EuobVE4/ch3DDVQQ4vRkp5uZlAsAEbLa635GLPJS0YFITqm6bWfIR1iSAn0LBW6EkQxyxfrVt3SLR0DqZsC/i87SmcSp3PiXdyLPC+GJ6ykLIe24UAy4jWb9UWKtQ4mHKBAwgUwG978FGlTL7fxV+4FrnFTPhrVI2SLQyY2091oA9UcLGJiXRiGXL0lUAnbdJdKBvOoFMVR/zkJkv9RnF7AH2UQd14L/CY5Bbj2FeMXJDoL+K6YTLPAaQ6zqL0l0v7FHXoYKZ7gMA0+x6HfJu78XEH7mgz3oRyYIOCHS+DdjTBq6rr7CsiQBYCZBeKLP6WKDnIQZxXgaTrh5i6JhJ42phzU2keQ43gjWbFM7qGgvW1UkeKfZ4r+X9MhD6tQG8C3GOkaGrm1sNmq7VtNkbo6WZqNNmRs8lv+R9PQaai0FUmnBeehJpQOrwC7aCSmmsTgDAbmHmMMJznTSYX0hW53V2hTfJlfCSBkxmvnFB2OoLF4ehSbEbDKF0KHTL2JzvRAlxziOTAbVhjBSwi438fYVlSzJAAIjncoKBFcYHwxYC2AROCxHKBntIpjSxAXO4FOUZFjPOAqzfUbxdV6QF+8qg96bjlHmB7Pijmrh4qnkeTFaXDTzTq4HZkJl9YLvEaqjx9PUSWZaujBjgUaccIy57hpTy4baDM8TsaeTKamXEwpCLxe29BwLA1HFPmNT9B53aVzzAImD0RSTQB8wzzJH2umfsXNVkbxIlpqZB4Mvl3wTmNRoJ3As7DMCYXUW2DjxPhmQBnTtlNnf/x6ZauyCciukcm3efKazrQDtk9pB9vRSLcLTN4BKDhXQrIVe/4bzyVdljnbdsOWDUpXaEJoOG9W1shlK2H/RimF15pKxJITZ5ahDDHiPnLRfQ1IWbBqu9bcqGrSL7uul2gYHxryzHEOUhBvXyLbG3VRjTpjGA9qLBCBx5Jq7FFqmtYkvdaEtp1uCeSpmoB3PPNbJWsXMUD3HkHtu96YQVGLhoceuI2aupTEXTeUtbsBODZZGGOKzJiKyuUSavsy06Z9LiXpKF6zYy4Tx1BaDnou0cIF5EDNBJDSgck8akU+syXsb4F8Nahwesn5vI1yMbwzaHsEEZ2UeNtzeEwpjS0/DWD0bIbQWR4Qi5OdfuTdNFC/Ggw5EIi5yOX+pSmYG985lR9ma3sE3fbC6+LZB5yT326Ht0ie4fTAIHqCMBwB/BmAthu9QT5uQ1/eWT7x4Dv4jor9Ssd5ZSPz2aLZ4/2IuNy3ZlA/RJw3BqVz5A2T2D2txdl+Z9lt535Z3Lp50JdmUH9l3Zld8o2IudKXblewH77gU9u/LdTFCjmf2HH/7s0kryecHHn3/+2wL/0XOlNIAoBUV5ZLcXe5WCMv9fbqcctkvr3Zn1VgBXzyN+u/QVPRLo6nW591zIQO1QuUG9CdqkJ2cSs16aLs06t8uvc0vWn0X9lO+fQq6M/cf1kIVkp+K2SfD1BJ/NUtzDZSbZpF3mrG2So8L3CyYTl1GzAS/cBl+YvtI29J88fwdc8b4rYceC6T6EHIVZr4uE9DdM7wmra+Mai0c/s2MVK4/EewJhBlAwZSAb4zffx3Bk1nlauocDNGH/8R2VvF7a4z5hHZkIufjzsbT8npj1o3Ik+wPrYNprb8R1tCvyCudSs37wfGBep2n7Rt+W2zWbj5LRK9lMwGYn+D32yMbtMjWbOXCSg3Z3zj3/hWzAC393y5TJor3ThZ/nzwpP0faEDTrSacnkoN91+hNWUkEi8hqV2Q3YNhUgfVUcgztWsSy+6iT7uwfGp33u1xAiE7cSmOv2Ko/N5vZQ6iRa9ctNzQuC2AAZmvVGKQJawa45M/6nfohBniFLzjp0YdZPzTRZXJswVkrYd81mFYjhRqmPtkT3jJ4K7XrkjLUBececDbKJ57x2/bNZvxGsRJsFA6iUw31/YoAN6X/OvMkoNgzn2ZiMCVq2CIn6EHhmNl97RkafQ6hFDSAuzea21iOz+ZhdauK2EOTm9fZYvk+cWG7WUEcKtU4b2meJY8DaDNmMHtDwAfES9dTtqZmY9bt32tigy7xOKVic68NBOAZ705scRqjjKzBw75HDsOuPavRPmJcv69jcC3b2ZI/bo3Dc4iWb5xAw8QjRYTFp6HUUBVhOY5qUGXO45XyFOiZ2Z2Rq1svqjYzMgDfBQBlH2MwwoJwo3ietkeMa8g5b2oD+L4S3vWH65MoAOUbbtLPxjA36JNJmPv3JZl18RpPOp+fnZ8OP8l//KO1RyfPyoOK+2xAmYWAYewycw3OMWJwfAkWXXXPH6l6YuOcUCZzyujvGRBPTbDPROQA5FnOQ2JLjuGH6hGxGHvbavH59xDVscW3q3885Evc3scEI/8udq/yxwFIMgjOWcODh1kJMOO+E/cgedzX6D1i429HmSz7MvtV2gZWb++mnv64M9+OPf0kQf8kwYgEDzY3+5Ak9pTRnhs3ACBkzykwBOLFLF52UiFDtHHXlLLtTRrANvX8lY514yn5PGSBmnnBtzOrJWWjQY53tsxk9dZ8oxDBA2xeBQZYxkIRs0GTgnyjZJRokSxaqOPkOGAHl+ORyDCCbZGuuP4V6h0zeJxYOkudwGHQfFxaTpW+C2rTcmfVrIHLOoq4B2xilufh1Bp3SZ0YpPN8zjNpc3HOEug49nTMT8R61z9/RQk+w07saiYkqcZ3xyDQRE8gZA+BIpN1KwYJXgv0ylm7j92g2O2MDecbqr/BfR7En16Nkcx/NBpru8vxn5XzB8EDeYII4/QR6ZUpfjBhBaPqXIp3KdSs9HnBDp/8JMAAoWRW8rHmVTQAAAABJRU5ErkJggg==\"/>\n        </svg>\n    </div>\n    <div class=\"perso-main\">\n        <div class=\"nano perso-container\">\n            <div class=\"nano-content perso-scroll\">\n            </div>\n        </div>\n    </div>\n    <div class=\"perso-footer\">\n        <div class=\"perso-footer-button\">"
    + alias4(((helper = (helper = helpers.closeText || (depth0 != null ? depth0.closeText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"closeText","hash":{},"data":data}) : helper)))
    + "</div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-pslide"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\""
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"perso-header\">\n        <sup><sup>"
    + alias4(((helper = (helper = helpers.feedByText || (depth0 != null ? depth0.feedByText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"feedByText","hash":{},"data":data}) : helper)))
    + " </sup><!--<img src=\"resources/images/perso-logo.png\" class=\"perso-logo\">-->\n            <svg  xmlns:svg=\"http://www.w3.org/2000/svg\" xmlns=\"http://www.w3.org/2000/svg\" width=\"187\" height=\"37\" viewBox=\"0 0 187 37\">\n                <image width=\"187\" height=\"37\" preserveAspectRatio=\"none\" id=\"image10\" x=\"0\" y=\"0\"\n                       xlink:href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALsAAAAlCAYAAAAa/JhZAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjRCQjk1MzY1QkU4NjExRTU4OTQzRThGMDA3MzBBMTYxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjRCQjk1MzY2QkU4NjExRTU4OTQzRThGMDA3MzBBMTYxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NEJCOTUzNjNCRTg2MTFFNTg5NDNFOEYwMDczMEExNjEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NEJCOTUzNjRCRTg2MTFFNTg5NDNFOEYwMDczMEExNjEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5K1DUkAAAMWElEQVR42uxcTXLruBGGX7x/yjqVMl05gOVdMrMwdQLLJ7B8AlknkHUCWScQfQLLJxC9mEl2pg+QMl9+1o/ZT8oBlK+tVhsNgpRdzzNPqGJJokigu/Hh60YD5J7ZlV0JlIM//HFpP1JxOv/y73/2PqrMfzpIXuT95Zf/FlbWyn3f33XndwfexH4k4nRlAVH8BnTr2o+5BXi+v/+7O/vp9Bzb8zdWv2wH9u+vDBwAJFPbo/crB3rHAd2CfGRBPraHA/69PS6cvpbtyx3Yv30HuU55cbW7stUgnuF7akHfs4Af2u9TB3j7fb4D+7d1uS4e7uD3wgL+7AOKOiIZedjzAeV0MmYgDxerjxGuuRCmsvZ93A91gGMcKFba4xETk6JBh7qJwikJALeS2TrKNwZOiknUCWS+t20sfi1AR+k7PazceQOdu6KvCs22LFY/8AEF9fli+KoN2KFjl80PKiZj7P0J6qB7yxB2LJt32c8bsDrh4GiPVd53MY9HMVlKVHStCW3rorgw8fzt7hm5CcMbTbbmnmyBM8qhYsCU6ehkWbz14IsIXR4U2zgiuKi599Iew0A/ucEykYPG3nvlidW18pJtUe5z9V8F5DtX9HvRE3WUnjr6AKl2fwH8ZRx/DAvOE02d/PbcJezkrqs+MYVuI4BuIIRT/gmCGY9R5wFhVxMJDIht2fHBA/SVjCSb6wDXlj2ecP0U8o/x/QGd9G7ZD1H/OGCbfoS+45p+cvZY2uun38hbjWuATvH1g8QAwHlbc38X/faENlcFA4fYfITTjtU/O1Z35LqHRp630HNjlNu6vkYOGjfajgNudwDBO/AmZ+RalTCA1+tkusZ1txHGv4j1NKizj3AgQXv3kmlYqHHLdFiA+ULlWIaKYK2HSLtuxNvODu/N7DX9EcSAref3zFbLBvdq3jvBYKLQJv/7l3Ll5fbxpwm4xA6LuX3F5TFdwxkElgo7Y9/B9fYFw4+RGvKVc1ZXAuD3agy7AHArxoS1RoNrjAH6AF7Lx8jODj0B1KXwiJeegdnxMLOUZx7Q9xEDr++5ZoqJ73uHaaHwl+Z9qUd3nu48DehnMB9LxcT5VYGuV96YPjQa+SoZwoKhEjZMEYf5yg0AkCOUSIRLii1pDdDdHIIb4FZh/QJMTBOeIgLkxBbjmmzArb32uEEa8cwTPh4ok++gN7LXjWAbadMhGN4B4Eph6tYroiCArgLUEQ00Fs+PCejC9l1Fx8rTD0dtEhD7rkHXQXUsiMoXCrvJ2Ty/rxAGuGwJdhMAus+tJorx2mRplp76KgZyzt79wMCXk1FHAIUAc9fj4Xz6ZsLOFQO89Dqjd2T1oRIGnUn5MNgWHlxoxcX1EyQRqhBrx5RPDRomoTPF7acICXyza36NDCG0rIPGnj7QXEVcZ8C8c9/EuobVE4/ch3DDVQQ4vRkp5uZlAsAEbLa635GLPJS0YFITqm6bWfIR1iSAn0LBW6EkQxyxfrVt3SLR0DqZsC/i87SmcSp3PiXdyLPC+GJ6ykLIe24UAy4jWb9UWKtQ4mHKBAwgUwG978FGlTL7fxV+4FrnFTPhrVI2SLQyY2091oA9UcLGJiXRiGXL0lUAnbdJdKBvOoFMVR/zkJkv9RnF7AH2UQd14L/CY5Bbj2FeMXJDoL+K6YTLPAaQ6zqL0l0v7FHXoYKZ7gMA0+x6HfJu78XEH7mgz3oRyYIOCHS+DdjTBq6rr7CsiQBYCZBeKLP6WKDnIQZxXgaTrh5i6JhJ42phzU2keQ43gjWbFM7qGgvW1UkeKfZ4r+X9MhD6tQG8C3GOkaGrm1sNmq7VtNkbo6WZqNNmRs8lv+R9PQaai0FUmnBeehJpQOrwC7aCSmmsTgDAbmHmMMJznTSYX0hW53V2hTfJlfCSBkxmvnFB2OoLF4ehSbEbDKF0KHTL2JzvRAlxziOTAbVhjBSwi438fYVlSzJAAIjncoKBFcYHwxYC2AROCxHKBntIpjSxAXO4FOUZFjPOAqzfUbxdV6QF+8qg96bjlHmB7Pijmrh4qnkeTFaXDTzTq4HZkJl9YLvEaqjx9PUSWZaujBjgUaccIy57hpTy4baDM8TsaeTKamXEwpCLxe29BwLA1HFPmNT9B53aVzzAImD0RSTQB8wzzJH2umfsXNVkbxIlpqZB4Mvl3wTmNRoJ3As7DMCYXUW2DjxPhmQBnTtlNnf/x6ZauyCciukcm3efKazrQDtk9pB9vRSLcLTN4BKDhXQrIVe/4bzyVdljnbdsOWDUpXaEJoOG9W1shlK2H/RimF15pKxJITZ5ahDDHiPnLRfQ1IWbBqu9bcqGrSL7uul2gYHxryzHEOUhBvXyLbG3VRjTpjGA9qLBCBx5Jq7FFqmtYkvdaEtp1uCeSpmoB3PPNbJWsXMUD3HkHtu96YQVGLhoceuI2aupTEXTeUtbsBODZZGGOKzJiKyuUSavsy06Z9LiXpKF6zYy4Tx1BaDnou0cIF5EDNBJDSgck8akU+syXsb4F8Nahwesn5vI1yMbwzaHsEEZ2UeNtzeEwpjS0/DWD0bIbQWR4Qi5OdfuTdNFC/Ggw5EIi5yOX+pSmYG985lR9ma3sE3fbC6+LZB5yT326Ht0ie4fTAIHqCMBwB/BmAthu9QT5uQ1/eWT7x4Dv4jor9Ssd5ZSPz2aLZ4/2IuNy3ZlA/RJw3BqVz5A2T2D2txdl+Z9lt535Z3Lp50JdmUH9l3Zld8o2IudKXblewH77gU9u/LdTFCjmf2HH/7s0kryecHHn3/+2wL/0XOlNIAoBUV5ZLcXe5WCMv9fbqcctkvr3Zn1VgBXzyN+u/QVPRLo6nW591zIQO1QuUG9CdqkJ2cSs16aLs06t8uvc0vWn0X9lO+fQq6M/cf1kIVkp+K2SfD1BJ/NUtzDZSbZpF3mrG2So8L3CyYTl1GzAS/cBl+YvtI29J88fwdc8b4rYceC6T6EHIVZr4uE9DdM7wmra+Mai0c/s2MVK4/EewJhBlAwZSAb4zffx3Bk1nlauocDNGH/8R2VvF7a4z5hHZkIufjzsbT8npj1o3Ik+wPrYNprb8R1tCvyCudSs37wfGBep2n7Rt+W2zWbj5LRK9lMwGYn+D32yMbtMjWbOXCSg3Z3zj3/hWzAC393y5TJor3ThZ/nzwpP0faEDTrSacnkoN91+hNWUkEi8hqV2Q3YNhUgfVUcgztWsSy+6iT7uwfGp33u1xAiE7cSmOv2Ko/N5vZQ6iRa9ctNzQuC2AAZmvVGKQJawa45M/6nfohBniFLzjp0YdZPzTRZXJswVkrYd81mFYjhRqmPtkT3jJ4K7XrkjLUBececDbKJ57x2/bNZvxGsRJsFA6iUw31/YoAN6X/OvMkoNgzn2ZiMCVq2CIn6EHhmNl97RkafQ6hFDSAuzea21iOz+ZhdauK2EOTm9fZYvk+cWG7WUEcKtU4b2meJY8DaDNmMHtDwAfES9dTtqZmY9bt32tigy7xOKVic68NBOAZ705scRqjjKzBw75HDsOuPavRPmJcv69jcC3b2ZI/bo3Dc4iWb5xAw8QjRYTFp6HUUBVhOY5qUGXO45XyFOiZ2Z2Rq1svqjYzMgDfBQBlH2MwwoJwo3ietkeMa8g5b2oD+L4S3vWH65MoAOUbbtLPxjA36JNJmPv3JZl18RpPOp+fnZ8OP8l//KO1RyfPyoOK+2xAmYWAYewycw3OMWJwfAkWXXXPH6l6YuOcUCZzyujvGRBPTbDPROQA5FnOQ2JLjuGH6hGxGHvbavH59xDVscW3q3885Evc3scEI/8udq/yxwFIMgjOWcODh1kJMOO+E/cgedzX6D1i429HmSz7MvtV2gZWb++mnv64M9+OPf0kQf8kwYgEDzY3+5Ak9pTRnhs3ACBkzykwBOLFLF52UiFDtHHXlLLtTRrANvX8lY514yn5PGSBmnnBtzOrJWWjQY53tsxk9dZ8oxDBA2xeBQZYxkIRs0GTgnyjZJRokSxaqOPkOGAHl+ORyDCCbZGuuP4V6h0zeJxYOkudwGHQfFxaTpW+C2rTcmfVrIHLOoq4B2xilufh1Bp3SZ0YpPN8zjNpc3HOEug49nTMT8R61z9/RQk+w07saiYkqcZ3xyDQRE8gZA+BIpN1KwYJXgv0ylm7j92g2O2MDecbqr/BfR7En16Nkcx/NBpru8vxn5XzB8EDeYII4/QR6ZUpfjBhBaPqXIp3KdSs9HnBDp/8JMAAoWRW8rHmVTQAAAABJRU5ErkJggg==\"/>\n            </svg>\n        </sup>\n    </div>\n    <div class=\"perso-main\">\n        <div class=\"nano perso-container\">\n            <div  class=\"nano-content perso-scroll\"></div>\n        </div>\n    </div>\n    <div class=\"perso-footer\"><div class=\"perso-footer-button\">"
    + alias4(((helper = (helper = helpers.closeText || (depth0 != null ? depth0.closeText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"closeText","hash":{},"data":data}) : helper)))
    + "</div></div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-pstory-dialog-continue-btn"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "        		"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.showBtn : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n";
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-dialog-btn\" data-action=\""
    + alias4(((helper = (helper = helpers.actionBtn || (depth0 != null ? depth0.actionBtn : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"actionBtn","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.diaogBtn || (depth0 != null ? depth0.diaogBtn : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"diaogBtn","hash":{},"data":data}) : helper)))
    + "</div>";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-dialog-frame perso-dialog-actions-btn "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.backgroundImage || (depth0 != null ? depth0.backgroundImage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"backgroundImage","hash":{},"data":data}) : helper)))
    + "\" id=\"pdlg-"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-dialog-container\">\n        <div class=\"pstory-dialog-content\" ></div>\n        <div class=\"pstory-dialog-buttons\" data-numOfDialogBtn=\""
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.btn : depth0)) != null ? stack1.length : stack1), depth0))
    + "\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.btn : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-pstory-dialog"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-dialog-frame "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.backgroundImage || (depth0 != null ? depth0.backgroundImage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"backgroundImage","hash":{},"data":data}) : helper)))
    + "\" id=\"pdlg-"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-dialog-container\">\n        <div class=\"pstory-dialog-content\" />\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-pstory-title-inside-story"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class=\"pstory-title-outer-wrapper\">\n    <div class=\"pstory-fixed pstory-title-wrapper\">\n        <div class=\"pstory-title\">\n            <div class=\"pstory-title-text pstory-title-inside-story\">"
    + ((stack1 = ((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-pstory-title"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class=\"pstory-title-outer-wrapper\">\n    <div class=\"pstory-fixed pstory-title-wrapper\">\n        <div class=\"pstory-title\">\n            <div class=\"pstory-title-text\">"
    + ((stack1 = ((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n        </div>\n    </div>\n</div>\n<div id=\"story-frame-container\"></div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["block-bar-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-block-frame pstory-block-bar-chart pstory-block-bar-chart "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " perso-"
    + alias4(((helper = (helper = helpers.direction || (depth0 != null ? depth0.direction : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"direction","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-bar-chart\"></div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["block-line-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"pstory-block-frame pstory-block-line-chart pstory-block-line-chart "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-line-chart\"></div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["block-pie-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"pstory-block-frame pstory-block-pie-chart\" tabindex=\"-1\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-pie-chart\"></div>\n            <div class=\"pstory-pie-data\">\n                <div class=\"pstory-next-arrow\"></div>\n                <div class=\"pstory-pie-info\">\n                </div>\n                <div class=\"pstory-previous-arrow\"></div>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["block-pinGraph-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"pstory-block-frame pstory-block-pinGraph-chart_"
    + container.escapeExpression(((helper = (helper = helpers.index || (depth0 != null ? depth0.index : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"index","hash":{},"data":data}) : helper)))
    + " pstory-block-pinGraph-chart\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-pinGraph-chart\"></div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["block-pure-bar-chart"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "perso-comparable-chart";
},"3":function(container,depth0,helpers,partials,data) {
    return "";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.series : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "                                <div class=\"perso-bar-wrapper perso-simple-bar\">\n                                <div class=\"perso-bar "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.program(9, data, 0),"data":data})) != null ? stack1 : "")
    + " pstory-category=\""
    + container.escapeExpression(((helper = (helper = helpers.category || (depth0 != null ? depth0.category : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"category","hash":{},"data":data}) : helper)))
    + "\"></div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.indicator : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                            </div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "perso-selected\" aria-selected=\"true\"";
},"9":function(container,depth0,helpers,partials,data) {
    return "\"";
},"11":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                                    <div class=\"perso-bar perso-indicator\" pstory-category=\""
    + container.escapeExpression(((helper = (helper = helpers.category || (depth0 != null ? depth0.category : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"category","hash":{},"data":data}) : helper)))
    + "\">\n                                    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-block-frame pstory-block-bar-chart pstory-block-bar-chart "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " perso-"
    + alias4(((helper = (helper = helpers.direction || (depth0 != null ? depth0.direction : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"direction","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-bar-chart\">\n                <div class=\"perso-bar-chart-wrapper perso-accessibility-wrapper "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isSeries : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" role=\"text\" >\n                    <span class=\"perso-accessibility-read perso-accessibility-bar-seleced-text\"></span>\n                    <span class=\"perso-tooltip\"></span>\n                    <div class=\"perso-bar-chart-container\">\n                    <div class=\"perso-bars-wrapper\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isSeries : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "                    </div>\n                    <div class=\"perso-month-amount\"></div>\n                </div>\n            </div>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["block-pure-pinGraph-chart"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "                        <div class=\"perso-pinGraph-raw\" >\n                            <div class=\"perso-raw-wrapper\" role=\"text\">\n                                <span class=\"perso-raw-item perso-date-txt\"></span>\n                                <div class=\"perso-raw-item perso-bar-wrapper\">\n                                    <span class=\"perso-pinGraph-bar\"></span>\n                                </div>\n                            </div>\n                        </div>\n                        <div aria-hidden=\"true\"></div><!--it's required for accessibility-->\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-block-frame pstory-block-pinGraph-chart_"
    + alias4(((helper = (helper = helpers.index || (depth0 != null ? depth0.index : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + " pstory-block-pinGraph-chart "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-pure-pinGraph-chart "
    + alias4(((helper = (helper = helpers.mode || (depth0 != null ? depth0.mode : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"mode","hash":{},"data":data}) : helper)))
    + "\">\n                <div class=\"perso-pinGraph-container\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.series : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["block-spline-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"pstory-block-frame pstory-block-spline-chart\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n            <div class=\"pstory-spline-chart\"></div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["block-tranList"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                    <div class=\"perso-table-row\">\n                        <div class=\"perso-loading-tbl\">\n                            <span>"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.loading : stack1), depth0))
    + "</span>\n                        </div>\n                    </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "                        <div class=\"perso-table-row\">\n                            <div class=\"perso-col perso-left-col\">\n                                <div class=\"perso-row perso-upper-row\">"
    + ((stack1 = alias1((depth0 != null ? depth0.transaction : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.accountNumber : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "                            </div>\n                            <div class=\"perso-col perso-right-col\">\n                                <div class=\"perso-amount-container\"> "
    + ((stack1 = alias1((depth0 != null ? depth0.amount : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n                            </div>\n                        </div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "                                    <div class=\"perso-row perso-lower-row\">"
    + ((stack1 = alias1((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + " <span class=\"perso-account-seperator\">|</span> "
    + ((stack1 = alias1((depth0 != null ? depth0.accountNumber : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                                    <div class=\"perso-row perso-lower-row\">"
    + ((stack1 = container.lambda((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <div class=\"pstory-table pstory-table-new-txlist1\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-perso-loading-animation"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div id='perso-loading-widget'>\n    <div class='perso-loading-text' >\n         "
    + ((stack1 = ((helper = (helper = helpers.loadingText || (depth0 != null ? depth0.loadingText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"loadingText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n    </div>\n    <div class='perso-loading'></div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-perso-widget-story-error"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "class=\"perso-mobile-err\"";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "<div id=\"personetics-error-container\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isMobileDevice : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n    <div class=\"personetics-error-img "
    + container.escapeExpression(((helper = (helper = helpers.imgClass || (depth0 != null ? depth0.imgClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"imgClass","hash":{},"data":data}) : helper)))
    + "\"></div> \n    <div class=\"personetics-error-large\">"
    + ((stack1 = ((helper = (helper = helpers.textLarge || (depth0 != null ? depth0.textLarge : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"textLarge","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n    <div class=\"personetics-error-small\">"
    + ((stack1 = ((helper = (helper = helpers.textSmall || (depth0 != null ? depth0.textSmall : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"textSmall","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n   \n\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["filter-inbox-template"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "			<li role=\"tab\" class =\"perso-teaser-filter-tab\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" aria-selected=\"false\" aria-label=\""
    + alias2(alias1((depth0 != null ? depth0.accText : depth0), depth0))
    + "\" tabindex=\"0\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + " ("
    + alias2(alias1((depth0 != null ? depth0.count : depth0), depth0))
    + ")</li>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"perso-teaser-filter\">\n	<ul role=\"tablist\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.tabs : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</ul>\n	<div class=\"perso-filter-selected-tab\"></div>\n</div>\n\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["inbox-template"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-title perso-teaser-title\" aria-label=\""
    + alias4(((helper = (helper = helpers.inboxTitle || (depth0 != null ? depth0.inboxTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"inboxTitle","hash":{},"data":data}) : helper)))
    + "\">\n    <div aria-live=\"assertive\" role=\"text\" class=\"pstory-title-text\">"
    + alias4(((helper = (helper = helpers.inboxTitle || (depth0 != null ? depth0.inboxTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"inboxTitle","hash":{},"data":data}) : helper)))
    + "</div>\n    <input aria-label=\""
    + alias4(((helper = (helper = helpers.closeInboxBtnlbl || (depth0 != null ? depth0.closeInboxBtnlbl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"closeInboxBtnlbl","hash":{},"data":data}) : helper)))
    + "\" aria-role=\"text\" type=\"button\" class=\"pstory-title-close\"/>\n</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "        <div class='perso-teaser-filter-wrapper'></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.filter : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "<div class='perso-multi-teaser-wrapper'>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.inboxTitle : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    <div class='perso-multi-teaser-template'></div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-perso-teaser-carousel-desktop"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class='perso-multi-teaser-wrapper'>\n    <div class=\"perso-carousel-animate perso-carousel-wrapper\">\n        <div class=\"perso-carousel-outer-wrapper\">\n            <div class='perso-multi-teaser-template perso-carousel-inner-wrapper'></div>\n        </div>\n        <div class=\"perso-teaser-navigation\">\n            <input type=\"button\" class=\"pstory-carousel-arrow pstory-carousel-arrow-prev\" aria-label=\""
    + alias4(((helper = (helper = helpers.prev || (depth0 != null ? depth0.prev : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"prev","hash":{},"data":data}) : helper)))
    + "\" tabindex=\"-1\"/>\n            <input type=\"button\" class=\"pstory-acc-carousel-selected\" aria-label=\""
    + alias4(((helper = (helper = helpers.selected || (depth0 != null ? depth0.selected : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"selected","hash":{},"data":data}) : helper)))
    + "\"/>\n            <div class=\"perso-carousel-index-wrapper\" aria-labelledby=\"perso-accessibility-read-"
    + alias4(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.insights : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.id : stack1), depth0))
    + "\">\n                <div class=\"perso-carousel-index-container\"></div>\n            </div>  \n            <input type=\"button\" class=\"pstory-carousel-arrow pstory-carousel-arrow-next\" aria-label=\""
    + alias4(((helper = (helper = helpers.next || (depth0 != null ? depth0.next : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"next","hash":{},"data":data}) : helper)))
    + "\" tabindex=\"-1\"/>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-perso-teaser-carousel"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class='perso-multi-teaser-wrapper'>\n    <div class=\"perso-carousel-index-wrapper\">\n        <div class=\"perso-carousel-index-container\">\n        </div>\n    </div>  \n    <div class=\"perso-carousel-animate perso-carousel-wrapper\">\n        <div class=\"perso-carousel-outer-wrapper\">\n            <div class='perso-multi-teaser-template perso-carousel-inner-wrapper'></div>\n        </div>\n        <input type=\"button\" class=\"pstory-carousel-arrow pstory-carousel-arrow-prev pstory-carousel-arrow-hidden\" aria-label=\""
    + alias4(((helper = (helper = helpers.prev || (depth0 != null ? depth0.prev : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"prev","hash":{},"data":data}) : helper)))
    + "\"/>\n        <input type=\"button\" class=\"pstory-acc-carousel-selected\" aria-label=\""
    + alias4(((helper = (helper = helpers.selected || (depth0 != null ? depth0.selected : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"selected","hash":{},"data":data}) : helper)))
    + "\"/>\n        <input type=\"button\" class=\"pstory-carousel-arrow pstory-carousel-arrow-next pstory-carousel-arrow-hidden\" aria-label=\""
    + alias4(((helper = (helper = helpers.next || (depth0 != null ? depth0.next : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"next","hash":{},"data":data}) : helper)))
    + "\"/>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-perso-feedback"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"perso-feedback-wrapper\">\n    <div class=\"perso-textarea-wrapper\">\n        <div class=\"perso-textarea-container\">\n            <div class=\"perso-accessibility-read perso-txt-feedback-box-label\" id=\"txtFeedbackBoxLabel\">"
    + alias4(((helper = (helper = helpers.lblAccessibilityfeedbackprompt || (depth0 != null ? depth0.lblAccessibilityfeedbackprompt : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lblAccessibilityfeedbackprompt","hash":{},"data":data}) : helper)))
    + "</div>\n            <textarea class=\"perso-feedback-text perso-direction-text-left\" role=\"textbox\" aria-labelledby=\"txtFeedbackBoxLabel\"  maxlength=\"500\" placeholder=\""
    + ((stack1 = ((helper = (helper = helpers.lblfeedbackprompt || (depth0 != null ? depth0.lblfeedbackprompt : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lblfeedbackprompt","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\"></textarea>\n            <div class=\"perso-textarea-size\" ></div>\n        </div>\n        <input class=\"perso-feedback-submit\" type=\"button\" value=\""
    + ((stack1 = ((helper = (helper = helpers.btnsubmit || (depth0 != null ? depth0.btnsubmit : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"btnsubmit","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" disabled=\"disabled\"/>\n    </div>\n    <div class=\"perso-feedback-reaction\" tabindex=\"0\" role=\"text\" aria-label=\""
    + alias4(((helper = (helper = helpers.lblreactionFeedback || (depth0 != null ? depth0.lblreactionFeedback : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lblreactionFeedback","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.lblreactionFeedback || (depth0 != null ? depth0.lblreactionFeedback : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lblreactionFeedback","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-perso-widget-teaser"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "                <input type=\"button\" class=\"perso-teaser-filter-tab"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" value=\""
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "\"/>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " perso-selected-tab";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"perso-teaser-widget-container\">\n    <div class=\"perso-teaser-header-wrapper\">\n        <div class=\"perso-teaser-header-top\"></div>\n        <div class=\"perso-teaser-header\">\n            <div class=\"perso-teaser-header-close\"></div>\n            <div class=\"perso-teaser-header-title\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.insightsTitle : stack1), depth0))
    + "</div>\n        </div>\n        <div class=\"perso-teaser-filter-tabs\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.tabs : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            <div class=\"pstory-tab-animated-border-bottom\"></div>\n        </div>\n   </div>\n    <div class=\"perso-insights-container\">\n        <div class=\"perso-loading-wrapper\">\n            <div class=\"perso-loading\"></div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["inboxContainer"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div id=\"personetics-inbox-container\">\n</div>\n<div id=\"personetics-story-inner-container\">\n    <div class=\"perso-story-inner-top\">\n        <div class=\"perso-story-top-title\"></div>\n        <div class=\"perso-story-top-buttons-wrapper\">\n            <div class=\"perso-story-top-button perso-story-top-button-inbox\">\n                <input id=\"story-button-inbox\" type=\"button\" class=\"perso-story-top-button-circle\">\n                <div class=\"perso-story-top-button-circle-img\"></div>\n                </input>\n                <div class=\"perso-story-top-button-text\">"
    + container.escapeExpression(((helper = (helper = helpers.insightsInboxTitle || (depth0 != null ? depth0.insightsInboxTitle : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"insightsInboxTitle","hash":{},"data":data}) : helper)))
    + "</div>\n            </div>\n        </div>\n    </div>\n    <div class=\"perso-story-inner\">\n        <div id=\"personetics-story\"></div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-account-selector-accessibility"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "			<div class=\"perso-header-container\">\n				<div class=\"pstory-tab-animated-border-bottom\"></div>\n				<ul class=\"perso-tablist\" role=\"tablist\">\n					<li class=\"pstory-header-button\" role=\"tab\" tabindex=\""
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" data-all=\"true\" pstory-id=\"all\">"
    + ((stack1 = alias5(((stack1 = (depth0 != null ? depth0.headerTexts : depth0)) != null ? stack1["0"] : stack1), depth0)) != null ? stack1 : "")
    + "</li>\n					<li class=\"pstory-header-button\" role=\"tab\" tabindex=\""
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" data-all=\"false\">"
    + ((stack1 = alias5(((stack1 = (depth0 != null ? depth0.headerTexts : depth0)) != null ? stack1["1"] : stack1), depth0)) != null ? stack1 : "")
    + "</li>\n				</ul>\n			</div>\n			<div class=\"perso-carousel-animate perso-carousel-wrapper perso-options-container perso-show-all\">\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "				<div class=\"perso-carousel-animate perso-carousel-wrapper perso-options-container\">\n";
},"5":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "						<div class=\"perso-carousel-item pstory-button "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" aria-hidden=\"true\" pstory-event=\""
    + alias4(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"event","hash":{},"data":data}) : helper)))
    + "\"\n						 role=\""
    + alias4(alias5((depths[1] != null ? depths[1].role : depths[1]), depth0))
    + "\">\n							<div class=\"perso-card perso-card_"
    + alias4(((helper = (helper = helpers.typeImage || (depth0 != null ? depth0.typeImage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"typeImage","hash":{},"data":data}) : helper)))
    + " perso-cards"
    + alias4(((helper = (helper = helpers.numCards || (depth0 != null ? depth0.numCards : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numCards","hash":{},"data":data}) : helper)))
    + "\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = container.invokePartial(partials.innerCard,depth0,{"name":"innerCard","hash":{"numberPrefAccessibility":(depths[1] != null ? depths[1].numberPrefAccessibility : depths[1]),"numberPref":(depths[1] != null ? depths[1].numberPref : depths[1])},"data":data,"indent":"\t\t\t\t\t\t\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "							</div>\n							<div id=\"pop-"
    + alias4(alias5(depth0, depth0))
    + "\" class=\"pstory-button-txt-wrapper\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.text2 : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " \n								<span class=\"pstory-button-txt\">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n							</div>\n						</div>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "perso-selected";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "								<span class=\"pstory-button-txt  perso-bold\">"
    + ((stack1 = ((helper = (helper = helpers.text2 || (depth0 != null ? depth0.text2 : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"text2","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "						<span class=\"perso-bold\">"
    + ((stack1 = ((helper = (helper = helpers["perso-all-accounts-txt2"] || (depth0 != null ? depth0["perso-all-accounts-txt2"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"perso-all-accounts-txt2","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span> ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"perso-accessibility-wrapper perso-accessibility-account-selector\" role=\"text\">\n	<span class=\"perso-accessibility-read\">"
    + alias4(((helper = (helper = helpers.accountSelectorText || (depth0 != null ? depth0.accountSelectorText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountSelectorText","hash":{},"data":data}) : helper)))
    + "</span>\n</div>\n<div id=\"pstory-block-account-selector_"
    + alias4(((helper = (helper = helpers.index || (depth0 != null ? depth0.index : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" class=\"pstory-block-frame pstory-block-account-selector "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" data-showAll="
    + alias4(((helper = (helper = helpers.showAll || (depth0 != null ? depth0.showAll : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"showAll","hash":{},"data":data}) : helper)))
    + ">\n	<div class=\"pstory-block-container\">\n		<div class=\"pstory-block-content\" tabindex=\"0\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showAll : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(3, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "					<div class=\"perso-carousel-inner-wrapper perso-cards-wrapper\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "					</div>\n					<div class=\"perso-accessibility-wrapper perso-account-button perso-selected\" role=\"text\">\n						<span class=\"perso-accessibility-read\">"
    + alias4(((helper = (helper = helpers.accountName || (depth0 != null ? depth0.accountName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountName","hash":{},"data":data}) : helper)))
    + ". "
    + alias4(((helper = (helper = helpers.numberPrefAccessibility || (depth0 != null ? depth0.numberPrefAccessibility : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numberPrefAccessibility","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.accountNumber || (depth0 != null ? depth0.accountNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountNumber","hash":{},"data":data}) : helper)))
    + "</span>\n					</div>\n					<input class=\"perso-account-button perso-prev\" id=\"prev\" type=\"button\" value=\"\" aria-label=\""
    + alias4(((helper = (helper = helpers.next || (depth0 != null ? depth0.next : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"next","hash":{},"data":data}) : helper)))
    + "\" />\n					<input class=\"perso-account-button perso-next\" id=\"next\" type=\"button\" value=\"\" aria-label=\""
    + alias4(((helper = (helper = helpers.next || (depth0 != null ? depth0.next : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"next","hash":{},"data":data}) : helper)))
    + "\" />\n					<div class=\"pstory-all-accounts-txt\" role=\"text\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.allAccountsTxt2 : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n						<span class=\"perso-amount-wrapper\">"
    + ((stack1 = ((helper = (helper = helpers.allAccountsTxt || (depth0 != null ? depth0.allAccountsTxt : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"allAccountsTxt","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n					</div>\n				</div>\n				<div class=\"pstory-comment\">"
    + ((stack1 = ((helper = (helper = helpers.comment || (depth0 != null ? depth0.comment : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"comment","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n			</div>\n		</div>\n	</div>";
},"usePartial":true,"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-account-selector-inner-card"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "            <span class=\"pstory-button-txt perso-bold\">"
    + ((stack1 = ((helper = (helper = helpers.text2 || (depth0 != null ? depth0.text2 : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"text2","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <span class=\"perso-account-number\" aria-label=\""
    + alias4(((helper = (helper = helpers.numberPrefAccessibility || (depth0 != null ? depth0.numberPrefAccessibility : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numberPrefAccessibility","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.number || (depth0 != null ? depth0.number : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"number","hash":{},"data":data}) : helper)))
    + ".\">*"
    + alias4(((helper = (helper = helpers.number || (depth0 != null ? depth0.number : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"number","hash":{},"data":data}) : helper)))
    + "</span>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"perso-card perso-card_"
    + alias4(((helper = (helper = helpers.typeImage || (depth0 != null ? depth0.typeImage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"typeImage","hash":{},"data":data}) : helper)))
    + " perso-cards"
    + alias4(((helper = (helper = helpers.numCards || (depth0 != null ? depth0.numCards : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numCards","hash":{},"data":data}) : helper)))
    + "\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"perso-card-image\">\n        <div class=\"perso-image\"></div>\n    </div>\n    <div class=\"perso-card-container\">\n        <div id=\"pop-"
    + alias4(container.lambda(depth0, depth0))
    + "\" class=\"pstory-button-txt-wrapper\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.text2 : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n        <div>\n            <span class=\"pstory-button-txt\">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n        </div>\n        <span class=\"perso-account-name\" aria-label=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + ".\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</span>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.number : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-account-selector-new"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "pstory-tabs";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <div class=\"perso-overlay perso-overlay-all\"></div>\n                <div class=\"perso-overlay perso-overlay-choose\"></div>\n                <div class=\"perso-header-container\">\n\n                    <ul class=\"perso-tablist\" role=\"tablist\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.headersArray : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </ul>\n                </div>\n            <div class=\"perso-carousel-outer-wrapper perso-collapse\">\n            <div class=\"perso-carousel-animate perso-options-container perso-carousel-wrapper pstory-show-header-tabs\">\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                            <li class=\"pstory-header-button\"  tabindex=\"0\" aria-label=\""
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\"  role=\"tab\" data-all=\""
    + container.escapeExpression(((helper = (helper = helpers.isAll || (depth0 != null ? depth0.isAll : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"isAll","hash":{},"data":data}) : helper)))
    + "\"\n                                "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isAll : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</li>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "pstory-id=\"all\" ";
},"7":function(container,depth0,helpers,partials,data) {
    return "            <div class=\"perso-carousel-outer-wrapper perso-collapse\">\n            <div class=\"perso-carousel-animate perso-options-container perso-carousel-wrapper\">\n";
},"9":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <div class=\"perso-carousel-item pstory-button "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" aria-hidden=\"true\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" pstory-event=\""
    + alias4(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"event","hash":{},"data":data}) : helper)))
    + "\"\n                         role=\""
    + alias4(container.lambda((depths[1] != null ? depths[1].role : depths[1]), depth0))
    + "\">\n                        "
    + ((stack1 = helpers["if"].call(alias1,(depths[1] != null ? depths[1].isDesktopMode : depths[1]),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.program(14, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "\n                    </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "perso-selected";
},"12":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return " "
    + ((stack1 = container.invokePartial(partials.innerCardDesktop,depth0,{"name":"innerCardDesktop","hash":{"numberPrefAccessibility":(depths[1] != null ? depths[1].numberPrefAccessibility : depths[1]),"numberPref":(depths[1] != null ? depths[1].numberPref : depths[1])},"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n                        ";
},"14":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return " "
    + ((stack1 = container.invokePartial(partials.innerCard,depth0,{"name":"innerCard","hash":{"numberPrefAccessibility":(depths[1] != null ? depths[1].numberPrefAccessibility : depths[1]),"numberPref":(depths[1] != null ? depths[1].numberPref : depths[1])},"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + " ";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "                <div class=\"perso-carousel-index-wrapper\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isDesktopMode : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n                    <div class=\"perso-carousel-index-container\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </div>\n                </div>\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " role=\"application\" aria-labelledby=\"perso-accessibility-read-"
    + container.escapeExpression(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.options : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.id : stack1), depth0))
    + "\"\n                ";
},"19":function(container,depth0,helpers,partials,data) {
    return "                            <div class=\"perso-carousel-index-dot\">\n                                <div class=\"perso-carousel-index-dot-circle\"></div>\n                            </div>\n";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression, alias2=depth0 != null ? depth0 : (container.nullContext || {}), alias3=helpers.helperMissing, alias4="function";

  return "                <div class=\"pstory-all-accounts-txt\" role=\"text\">\n                    <span class=\"perso-bold\">"
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.options : depth0)) != null ? stack1.length : stack1), depth0))
    + " "
    + alias1(((helper = (helper = helpers.lblaccSelectAccounts || (depth0 != null ? depth0.lblaccSelectAccounts : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"lblaccSelectAccounts","hash":{},"data":data}) : helper)))
    + "</span>\n                    <span class=\"perso-amount-wrapper\">"
    + ((stack1 = helpers["if"].call(alias2,(depth0 != null ? depth0.allAccountsTxt2 : depth0),{"name":"if","hash":{},"fn":container.program(22, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = ((helper = (helper = helpers.allAccountsTxt || (depth0 != null ? depth0.allAccountsTxt : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"allAccountsTxt","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n                </div>\n";
},"22":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return ((stack1 = ((helper = (helper = helpers.allAccountsTxt2 || (depth0 != null ? depth0.allAccountsTxt2 : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"allAccountsTxt2","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + ": ";
},"24":function(container,depth0,helpers,partials,data) {
    return "\n                   tabindex=\"-1\" ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "\n<div id=\"pstory-block-account-selector_"
    + alias4(((helper = (helper = helpers.index || (depth0 != null ? depth0.index : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" class=\"pstory-block-frame pstory-block-account-selector "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" data-show-header-tabs="
    + alias4(((helper = (helper = helpers.showHeaderTabs || (depth0 != null ? depth0.showHeaderTabs : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"showHeaderTabs","hash":{},"data":data}) : helper)))
    + ">\n    <div class=\"pstory-block-container "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showHeaderTabs : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\n        <div class=\"pstory-block-content\" tabindex=\"0\">\n            <div class=\"perso-accessibility-wrapper perso-accessibility-account-selector\" role=\"text\">\n                <span class=\"perso-accessibility-read\">"
    + alias4(((helper = (helper = helpers.accountSelectorText || (depth0 != null ? depth0.accountSelectorText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountSelectorText","hash":{},"data":data}) : helper)))
    + "</span>\n            </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showHeaderTabs : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.program(7, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "            <div class=\"perso-carousel-inner-wrapper perso-cards-wrapper\" aria-hidden=\"true\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n\n            <div class=\"perso-accessibility-wrapper pstory-account-button perso-selected\" role=\"text\">\n                <span class=\"perso-accessibility-read\">"
    + alias4(((helper = (helper = helpers.accountName || (depth0 != null ? depth0.accountName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountName","hash":{},"data":data}) : helper)))
    + ". "
    + alias4(((helper = (helper = helpers.numberPrefAccessibility || (depth0 != null ? depth0.numberPrefAccessibility : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numberPrefAccessibility","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.accountNumber || (depth0 != null ? depth0.accountNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountNumber","hash":{},"data":data}) : helper)))
    + "</span>\n            </div>\n\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showIndexDots : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showHeaderTabs : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n            <input type=\"button\" class=\"pstory-account-button pstory-prev-account-button\" aria-label=\""
    + alias4(((helper = (helper = helpers.next || (depth0 != null ? depth0.next : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"next","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isDesktopMode : depth0),{"name":"if","hash":{},"fn":container.program(24, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " />\n            <input type=\"button\" class=\"pstory-account-button pstory-next-account-button\" aria-label=\""
    + alias4(((helper = (helper = helpers.next || (depth0 != null ? depth0.next : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"next","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isDesktopMode : depth0),{"name":"if","hash":{},"fn":container.program(24, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "/>\n        </div>\n            <div class=\"pstory-comment\">"
    + ((stack1 = ((helper = (helper = helpers.comment || (depth0 != null ? depth0.comment : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"comment","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n        </div>\n        </div>\n        </div>\n";
},"usePartial":true,"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-account-selector"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "                <div class=\"perso-header-container\">\n                    <div class=\"pstory-tab-animated-border-bottom\"></div>\n                    <ul class=\"perso-tablist\" role=\"tablist\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.headersArray : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </ul>\n                </div>\n            <div class=\"perso-carousel-outer-wrapper\">\n            <div class=\"perso-carousel-animate perso-options-container perso-carousel-wrapper pstory-show-header-tabs\">\n";
},"2":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                            <li class=\"pstory-header-button\" "
    + ((stack1 = helpers["if"].call(alias1,(depths[1] != null ? depths[1].isDesktopMode : depths[1]),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " role=\"tab\" data-all=\""
    + container.escapeExpression(((helper = (helper = helpers.isAll || (depth0 != null ? depth0.isAll : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"isAll","hash":{},"data":data}) : helper)))
    + "\"\n                                "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isAll : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</li>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return " tabindex=\"0\" aria-label=\""
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" ";
},"5":function(container,depth0,helpers,partials,data) {
    return "pstory-id=\"all\" ";
},"7":function(container,depth0,helpers,partials,data) {
    return "            <div class=\"perso-carousel-outer-wrapper\">\n            <div class=\"perso-carousel-animate perso-options-container perso-carousel-wrapper\">\n";
},"9":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <div class=\"perso-carousel-item pstory-button "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selected : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" aria-hidden=\"true\" pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" pstory-event=\""
    + alias4(((helper = (helper = helpers.event || (depth0 != null ? depth0.event : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"event","hash":{},"data":data}) : helper)))
    + "\"\n                         role=\""
    + alias4(container.lambda((depths[1] != null ? depths[1].role : depths[1]), depth0))
    + "\">\n                        "
    + ((stack1 = helpers["if"].call(alias1,(depths[1] != null ? depths[1].isDesktopMode : depths[1]),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.program(14, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "\n                    </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "perso-selected";
},"12":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return " "
    + ((stack1 = container.invokePartial(partials.innerCardDesktop,depth0,{"name":"innerCardDesktop","hash":{"numberPrefAccessibility":(depths[1] != null ? depths[1].numberPrefAccessibility : depths[1]),"numberPref":(depths[1] != null ? depths[1].numberPref : depths[1])},"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n                        ";
},"14":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return " "
    + ((stack1 = container.invokePartial(partials.innerCard,depth0,{"name":"innerCard","hash":{"numberPrefAccessibility":(depths[1] != null ? depths[1].numberPrefAccessibility : depths[1]),"numberPref":(depths[1] != null ? depths[1].numberPref : depths[1])},"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + " ";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "                <div class=\"perso-carousel-index-wrapper\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isDesktopMode : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n                    <div class=\"perso-carousel-index-container\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </div>\n                </div>\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " role=\"application\" aria-labelledby=\"perso-accessibility-read-"
    + container.escapeExpression(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.options : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.id : stack1), depth0))
    + "\"\n                ";
},"19":function(container,depth0,helpers,partials,data) {
    return "                            <div class=\"perso-carousel-index-dot\">\n                                <div class=\"perso-carousel-index-dot-circle\"></div>\n                            </div>\n";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                    <span class=\"perso-bold\">"
    + ((stack1 = ((helper = (helper = helpers["perso-all-accounts-txt2"] || (depth0 != null ? depth0["perso-all-accounts-txt2"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"perso-all-accounts-txt2","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n";
},"23":function(container,depth0,helpers,partials,data) {
    return "\n                   tabindex=\"-1\" ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"perso-accessibility-wrapper perso-accessibility-account-selector\" role=\"text\">\n    <span class=\"perso-accessibility-read\">"
    + alias4(((helper = (helper = helpers.accountSelectorText || (depth0 != null ? depth0.accountSelectorText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountSelectorText","hash":{},"data":data}) : helper)))
    + "</span>\n</div>\n<div id=\"pstory-block-account-selector_"
    + alias4(((helper = (helper = helpers.index || (depth0 != null ? depth0.index : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" class=\"pstory-block-frame pstory-block-account-selector "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" data-show-header-tabs="
    + alias4(((helper = (helper = helpers.showHeaderTabs || (depth0 != null ? depth0.showHeaderTabs : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"showHeaderTabs","hash":{},"data":data}) : helper)))
    + ">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showHeaderTabs : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(7, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "            <div class=\"perso-carousel-inner-wrapper perso-cards-wrapper\" aria-hidden=\"true\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.options : depth0),{"name":"each","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n\n            <div class=\"perso-accessibility-wrapper pstory-account-button perso-selected\" role=\"text\">\n                <span class=\"perso-accessibility-read\">"
    + alias4(((helper = (helper = helpers.accountName || (depth0 != null ? depth0.accountName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountName","hash":{},"data":data}) : helper)))
    + ". "
    + alias4(((helper = (helper = helpers.numberPrefAccessibility || (depth0 != null ? depth0.numberPrefAccessibility : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"numberPrefAccessibility","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.accountNumber || (depth0 != null ? depth0.accountNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountNumber","hash":{},"data":data}) : helper)))
    + "</span>\n            </div>\n\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showIndexDots : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            <div class=\"pstory-all-accounts-txt\" role=\"text\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.allAccountsTxt2 : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                <span>"
    + ((stack1 = ((helper = (helper = helpers.allAccountsTxt || (depth0 != null ? depth0.allAccountsTxt : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"allAccountsTxt","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n            </div>\n        </div>\n            <input type=\"button\" class=\"pstory-account-button pstory-prev-account-button\" aria-label=\""
    + alias4(((helper = (helper = helpers.next || (depth0 != null ? depth0.next : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"next","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isDesktopMode : depth0),{"name":"if","hash":{},"fn":container.program(23, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " />\n            <input type=\"button\" class=\"pstory-account-button pstory-next-account-button\" aria-label=\""
    + alias4(((helper = (helper = helpers.next || (depth0 != null ? depth0.next : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"next","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isDesktopMode : depth0),{"name":"if","hash":{},"fn":container.program(23, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "/>\n        </div>\n            <div class=\"pstory-comment\">"
    + ((stack1 = ((helper = (helper = helpers.comment || (depth0 != null ? depth0.comment : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"comment","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n        </div>\n        </div>\n        </div>";
},"usePartial":true,"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-pie-chart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-pie-chart\" >\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"0\">\n            <div class=\"pstory-pie-chart\"></div>\n            <div class=\"pstory-pie-data\">\n                <input type=\"button\" class=\"pstory-arrow pstory-next-arrow\" aria-hidden=\"true\"/>\n                <div class=\"pstory-pie-info perso-accessibility-wrapper\" role=\"text\">\n                </div>\n                <input type=\"button\" class=\"pstory-arrow pstory-previous-arrow\" aria-label=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.nextCategory : stack1), depth0))
    + "\"/>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table-minimal"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                    <tr>\n                        <td class=\"perso-loading-table\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.loading : stack1), depth0))
    + "</td>\n                    </tr>\n";
},"3":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=container.lambda;

  return "                            <tr class=\""
    + container.escapeExpression(alias1((depths[1] != null ? depths[1]["class"] : depths[1]), depth0))
    + "\">\n                                <td role=\"cell\" class=\"perso-td-date\">"
    + ((stack1 = alias1((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "</td>\n                                <td  align=\"right\" role=\"cell\" class=\"perso-td-amount-balance\">"
    + ((stack1 = alias1((depth0 != null ? depth0.amount : depth0), depth0)) != null ? stack1 : "")
    + " </td>                                \n                                <td  align=\"right\" class=\"perso-td-amount-balance  \" role=\"cell\"> "
    + ((stack1 = alias1((depth0 != null ? depth0.account : depth0), depth0)) != null ? stack1 : "")
    + "</td>\n\n                            </tr>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <table class=\"pstory-table\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(3, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "            </table>\n        </div>\n    </div>\n</div>";
},"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table-perso-subscriptions"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression, alias2=container.lambda;

  return "                    <div class=\"perso-table-row\">\n                        <div class=\"perso-col perso-left-icon\">\n                            <div class=\"perso-col-icon "
    + alias1(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"icon","hash":{},"data":data}) : helper)))
    + "\"></div>\n                        </div>\n                        <div class=\"perso-col perso-left-col\">\n                            <div class=\"perso-row perso-upper-row\">"
    + ((stack1 = alias2((depth0 != null ? depth0.device : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n                            <div class=\"perso-row perso-lower-row\"><span\n                                    class=\"perso-last-payment\">"
    + alias1(alias2(((stack1 = ((stack1 = (data && data.root)) && stack1.subscriptionTexts)) && stack1.lblLastPayment), depth0))
    + "</span> "
    + ((stack1 = alias2((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "\n                            </div>\n                        </div>\n                        <div class=\"perso-col perso-right-col\">\n                            <div class=\"perso-amount-container\"> "
    + ((stack1 = alias2((depth0 != null ? depth0.amount : depth0), depth0)) != null ? stack1 : "")
    + "\n                                <span class=\"perso-account-seperator\">/</span> "
    + ((stack1 = alias2((depth0 != null ? depth0.cycle : depth0), depth0)) != null ? stack1 : "")
    + "\n                            </div>\n                        </div>\n                    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <div class=\"pstory-table pstory-table-subscriptions\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table-perso-txlist1-new"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                    <div class=\"perso-table-row\">\n                        <div class=\"perso-loading-tbl\">\n                            <span>"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.loading : stack1), depth0))
    + "</span>\n                        </div>\n                    </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.lambda;

  return "                        <div class=\"perso-table-row\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.category : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "\n                            <div class=\"perso-col perso-left-col\">\n                                <div class=\"perso-row perso-upper-row perso-row-transaction\">"
    + ((stack1 = alias2((depth0 != null ? depth0.transaction : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.accountNumber : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.program(14, data, 0),"data":data})) != null ? stack1 : "")
    + "                            </div>\n                            <div class=\"perso-col perso-right-col\">\n                                <div class=\"perso-amount-container\"> "
    + ((stack1 = alias2((depth0 != null ? depth0.amount : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n                            </div>\n                        </div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                            <div class=\"perso-col perso-left-icon\">\n                                <div class=\"perso-col-icon "
    + container.escapeExpression(((helper = (helper = helpers.category || (depth0 != null ? depth0.category : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"category","hash":{},"data":data}) : helper)))
    + "\"></div>\n                            </div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.cashFlowCategory : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.program(10, data, 0),"data":data})) != null ? stack1 : "");
},"8":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                                    <div class=\"perso-col perso-left-icon\">\n                                        <div class=\"perso-col-icon "
    + container.escapeExpression(((helper = (helper = helpers.cashFlowCategory || (depth0 != null ? depth0.cashFlowCategory : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"cashFlowCategory","hash":{},"data":data}) : helper)))
    + "\"></div>\n                                    </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "                                <div class=\"perso-col perso-left-icon\">\n                                    <div class=\"perso-col-icon\"></div>\n                                </div>\n";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "                                    <div class=\"perso-row perso-lower-row perso-row-date\">"
    + ((stack1 = alias1((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + " <span class=\"perso-account-seperator\">|</span> <span class=\"perso-account-number\">"
    + ((stack1 = alias1((depth0 != null ? depth0.accountNumber : depth0), depth0)) != null ? stack1 : "")
    + "</span></div>\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                                    <div class=\"perso-row perso-lower-row perso-row-date\">"
    + ((stack1 = container.lambda((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <div class=\"pstory-table pstory-table-new-txlist1\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table-perso-txlist1"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                    <tr>\n                        <td class=\"perso-loading-table\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.loading : stack1), depth0))
    + "</td>\n                    </tr>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "                        <tr>\n                            <td>\n                                   <span class=\"perso-td-first-row perso-td-account-number\">\n                                       "
    + ((stack1 = alias1((depth0 != null ? depth0.accountNumber : depth0), depth0)) != null ? stack1 : "")
    + "\n                                   </span>\n                                <span class=\"perso-td-transaction\">\n                                    "
    + ((stack1 = alias1((depth0 != null ? depth0.transaction : depth0), depth0)) != null ? stack1 : "")
    + "\n                                </span>\n                            </td>\n                            <td  class=\"perso-rightAlign\" >\n                                     <span class=\"perso-td-first-row perso-td-date\">\n                                         "
    + ((stack1 = alias1((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "\n                                     </span>\n                                <span class=\"perso-td-amount-balance perso-td-amount\">\n                                    "
    + ((stack1 = alias1((depth0 != null ? depth0.amount : depth0), depth0)) != null ? stack1 : "")
    + "\n                                </span>\n                            </td>\n                        </tr>\n\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <table class=\"pstory-table pstory-table-txlist1\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "            </table>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table-perso-txlist2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                    <tr>\n                        <td class=\"perso-loading-table\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.loading : stack1), depth0))
    + "</td>\n                    </tr>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "                            <tr>\n                                <td>\n                                    <span class=\"perso-td-first-row perso-td-transaction\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.transaction : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                </td>\n                                <td  class=\"perso-rightAlign\" >\n                                    <span class=\"perso-td-first-row perso-td-date\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                    <span class=\"perso-td-amount-balance perso-td-amount\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.amount : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                </td>\n                            </tr>\n\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <table class=\"pstory-table pstory-table-txlist2\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "            </table>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table-perso-txlist3"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                    <tr>\n                        <td class=\"perso-loading-table\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.loading : stack1), depth0))
    + "</td>\n                    </tr>\n";
},"3":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=container.lambda;

  return "                            <tr >\n                                <td>\n                                    <span class=\"perso-td-first-row perso-td-date\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>                                    \n                                </td>\n                                <td> \n                                    <span class=\"perso-td-first-row perso-td-account-number\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.accountNumber : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                    <span class=\"perso-td-transaction\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.transaction : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>                           \n                                </td>\n                                <td class=\"perso-rightAlign\" >\n                                     <span class=\"perso-td-first-row perso-td-balance-title perso-td-small\">\n                                        "
    + container.escapeExpression(alias1((depths[1] != null ? depths[1].balanceLabel : depths[1]), depth0))
    + "\n                                    </span>\n                                    <span class=\"perso-td-amount-balance perso-td-balance\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.balance : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                </td>   \n                                \n                            </tr>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <table class=\"pstory-table pstory-table-txlist3\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(3, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "            </table>\n        </div>\n    </div>\n</div>";
},"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table-perso-txlist4"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <div id=\"perso-legend-wrapper\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.legendTexts : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div class=\"perso-legend-container\">\n                            <div class=\"perso-legend-dot "
    + ((stack1 = ((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "-dot\"></div>\n                            <span class=\"perso-legend-text\">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n                        </div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                    <div class=\"perso-table-row\">\n                        <div class=\"perso-loading-tbl\">\n                            <span>"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.loading : stack1), depth0))
    + "</span>\n                        </div>\n                    </div>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "                    <div class=\"perso-table-row perso-scheduled-"
    + ((stack1 = alias1((depth0 != null ? depth0.isScheduled : depth0), depth0)) != null ? stack1 : "")
    + "\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.isScheduled : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.program(10, data, 0),"data":data})) != null ? stack1 : "")
    + "                        <div class=\"perso-table-cell perso-cell-1\">\n                            <div class=\"perso-transaction-title\">"
    + ((stack1 = alias1((depth0 != null ? depth0.transaction : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n                            <div class=\"perso-transaction-date\">"
    + ((stack1 = alias1((depth0 != null ? depth0.isAnticipated : depth0), depth0)) != null ? stack1 : "")
    + " "
    + ((stack1 = alias1((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n                        </div>\n                        <div class=\"perso-table-cell perso-cell-2\">\n                            <div class=\"perso-td-amount-balance perso-td-amount\">"
    + ((stack1 = alias1((depth0 != null ? depth0.amount : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n                        </div>\n                    </div>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                            <div class=\"perso-large-legend-dot perso-scheduled-dot "
    + ((stack1 = container.lambda((depth0 != null ? depth0.debitOrCredit : depth0), depth0)) != null ? stack1 : "")
    + "\"></div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                            <div class=\"perso-large-legend-dot perso-estimated-dot "
    + ((stack1 = container.lambda((depth0 != null ? depth0.debitOrCredit : depth0), depth0)) != null ? stack1 : "")
    + "\"></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.legendTexts : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            <div class=\"pstory-table-type-2 pstory-table-txlist4\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table-perso-txlist5"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                    <tr>\n                        <td class=\"perso-loading-table\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.loading : stack1), depth0))
    + "</td>\n                    </tr>\n";
},"3":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=container.lambda;

  return "                            <tr >\n                                <td>\n                                    <span class=\"perso-td-first-row perso-td-date\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                </td>\n                                <td  class=\"perso-leftAlign\">\n                                    <span class=\"perso-td-first-row perso-td-transaction perso-td-small\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.transaction : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                      <span class=\"perso-td-amount-balance perso-td-amount\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.amount : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                </td>\n                                <td  class=\"perso-leftAlign\" role=\"cell\">\n                                    <span class=\"perso-td-first-row perso-td-balance-title perso-td-small\">\n                                        "
    + container.escapeExpression(alias1((depths[1] != null ? depths[1].balanceLabel : depths[1]), depth0))
    + "\n                                    </span>\n                                      <span class=\"perso-td-amount-balance perso-td-balance\">\n                                          "
    + ((stack1 = alias1((depth0 != null ? depth0.balance : depth0), depth0)) != null ? stack1 : "")
    + "\n                                      </span>\n                                </td>\n                            </tr>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <table class=\"pstory-table pstory-table-txlist5\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(3, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "            </table>\n        </div>\n    </div>\n</div>";
},"useData":true,"useDepths":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pstory-block-table-perso-txlist6"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                    <tr>\n                        <td class=\"perso-loading-table\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.texts : depth0)) != null ? stack1.loading : stack1), depth0))
    + "</td>\n                    </tr>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.rows : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "                            <tr>\n                                <td>\n                                    <span class=\"perso-td-date\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.date : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                </td>\n                                <td >\n                                    <span class=\"perso-td-transaction\">\n                                        "
    + ((stack1 = alias1((depth0 != null ? depth0.transaction : depth0), depth0)) != null ? stack1 : "")
    + "\n                                    </span>\n                                </td>\n                                <td  class=\"perso-rightAlign\">\n                                       <span class=\"perso-td-amount-balance perso-td-amount\">\n                                           "
    + ((stack1 = alias1((depth0 != null ? depth0.amount : depth0), depth0)) != null ? stack1 : "")
    + "\n                                       </span>\n                                </td>\n                            </tr>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"pstory-block-frame pstory-block-table\">\n    <div class=\"pstory-block-container\">\n        <div class=\"pstory-block-content\" tabindex=\"-1\">\n            <table class=\"pstory-table pstory-table-txlist6\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.future : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "            </table>\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["tmpl-pstory-title"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"pstory-title-outer-wrapper\">\n    <div class=\"pstory-title-wrapper pstory-fixed\">\n        <div class=\"pstory-title\">\n            <input aria-label=\""
    + alias4(((helper = (helper = helpers.backButtonText || (depth0 != null ? depth0.backButtonText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"backButtonText","hash":{},"data":data}) : helper)))
    + "\" type=\"button\" class=\"pstory-title-close\"/>\n            <div role=\"text\" aria-label=\""
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + ", heading level 1\" class=\"pstory-title-text\">"
    + ((stack1 = ((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\n        </div>\n    </div>\n</div>\n<div id=\"story-frame-container\"></div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["doubleBox"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.lambda;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"pstory-block-hbox-columns-frame teaser-body perso-double-box\" aria-hidden=\"true\">\n        <div class=\"pstory-hbox-column-frame\" data-width=\"6\">\n\n            <div class=\"pstory-hbox-column-frame perso-double-box perso-double-box-left\" data-v-align=\"middle\" data-width=\"12\">\n                <div class=\"perso-double-box-container\">\n                    "
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0["box-icon1"] : depth0)) != null ? stack1.url : stack1),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-value "
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0["box-value1"] : depth0)) != null ? stack1.isDate : stack1),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\n                            "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-value1"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-text\">\n                            "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-label1"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n        <div class=\"pstory-hbox-column-frame\" data-width=\"6\">\n\n            <div class=\"pstory-hbox-column-frame perso-double-box perso-double-box-right\" data-v-align=\"middle\" data-width=\"12\">\n                <div class=\"perso-double-box-container\">\n                    "
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0["box-icon2"] : depth0)) != null ? stack1.url : stack1),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-value "
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0["box-value2"] : depth0)) != null ? stack1.isDate : stack1),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\n                            "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-value2"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-text\">\n                            "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-label2"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"perso-icon-box "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0["box-icon1"] : depth0)) != null ? stack1.url : stack1), depth0))
    + "\"></div>";
},"4":function(container,depth0,helpers,partials,data) {
    return "perso-double-box-value-date";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"perso-icon-box "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0["box-icon2"] : depth0)) != null ? stack1.url : stack1), depth0))
    + "\"></div>";
},"8":function(container,depth0,helpers,partials,data) {
    return " perso-double-box-value-date ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["doubleBox2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.lambda;

  return "    <div class=\"perso-teaser-template-inner\">\n"
    + ((stack1 = container.invokePartial(partials.teaserHeader2,depth0,{"name":"teaserHeader2","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"pstory-block-hbox-columns-frame teaser-body perso-double-box-2\" aria-hidden=\"true\">\n        <div class=\"pstory-hbox-column-frame\" data-width=\"6\">\n\n            <div class=\"pstory-hbox-column-frame perso-double-box perso-double-box-left\" data-v-align=\"middle\" data-width=\"12\">\n                <div class=\"perso-double-box-container\">\n                    <div class=\"perso-icon-box perso-left-icon\"></div>\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-value "
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0["box-value1"] : depth0)) != null ? stack1.isDate : stack1),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\n                            "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-value1"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-text\">\n                            "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-label1"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n        <div class=\"pstory-hbox-column-frame\" data-width=\"6\">\n\n            <div class=\"pstory-hbox-column-frame perso-double-box perso-double-box-right\" data-v-align=\"middle\" data-width=\"12\">\n                <div class=\"perso-double-box-container\">\n                    <div class=\"perso-icon-box perso-right-icon\"></div>\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-value "
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0["box-value2"] : depth0)) != null ? stack1.isDate : stack1),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\n                            "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-value2"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-text\">\n                            "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-label2"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n  </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "perso-double-box-value-date";
},"4":function(container,depth0,helpers,partials,data) {
    return " perso-double-box-value-date ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial2,depth0,{"name":"teaserWrapperPartial2","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["entitySelector"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.escapeExpression;

  return "    <div class=\"perso-teaser-template-inner\">\n"
    + ((stack1 = container.invokePartial(partials.teaserHeader2,depth0,{"name":"teaserHeader2","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"pstory-block-hbox-columns-frame teaser-body perso-entity-container\" id=\""
    + alias2(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias2(container.lambda(((stack1 = (depth0 != null ? depth0["entity-selector1"] : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\" aria-hidden=\"true\">\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0["entity-selector1"] : depth0)) != null ? stack1.options : stack1),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n  </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <div class=\"pstory-hbox-column-frame pstory-hbox-entity-container-item\">\n\n            <div class=\"pstory-hbox-column-frame perso-double-box\" data-v-align=\"middle\" data-width=\"12\">\n                <div class=\"pstory-block-entity-selector\">\n                    <div class=\"perso-options-container\">\n                        <div class=\"pstory-button pstory-entity-selector-item "
    + alias4(((helper = (helper = helpers.spendingIndication || (depth0 != null ? depth0.spendingIndication : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"spendingIndication","hash":{},"data":data}) : helper)))
    + "\" action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\"\n                             pstory-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                            <div class=\"pstory-entity-icon "
    + alias4(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data}) : helper)))
    + "\">\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-text\">\n                            "
    + ((stack1 = ((helper = (helper = helpers.budgetCategoryName || (depth0 != null ? depth0.budgetCategoryName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"budgetCategoryName","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-value perso-budget-value\" data-budget-value="
    + ((stack1 = ((helper = (helper = helpers.budgetRemainingAmount || (depth0 != null ? depth0.budgetRemainingAmount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"budgetRemainingAmount","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + ">\n                            $ "
    + ((stack1 = ((helper = (helper = helpers.budgetRemainingAmount || (depth0 != null ? depth0.budgetRemainingAmount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"budgetRemainingAmount","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n                    <div class=\"pstory-block-frame pstory-block-text\">\n                        <div class=\"pstory-block-content perso-double-box-sub-value\">\n                            out of $ "
    + ((stack1 = ((helper = (helper = helpers.budgetAmount || (depth0 != null ? depth0.budgetAmount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"budgetAmount","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n\n        </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial2,depth0,{"name":"teaserWrapperPartial2","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["horizontalBar"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"pstory-block-hbox-columns-frame teaser-body\" aria-hidden=\"true\">\n        <div class=\"pstory-hbox-column-frame\" data-width=\"12\" id=\""
    + alias1(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.chart : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"></div>\n    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["horizontalBarAndBox"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression, alias2=container.lambda;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"pstory-block-hbox-columns-frame teaser-body\" aria-hidden=\"true\">\n        <div class=\"pstory-hbox-column-frame\" data-width=\"8\">\n            <div id=\""
    + alias1(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.chart : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"></div>\n        </div>\n\n        <div class=\"pstory-hbox-column-frame perso-left-separator\" data-width=\"4\" data-v-align=\"middle\">\n            <div class=\"pstory-block-hbox-columns-frame\">\n                <div class=\"pstory-block-frame pstory-block-text\">\n                    <div class=\"pstory-block-content\">\n                        "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-label"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                    </div>\n                </div>\n                <div class=\"pstory-block-frame pstory-block-text perso-percent-small\">\n                    <div class=\"pstory-block-content\">\n                            "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-value"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["image"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"teaser-body\" aria-hidden=\"true\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0["main-image"] : depth0)) != null ? stack1.isAbsoluteURL : stack1),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "    </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "        <img src=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0["main-image"] : depth0)) != null ? stack1.absUrl : stack1), depth0))
    + "\" alt=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0["main-image"] : depth0)) != null ? stack1.alt : stack1), depth0))
    + "\" title=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0["main-image"] : depth0)) != null ? stack1.alt : stack1), depth0))
    + "\" />\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "        <div class=\"perso-image "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0["main-image"] : depth0)) != null ? stack1.absUrl : stack1), depth0))
    + "\" title=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0["main-image"] : depth0)) != null ? stack1.alt : stack1), depth0))
    + "\"></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["imageAndBox"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"pstory-block-hbox-columns-frame teaser-body\" aria-hidden=\"true\">\n        <div class=\"pstory-hbox-column-frame\" data-width=\"6\">\n            <img src=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0["main-image"] : depth0)) != null ? stack1.url : stack1), depth0))
    + "\" alt=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0["main-image"] : depth0)) != null ? stack1.alt : stack1), depth0))
    + "\" title=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0["main-image"] : depth0)) != null ? stack1.alt : stack1), depth0))
    + "\" style=\"max-height: 100%; max-width: 100%\" />\n        </div>\n        <div class=\"pstory-hbox-column-frame\" data-v-align=\"middle\" data-width=\"6\">\n\n            <div class=\"pstory-block-hbox-columns-frame\">\n\n                <div class=\"pstory-block-frame pstory-block-text\">\n                    <div class=\"pstory-block-content\">\n                        "
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0["box-label"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                    </div>\n                </div>\n                <div class=\"pstory-block-frame pstory-block-text perso-percent-large\">\n                    <div class=\"pstory-block-content\">\n                        "
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0["box-value"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                    </div>\n                </div>\n\n            </div>\n        </div>\n    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["lineChart"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"pstory-block-hbox-columns-frame teaser-body\" aria-hidden=\"true\">\n        <div class=\"pstory-hbox-column-frame\" data-width=\"12\" id=\""
    + alias1(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.chart : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"></div>\n    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pie"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"pstory-block-hbox-columns-frame teaser-body\" aria-hidden=\"true\">\n        <div class=\"pstory-hbox-column-frame\" data-width=\"4\">\n            <div id=\""
    + alias4(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.chart1 : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"></div>\n\n            <div class=\"pstory-block-frame pstory-block-text perso-pie-text perso-block-title\">\n                <div class=\"pstory-block-content\">\n                    "
    + ((stack1 = alias5(((stack1 = (depth0 != null ? depth0.label1 : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                </div>\n            </div>\n\n        </div>\n        <div class=\"pstory-hbox-column-frame\" data-width=\"4\">\n            <div id=\""
    + alias4(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.chart2 : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"></div>\n\n            <div class=\"pstory-block-frame pstory-block-text perso-pie-text perso-block-title\">\n                <div class=\"pstory-block-content\">\n                    "
    + ((stack1 = alias5(((stack1 = (depth0 != null ? depth0.label2 : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                </div>\n            </div>\n        </div>\n        <div class=\"pstory-hbox-column-frame\" data-width=\"4\">\n            <div id=\""
    + alias4(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.chart3 : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"></div>\n\n            <div class=\"pstory-block-frame pstory-block-text perso-pie-text perso-block-title\">\n                <div class=\"pstory-block-content\">\n                    "
    + ((stack1 = alias5(((stack1 = (depth0 != null ? depth0.label3 : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                </div>\n            </div>\n        </div>\n    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["pinChart"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"teaser-body\" id=\""
    + alias1(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.chart : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"  aria-hidden=\"true\"></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["simpleText"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["teaserHeader"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<div class=\"perso-teaser-header\" aria-hidden=\"true\">\n    <div class=\"perso-teaser-top\">\n        <div class=\"pstory-block-frame pstory-block-date-box\">\n            <div class=\"pstory-block-content\">\n                <div class=\"perso-teaser-date pstory-block-date-month\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.date : depth0)) != null ? stack1.month : stack1), depth0))
    + "</div>\n                <div class=\"perso-teaser-date pstory-block-date-day\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.date : depth0)) != null ? stack1.day : stack1), depth0))
    + "</div>\n            </div>\n        </div>\n        <div class=\"pstory-block-frame pstory-block-text perso-insight-title-text perso-block-title\">\n            <div class=\"pstory-block-content\">\n                "
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.title : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n            </div>\n        </div>\n    </div>\n\n    <div class=\"pstory-block-frame pstory-block-text perso-teaser-text\">\n        <div class=\"pstory-block-content\">\n           "
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0["teaser-text"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["teaserHeader2"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "<div class=\"perso-teaser-header perso-teaser-header-2\" aria-hidden=\"true\">\n    <div class=\"perso-teaser-top\">\n        <div class=\"pstory-block-frame pstory-block-text perso-insight-title-text perso-block-title\">\n            <div class=\"pstory-block-content\">\n                "
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.title : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n            </div>\n        </div>\n    </div>\n\n    <div class=\"pstory-block-frame pstory-block-text perso-teaser-text\">\n        <div class=\"pstory-block-content\">\n           "
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0["teaser-text"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n        </div>\n    </div>\n</div>";
},"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["teaserWrapperPartial"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "perso-highlighted";
},"3":function(container,depth0,helpers,partials,data) {
    return "not ";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.active : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                        <a href=\"#!\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"\n                           class=\"pstory-link-navigateTo perso-button-animation pstory-button perso-ripple-closed\"\n                           action-id=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" pnavigate-id=\""
    + alias4(((helper = (helper = helpers.navigateTarget || (depth0 != null ? depth0.navigateTarget : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"navigateTarget","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "<div class=\"perso-teaser-template perso-teaser-template-bar-box perso-"
    + alias4(((helper = (helper = helpers.status || (depth0 != null ? depth0.status : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"status","hash":{},"data":data}) : helper)))
    + "  "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.highlighted : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" id=\"perso-teaser-template_"
    + alias4(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"insightId","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"perso-teaser-template-inner\" role=\"button\" aria-label=\""
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.date : depth0)) != null ? stack1.month : stack1), depth0))
    + " "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.date : depth0)) != null ? stack1.day : stack1), depth0))
    + ", "
    + alias4(((helper = (helper = helpers.status || (depth0 != null ? depth0.status : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"status","hash":{},"data":data}) : helper)))
    + ". "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.highlighted : depth0),{"name":"unless","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "highlighted. "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.title : depth0)) != null ? stack1.text : stack1), depth0))
    + ". "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0["teaser-text"] : depth0)) != null ? stack1.text : stack1), depth0))
    + "\">\n"
    + ((stack1 = container.invokePartial(partials["@partial-block"],depth0,{"name":"@partial-block","data":data,"indent":"        ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        <div id=\"pstory-block-buttons_"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.buttons : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\" class=\"pstory-block-frame pstory-block-buttons\">\n            <div class=\"pstory-block-content\">\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0.buttons : depth0)) != null ? stack1.options : stack1),{"name":"each","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>\n";
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["teaserWrapperPartial2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "perso-highlighted";
},"3":function(container,depth0,helpers,partials,data) {
    return "not ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "<div class=\"perso-teaser-template perso-teaser-template2 perso-teaser-template-bar-box perso-"
    + alias4(((helper = (helper = helpers.status || (depth0 != null ? depth0.status : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"status","hash":{},"data":data}) : helper)))
    + "  "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.highlighted : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" id=\"perso-teaser-template_"
    + alias4(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"insightId","hash":{},"data":data}) : helper)))
    + "\"\n role=\"button\" aria-label=\""
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.date : depth0)) != null ? stack1.month : stack1), depth0))
    + " "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.date : depth0)) != null ? stack1.day : stack1), depth0))
    + ". "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.title : depth0)) != null ? stack1.text : stack1), depth0))
    + ". "
    + alias4(((helper = (helper = helpers.status || (depth0 != null ? depth0.status : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"status","hash":{},"data":data}) : helper)))
    + ". "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.highlighted : depth0),{"name":"unless","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "highlighted. "
    + ((stack1 = alias5(((stack1 = (depth0 != null ? depth0["teaser-text"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\">\n"
    + ((stack1 = container.invokePartial(partials["@partial-block"],depth0,{"name":"@partial-block","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "</div>";
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["textAndButton"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "    <div class=\"teaser-body\" aria-hidden=\"true\"></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["txList"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"teaser-body\" id=\""
    + alias1(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.txList : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"  aria-hidden=\"true\"></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["verticalBar"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"teaser-body\" id=\""
    + alias1(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.chart : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"  aria-hidden=\"true\"></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["Personetics"]["UI"]["Handlebars"]["templates"]["verticalBarAndBox"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression, alias2=container.lambda;

  return ((stack1 = container.invokePartial(partials.teaserHeader,depth0,{"name":"teaserHeader","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n    <div class=\"pstory-block-hbox-columns-frame teaser-body\" aria-hidden=\"true\">\n        <div class=\"pstory-hbox-column-frame\" data-width=\"6\">\n            <div id=\""
    + alias1(((helper = (helper = helpers.insightId || (depth0 != null ? depth0.insightId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"insightId","hash":{},"data":data}) : helper)))
    + "_"
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.chart : depth0)) != null ? stack1.blockId : stack1), depth0))
    + "\"></div>\n        </div>\n        <div class=\"pstory-hbox-column-frame\" data-width=\"6\">\n            <!--<div class=\"pstory-block-hbox-columns-frame\">-->\n\n            <div class=\"pstory-hbox-column-frame perso-left-separator\" data-v-align=\"middle\" data-width=\"12\">\n                <div class=\"pstory-block-frame pstory-block-text perso-upper-text\">\n                    <div class=\"pstory-block-content\">\n                        "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-label"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                    </div>\n                </div>\n                <div class=\"pstory-block-frame pstory-block-text perso-down-text\">\n                    <div class=\"pstory-block-content\">\n                        "
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0["box-value"] : depth0)) != null ? stack1.text : stack1), depth0)) != null ? stack1 : "")
    + "\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.teaserWrapperPartial,depth0,{"name":"teaserWrapperPartial","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});})(window, window.Personetics = window.Personetics || {}, jQuery, window.Personetics.UI.Handlebars);

