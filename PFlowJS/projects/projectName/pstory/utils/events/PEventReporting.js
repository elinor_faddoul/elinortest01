(function (window, Personetics, $, undefined) {

    Personetics.utils = Personetics.utils ? Personetics.utils : {};
    var pEventReporting = Personetics.utils.PEvent.extend({

        initialize: function () {
            this.params = {
                "eventType": "sendEvents",
                "checkInPastEvent": "false",
                "protocolVersion": Personetics.projectConfiguration.getConfig("protocolVersion"),
                "userId": "",
                "type": "",
                "value": "",
                "insightId": "",
                "instanceId": ""
            }
        },

        setValue: function (parameters) {
            var me = this;
            if (typeof parameters !== 'object' || parameters === null)
                return false;
            $.each(parameters, function (key, value) {
                if (me.params.hasOwnProperty(key) && value != 'undefined' && value != null) {
                    me.params[key] = value;
                }
            })
        },

        createMessage: function () {
            var params = this.params;
            var date = new Date();
            if (params.eventType == "sendEvents")   
            {         
                var datestr = date.toISOString();
                date = Personetics.utils.date.monthNumber(datestr) + '/' + Personetics.utils.date.dayNumber(datestr, "DD") + '/' + Personetics.utils.date.yearNumber(datestr);
            }
            var eventMessage = {
                "type": params.eventType,
                "userId": params.userId,
                "checkInPastEvent": params.checkInPastEvent,
                "protocolVersion": params.protocolVersion,
                "eventInfo": {
                    "type": params.type,
                    "eventDateTime": this.getFormattedDate(),
                    "params": {
                        "value": params.value,
                        "insightId": params.insightId,
                        "instanceId": params.instanceId
                    }
                },

            }
            return eventMessage;
        },

        notifyEvent: function () {
            var pevent = {
                eventMessage: this.createMessage()
            }
            personetics.notifyEvent(pevent);
        },
        getFormattedDate: function () {
            var currentDate = new Date();
            var currentDateString = (currentDate.getUTCMonth() + 1) + "/" + currentDate.getUTCDate() + "/" + currentDate.getUTCFullYear() + " " + currentDate.getUTCHours() + ":" + currentDate.getUTCMinutes() + ":" + currentDate.getUTCSeconds();
            return currentDateString;
        }

        /*getJsonMessageEvent: function () {
            return JSON.stringify(this.eventMessage);
        },

        onSuccessCallback: function(){
        },

        onErrorCallback: function(){
        }*/
    });

    Personetics.utils.PEventReporting = new pEventReporting();

})(window, window.Personetics = window.Personetics || {}, jQuery);
